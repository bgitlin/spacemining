﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiningShipAnim : MonoBehaviour {
    public Animator anim;
    public HealthSystem hs;

    int cycleOffset = Animator.StringToHash("cycleOffset");
    int hit = Animator.StringToHash("hit");
    int flyAway = Animator.StringToHash("flyAway");
    void Awake()
    {
        anim.SetFloat(cycleOffset, Random.Range(0f,1f));
        Subscribe();
    }
    void Subscribe()
    {
        hs.OnDamageTaken += HitAnim;
        hs.OnDeath += FlyAway;
        //hs.Onkilled += HitAnim;
    }

    void HitAnim(){
        anim.SetTrigger(hit);
    }
    void FlyAway(){
        anim.SetTrigger(flyAway);
    }
}
