﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossShot : Minable {

    Vector3 myVector;
    Vector3 moveToPos;

    public LevelBlockInfo myBlockInfo;
    public int damageAmount;
    public float moveSpeed;
    public float rotationSpeed;
    public Rigidbody myRB;

    private void Start()
    {
        Transform target;
        if (GameManager.instance.companionFactory.currentCompanions.Count > 0 && Random.value > 0.5f)
        {
            CompanionFactory factoryRef = GameManager.instance.companionFactory;
            target = factoryRef.currentCompanions[Random.Range(0,factoryRef.currentCompanions.Count)].transform;
        }
        else
        {
            target = GameManager.instance.player.transform;
        }

        StartCoroutine(MyUpdate(target));
        damageAmount = (int)(UpgradeManager.instance.damageCurve.Evaluate(DataManager.instance.celestrialData.level + 1 + (DataManager.instance.celestrialData.currentMissionIndex * 0.15f)) * 0.25f);
        healthSystem.InitHealth((UpgradeManager.instance.healthCurve.Evaluate(DataManager.instance.celestrialData.level + 0.5f + (DataManager.instance.celestrialData.currentMissionIndex * 0.15f))) * 0.25f);
    }

    public override void Destroy()
    {
        base.Destroy();

        //TO DO: change to separate list for AI
        myBlockInfo.myMinables.Remove(this);
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.tag == "Player" || other.transform.tag == "Companion")
        {
            //deduct hp from player
            other.gameObject.GetComponent<HealthSystem>().TakeDamage(damageAmount);
            //explosion fx

            //TO DO: change to separate list for AI
            myBlockInfo.myMinables.Remove(this);
            Destroy(gameObject);
        }
    }

    private IEnumerator MyUpdate(Transform _target)
    {
        //float timer = 0;
        while (_target != null)
        {
            myVector = _target.position - transform.position;
            Quaternion myQuat = Quaternion.LookRotation(myVector, transform.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, myQuat, rotationSpeed * Time.deltaTime);

            myRB.AddForce(myVector.normalized * moveSpeed * Time.deltaTime);

            yield return 0f;
        }
    }
}
