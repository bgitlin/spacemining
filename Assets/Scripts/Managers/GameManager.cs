﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    private void Awake()
    {
        instance = this;
        Screen.orientation = ScreenOrientation.Portrait;
    }


    public CameraManager cameraManager;
    public LevelManager levelManager;
    public GameObject player;
    public GameObject mineFx;
    public CompanionFactory companionFactory;
    public MissionData missionData;

    //Holds levelPrefablists based on generation type
    public List<GameObject> planetType1Prefabs;
    public List<GameObject> planetType2Prefabs;
    public List<GameObject> gasGiantTypePrefabs;

    //TEMP
    public GameObject backgroundCamera;

    public void Start()
    {
        PlayerShip.instance.Initialize();
        DataManager.instance.LoadLastCelestrialData();

        List<GameObject> tempGoList = new List<GameObject>();
        switch(DataManager.instance.celestrialData.generationType)
        {
            case (CelestrialData.GenerationType.planetType1):
                print("planetType1");
                tempGoList = planetType1Prefabs;
                break;
            case (CelestrialData.GenerationType.planetType2):
                print("planetType2");
                tempGoList = planetType2Prefabs;
                break;
            case (CelestrialData.GenerationType.gasGiant):
                print("gasGiant");
                tempGoList = gasGiantTypePrefabs;
                backgroundCamera.SetActive(true);
                break;
        }
        levelManager.InitializeLevel(tempGoList);
        missionData = DataManager.instance.celestrialData.missions[DataManager.instance.celestrialData.currentMissionIndex];
    }

    public void RestartLevel()
    {
        StartCoroutine(RestartLevelRoutine());
    }
    private IEnumerator RestartLevelRoutine()
    {
        //Display message to player they've lost
        print("you've failed");
        yield return new WaitForSeconds(1f);
        //Restart the level
        print("restarting Level");
        SceneManager.LoadScene("MiningLevel");
    }

    public void MissionComplete()
    {
        //In the future we should split this function out so we can return to overworld without marking it complete.
        CelestrialData tempData = DataManager.instance.LoadLastCelestrialData();

        tempData.missions[tempData.currentMissionIndex].complete = true;
        UnlockNextMission(tempData, tempData.currentMissionIndex);

        
        IdleMineManager.instance.AddNewIdleMine(tempData.planetIndex);

        DataManager.instance.SaveLastCelestrialData(tempData);
        StartCoroutine(ReturnToOverworldRoutine());
    }
    /*
    public void ReturnToOverworld(bool _complete)
    {
        CelestrialData tempData = DataManager.instance.LoadLastCelestrialData();
        GalaxyData tempGalaxyData = DataManager.instance.LoadGalaxyData();
        tempGalaxyData.celestrialDatas[tempData.index].missions[tempData.currentMissionIndex].complete = _complete;
        if (_complete)
        {
            tempGalaxyData.celestrialDatas[tempData.index].UnlockNextMission(tempData.currentMissionIndex);
        }
        DataManager.instance.SaveGalaxyData(tempGalaxyData.celestrialDatas);

        StartCoroutine(ReturnToOverworldRoutine());
    }
    */
    public IEnumerator ReturnToOverworldRoutine()
    {
        print("Level Complete, returning to overworld.");
        yield return new WaitForSeconds(1f);
        ReturnToOverWorld();
    }

    public void LoadLevel()
    {
        StartCoroutine(LoadLevelRoutine());
    }
    public IEnumerator LoadLevelRoutine()
    {
        print("Loading Level");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("MiningLevel");
    }

    public void UnlockNextMission(CelestrialData _data, int _prevMission)
    {
        Debug.Log("prevMission" + _prevMission + "|  missioncount: " + _data.missions.Count);
        if ((_prevMission + 1) < _data.missions.Count)
        {
            Debug.Log("unlockNextmission");
            int num = (_prevMission + 1);
            _data.missions[num].locked = false;
        }
        else
        {
            Debug.Log("NextPlanet!");
            _data.beat = true;
        }
    }

    public void ReturnToOverWorld()
    {
        SceneManager.LoadScene("GalaxyMap");
    }
}
