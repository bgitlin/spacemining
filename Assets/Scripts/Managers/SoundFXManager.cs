﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundFXManager : MonoBehaviour
{
    public static SoundFXManager instance;
    private void Awake()
    {
        instance = this;
    }

    [FMODUnity.EventRef] [SerializeField] string miningSound;
    [FMODUnity.EventRef] [SerializeField] string resourceDestroy;
    [FMODUnity.EventRef] [SerializeField] string completeBlock;
    [FMODUnity.EventRef] [SerializeField] string completeEnemies;
    [FMODUnity.EventRef] [SerializeField] string completeBoss;
    [FMODUnity.EventRef] [SerializeField] string startEnemyBlock;
    [FMODUnity.EventRef] [SerializeField] string startBossBlock;
    [FMODUnity.EventRef] [SerializeField] string moveToNextLevelChunk;
    [FMODUnity.EventRef] [SerializeField] string openMenu;
    [FMODUnity.EventRef] [SerializeField] string closeMenu;
    [FMODUnity.EventRef] [SerializeField] string weaponEquip;
    [FMODUnity.EventRef] [SerializeField] string companionEquip;
    [FMODUnity.EventRef] [SerializeField] string upgradePurchased;
    [FMODUnity.EventRef] [SerializeField] string upgradeCantPurchase;

    private bool canSwipe = true;
    public void Swipe()
    {
        if (canSwipe)
        {
            FMODUnity.RuntimeManager.PlayOneShot(miningSound);
            
            //digsource.pitch = Random.Range(0.75f, 1.25f);
            //digsource.PlayOneShot(swipeSound);
            canSwipe = false;
        }
    }
    private IEnumerator LimitSwipeSound()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.4f);
            canSwipe = true;
        }
    }

    //Game Sounds
    public void ResourceDestroy()
    {
        FMODUnity.RuntimeManager.PlayOneShot(resourceDestroy);
    }
    public void CompleteBlock()
    {
        FMODUnity.RuntimeManager.PlayOneShot(completeBlock);
    }
    public void CompleteEnemies()
    {
        FMODUnity.RuntimeManager.PlayOneShot(completeEnemies);
    }
    public void CompleteBoss()
    {
        FMODUnity.RuntimeManager.PlayOneShot(completeBoss);
    }
    public void StartEnemyBlock()
    {
        FMODUnity.RuntimeManager.PlayOneShot(startEnemyBlock);
    }
    public void StartBossBlock()
    {
        FMODUnity.RuntimeManager.PlayOneShot(startBossBlock);
    }
    public void MoveToNextLevelChunk()
    {
        FMODUnity.RuntimeManager.PlayOneShot(moveToNextLevelChunk);
    }

    //UI Sounds
    public void OpenMenu()
    {
        FMODUnity.RuntimeManager.PlayOneShot(openMenu);
    }
    public void CloseMenu()
    {
        FMODUnity.RuntimeManager.PlayOneShot(closeMenu);
    }
    public void WeaponEquip()
    {
        FMODUnity.RuntimeManager.PlayOneShot(weaponEquip);
    }
    public void CompanionEquip()
    {
        FMODUnity.RuntimeManager.PlayOneShot(companionEquip);
    }
    public void UpgradePurchased()
    {
        FMODUnity.RuntimeManager.PlayOneShot(companionEquip);
    }
    public void UpgradeCantPurchase()
    {
        FMODUnity.RuntimeManager.PlayOneShot(upgradeCantPurchase);
    }
}