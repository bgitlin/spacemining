﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Overworld : MonoBehaviour {

    public static UI_Overworld instance;

    private void Awake()
    {
        instance = this;
    }
    
    public GameObject returnToOverworldButton;
    public GameObject startMissionButton;
    public CelestrialBody selectedBody;


    public void BodySelected(CelestrialBody _body)
    {
        returnToOverworldButton.SetActive(true);
        startMissionButton.SetActive(!_body.IsBeat());
        
        if (_body.celesData.type == CelestrialData.CelestrialBodyType.planet)
        {
            selectedBody = _body;
        }
        else
        {
            print("side");
            print(_body.celesData.planetIndex);
            selectedBody = _body;
        }
    }

    public void ReturnToOverworld()
    {
        returnToOverworldButton.SetActive(false);
        startMissionButton.SetActive(false);
        selectedBody.Return();
        Vector3 tempVector = new Vector3(0, selectedBody.transform.position.y, 0);
        GalaxyMapManager.instance.ReturnToOverworldView(tempVector);
    }
    
    
    public void StartMission()
    {
        selectedBody.SelectedMission();
    }
}
