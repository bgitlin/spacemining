﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    public new Camera camera;
    public AnimationCurve easeOutCurve;
    public AnimationCurve easeInOutCurve;
    public float trackingSpeed = 0.2f;
    public float scrollSpeed = 1;
    public LayerMask layerMask;

    [HideInInspector]
    public IEnumerator moveToRoutine;
    public void MoveTo(Vector3 _target, float _speed, AnimationCurve _curveType = null)
    {
        CancelMoveTo();
        moveToRoutine = MoveToRoutine(_target, _speed, _curveType);
        StartCoroutine(moveToRoutine);
    }
    public void CancelMoveTo()
    {
        if (moveToRoutine != null)
        {
            StopCoroutine(moveToRoutine);
        }
    }
    private IEnumerator MoveToRoutine(Vector3 _targetPos, float _speed, AnimationCurve _curveType = null)
    {
        Vector3 startPos = transform.position;
        Vector3 currentPos = startPos;
        _curveType = _curveType == null ? easeOutCurve : _curveType;

        while (Vector3.Magnitude(transform.position - _targetPos) > 0.1f)
        {
            currentPos = Vector3.MoveTowards(currentPos, _targetPos, Time.deltaTime * _speed * 25);
            float percentComplete = Map.map(Vector3.Magnitude(currentPos - _targetPos), Vector3.Magnitude(startPos - _targetPos), 0, 0, 1);
            transform.position = Vector3.LerpUnclamped(startPos, _targetPos, _curveType.Evaluate(percentComplete));
            yield return 0f;
        }
        transform.position = _targetPos;
    }

    [HideInInspector]
    public IEnumerator adjustOffsetRoutine;
    public void AdjustOffset(float _targetOffset, float _speed, AnimationCurve _curveType = null)
    {
        CancelAdjustOffset();
        adjustOffsetRoutine = AdjustOffsetRoutine(_targetOffset, _speed, _curveType);
        StartCoroutine(adjustOffsetRoutine);
    }
    public void CancelAdjustOffset()
    {
        if (adjustOffsetRoutine != null)
        {
            StopCoroutine(adjustOffsetRoutine);
        }
    }
    private IEnumerator AdjustOffsetRoutine(float _targetOffset, float _speed, AnimationCurve _curveType = null)
    {
        if (_curveType == null)
        {
            _curveType = easeOutCurve;
        }

        float timer = 0;
        float distance = Mathf.Abs(camera.transform.localPosition.y - _targetOffset);
        Vector3 startPos = camera.transform.localPosition;

        while (timer < 1)
        {
            if (startPos.y == _targetOffset) { break; }
            timer += (Time.deltaTime / distance) * _speed * 20;
            camera.transform.localPosition = Vector3.Lerp(startPos, new Vector3(0, 1, 0) * _targetOffset, _curveType.Evaluate(timer));
            yield return 0f;
        }
        camera.transform.localPosition = new Vector3(0, 1, 0) * _targetOffset;
        adjustOffsetRoutine = null;
    }

    [HideInInspector]
    public IEnumerator adjustScaleRoutine;
    public void AdjustScale(float _target, float _speed)
    {
        CancelScale();
        adjustScaleRoutine = AdjustOffsetRoutine(_target, _speed);
        StartCoroutine(adjustScaleRoutine);
    }
    public void CancelScale()
    {
        if (adjustScaleRoutine != null)
        {
            StopCoroutine(adjustScaleRoutine);
        }
    }
    private IEnumerator AdjustScaleRoutine(float _target, float _speed)
    {
        float timer = 0;
        Vector3 _start = transform.localScale;

        while (timer < 1)
        {
            timer += Time.deltaTime * _speed;
            camera.transform.localScale = Vector3.Lerp(_start, Vector3.one * _target, easeOutCurve.Evaluate(timer));
            yield return 0f;
        }
        camera.transform.localScale = Vector3.one * _target;
        adjustScaleRoutine = null;
    }

    [HideInInspector]
    public IEnumerator adjustAngleRoutine;
    public void AdjustAngle(Vector3 _target, float _speed)
    {
        CancelAngle();
        adjustAngleRoutine = AdjustAngleRoutine(_target, _speed);
        StartCoroutine(adjustAngleRoutine);
    }
    public void CancelAngle()
    {
        if (adjustAngleRoutine != null)
        {
            StopCoroutine(adjustAngleRoutine);
        }
    }
    private IEnumerator AdjustAngleRoutine(Vector3 _target, float _speed)
    {
        float timer = 0;
        Quaternion _start = transform.rotation;

        while (timer < 1)
        {
            timer += Time.deltaTime * _speed;
            transform.rotation = Quaternion.Lerp(_start, Quaternion.Euler(_target), easeOutCurve.Evaluate(timer));
            yield return 0f;
        }
        transform.rotation = Quaternion.Euler(_target);
        adjustAngleRoutine = null;
    }

    private Vector3 mousePosition;
    private bool scrolling = false;
    private IEnumerator trackMousePosition;
    private IEnumerator TrackMousePositionRoutine()
    {
        while (true)
        {
            float scrollDistance = Vector3.Distance(mousePosition, Input.mousePosition);
            if (scrollDistance > 1f)
            {
                Vector3 mouseDistance = mousePosition - Input.mousePosition;
                if (scrollDistance > 40)
                {
                    MoveTo(transform.position + new Vector3(mouseDistance.x * scrollSpeed, 0, mouseDistance.y * scrollSpeed), 2f);
                }
                else
                {
                    CancelMoveTo();
                    transform.position = Vector3.MoveTowards(transform.position, transform.position + (new Vector3(mouseDistance.x, 0, mouseDistance.y) * trackingSpeed), 1f);
                }
                mousePosition = Input.mousePosition;
                scrolling = true;
            }
            yield return 0f;
        }
    }
    public void StartMouseTrackPosition()
    {
        if (trackMousePosition == null)
        {
            mousePosition = Input.mousePosition;
            StopMouseTrackPosition();
            trackMousePosition = TrackMousePositionRoutine();
            StartCoroutine(trackMousePosition);
        }
    }
    public void StopMouseTrackPosition()
    {
        if (trackMousePosition != null)
        {
            StopCoroutine(trackMousePosition);
        }
        trackMousePosition = null;
    }

    public IEnumerator trackPosition;
    public void StartTrackPosition(Transform _transform, Vector3 _offset)
    {
        if (trackPosition == null)
        {
            StopTrackPosition();
            trackPosition = TrackPositionRoutine(_transform, _offset);
            StartCoroutine(trackPosition);
        }
    }
    private IEnumerator TrackPositionRoutine(Transform _transform, Vector3 _offset)
    {
        while (true)
        {
            transform.position = _transform.position + _offset;
            yield return 0f;
        }
    }

    public void StopTrackPosition()
    {
        if (trackPosition != null)
        {
            print("trackPosition Stopped");
            StopCoroutine(trackPosition);
        }
        trackPosition = null;
    }
    
    public void GoToPosition(Vector3 _inputPos, float _offsetAmt = -10, float _speed = 1)
    {
        StartCoroutine(GoToPositionRoutine(_inputPos, _offsetAmt, _speed));
    }
    private IEnumerator GoToPositionRoutine(Vector3 _inputPos, float _offsetAmt, float _speed)
    {
        float timer = 0;

        Vector3 currentPos = transform.position;
        Vector3 newLocation = new Vector3(_inputPos.x, _inputPos.y, _offsetAmt);

        while (timer < 1)
        {
            timer += Time.deltaTime * _speed;
            transform.position = Vector3.Lerp(currentPos, newLocation, timer);
            yield return 0f;
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
