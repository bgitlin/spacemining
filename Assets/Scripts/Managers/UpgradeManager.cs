﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeManager : MonoBehaviour {

    public static UpgradeManager instance;
    private void Awake()
    {
        instance = this;
    }

    public AnimationCurve costCurve;
    public AnimationCurve healthCurve;
    public AnimationCurve companionStatsCurve;
    public AnimationCurve voxelHealthCurve;
    public AnimationCurve resourceHealthCurve;
    public AnimationCurve damageCurve;
    public enum StatType { Attack, Defense, Mining, Luck }
    public UI_UpgradesMenu ui;

    public void UpgradeStat (StatData _data, StatType _type, string _id)
    {
        float value;

        switch (_type)
        {
            case StatType.Attack:
                value = _data.attack;
                _data.attack++;
                break;
            case StatType.Defense:
                value = _data.defense;
                _data.defense++;
                break;
            case StatType.Mining:
                value = _data.miningEfficiency;
                _data.miningEfficiency++;
                break;
            case StatType.Luck:
                value = _data.luck;
                _data.luck++;
                break;
            default:
                value = 0;
                break;
        }
        print("stats upgraded");
        DataManager.instance.AdjustGold(-Mathf.RoundToInt(costCurve.Evaluate(value)));
        DataManager.instance.SaveStatData(_data, _id);
        ui.UpdateStats();

        SoundFXManager.instance.UpgradePurchased();
    }

    public bool CanPurcahse(StatData _data, StatType _type)
    {
        float value;

        switch (_type)
        {
            case StatType.Attack:
                value = _data.attack;
                break;
            case StatType.Defense:
                value = _data.defense;
                break;
            case StatType.Mining:
                value = _data.miningEfficiency;
                break;
            case StatType.Luck:
                value = _data.luck;
                break;
            default:
                value = 0;
                break;
        }
        return DataManager.instance.currentGold >= Mathf.RoundToInt(costCurve.Evaluate(value));
    }

    //companions
    public bool CanPurchase(int _level)
    {
        return DataManager.instance.currentGold >= Mathf.RoundToInt(costCurve.Evaluate(_level));
    }
    public void PurchaseCompanionUpgrade(int _value)
    {
        DataManager.instance.AdjustGold(-Mathf.RoundToInt(costCurve.Evaluate(_value)));
    }
    public int ReturnUpgradeCost(int _level)
    {
        return Mathf.RoundToInt(costCurve.Evaluate(_level));
    }

    public void PurcahsePlayerUpgrade(int _type)
    {
        if(CanPurcahse(PlayerShip.instance.baseStats, (StatType)_type))
        {
            //change to use base states
            UpgradeStat(PlayerShip.instance.baseStats, (StatType)_type, "playerStats");
        }
        else
        {
            SoundFXManager.instance.UpgradeCantPurchase();
        }
    }
}
