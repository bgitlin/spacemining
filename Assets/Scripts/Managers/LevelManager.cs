﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour 
{
    
    public Transform[] blockPositions;
    //Temp
    public Transform tempSafeBlockPosition;
    public List<GameObject> activeBlocks = new List<GameObject>();
    
    public List<GameObject> currentPlanetPrefabs = new List<GameObject>();

    private int segmentsCleared = 0;
    public bool bossBeat = false;
    public bool levelMoving = false;
    public RectTransform blockCompleteUI;
    public ParticleSystem blockCompleteParticles;
    public GameObject nextSpawn;

    [SerializeField]
    Transform _levelBlockContainer;


    public static System.Action<LevelBlockInfo> OnNextBlock;

    public void InitializeLevel(List<GameObject> levelPrefabs)
    {
        currentPlanetPrefabs = new List<GameObject>();
        currentPlanetPrefabs = levelPrefabs;
        StartingBlocksSetup();
    }

    private void StartingBlocksSetup()
    {
        Instantiate(currentPlanetPrefabs[(int)LevelBlockInfo.BlockType.empty], tempSafeBlockPosition.position, Quaternion.identity);

        for (int i = 0; i < blockPositions.Length; i++)
        {
            if (i == 0)
            {
                print("added Empty");
                GameObject go = Instantiate(currentPlanetPrefabs[(int)LevelBlockInfo.BlockType.empty], blockPositions[i].position, Quaternion.identity);
                go.transform.SetParent(_levelBlockContainer);
                activeBlocks.Add(go);
            }
            else
            {
                print("added Minable");
                GameObject go = Instantiate(currentPlanetPrefabs[(int)LevelBlockInfo.BlockType.minable], blockPositions[i].position, Quaternion.identity);
                go.transform.SetParent(_levelBlockContainer);
                activeBlocks.Add(go);
            }
        }
        ResetLevel();
    }
    
    public void ResetLevel()
    {
        levelMoving = true;
        GameObject tempObj;
        if (segmentsCleared < 3)
        {
            tempObj = currentPlanetPrefabs[(int)LevelBlockInfo.BlockType.minable];//blockPrefab;
            if(segmentsCleared == 1)
            {
                tempObj = currentPlanetPrefabs[(int)LevelBlockInfo.BlockType.enemies];//enemyBlockPrefab;
            }
        }
        else if (segmentsCleared == 3)
        {
            tempObj = currentPlanetPrefabs[(int)LevelBlockInfo.BlockType.boss];//bossBlockPrefab;
        }
        else
        {
            tempObj = currentPlanetPrefabs[(int)LevelBlockInfo.BlockType.empty];//emptyBlockPrefab;
        }
        nextSpawn = tempObj;
        //Set up voxels before they move into view.
        if (segmentsCleared > 0)
        {
            BlockComplete();
        }
        StartCoroutine(DelayedMove());
    }

    private void SlideField()
    {
        if (!bossBeat)
        {
            for (int i = 0; i < activeBlocks.Count; i++)
            {
                bool active = i == 1 ? true : false;
                MoveToPos(activeBlocks[i], blockPositions[i], active);
            }
        }
    }

    private void BlockComplete()
    {
        print("Block Complete");
        AnimationManager.instance.Scale(blockCompleteUI, 0, 0, 1, 3);
        AnimationManager.instance.Scale(blockCompleteUI, 3, 1, 0, 100);
        blockCompleteParticles.Play();

        LevelBlockInfo currentBlock = activeBlocks[0].GetComponent<LevelBlockInfo>();
        if (currentBlock.voxelMap != null && currentBlock.voxelMap.chunks.Length > 0)
        {
            currentBlock.voxelMap.chunks[0].DestoryAll();
        }

        if(currentBlock.blockType == LevelBlockInfo.BlockType.minable)
        {
            SoundFXManager.instance.CompleteBlock();
        }
        else if (currentBlock.blockType == LevelBlockInfo.BlockType.enemies)
        {
            SoundFXManager.instance.CompleteEnemies();
        }
        else if (currentBlock.blockType == LevelBlockInfo.BlockType.boss)
        {
            SoundFXManager.instance.CompleteBoss();
        }
    }
    private void MoveToPos(GameObject _inputObj, Transform _pos, bool _activeBlock)
    {
        StartCoroutine(MoveToPosRoutine(_inputObj, _pos, _activeBlock));
    }
    private IEnumerator DelayedMove()
    {
        float waitTime = segmentsCleared == 0 ? 0 : 2;
        yield return new WaitForSeconds(waitTime);

        LevelBlockInfo next = activeBlocks[1].GetComponent<LevelBlockInfo>();
        next.InitializeVoxelMap(SlideField);

        yield return 0f;
    }

    private IEnumerator MoveToPosRoutine(GameObject _inputObj, Transform _pos, bool _activeBlock)
    {
        Vector3 tempPos = _inputObj.transform.position;
        float timer = 0;
        SoundFXManager.instance.MoveToNextLevelChunk();

        while (timer <= 1)
        {
            timer += Time.deltaTime;
            _inputObj.transform.position = Vector3.Lerp(tempPos, _pos.position, timer);

            yield return 0f;
        }
        _inputObj.transform.position = _pos.position;


        if (_activeBlock)
        {
            FinishReset();
        }
    }

    private void FinishReset()
    {
        GameObject tempBlock = activeBlocks[0];
        activeBlocks.Remove(tempBlock);
        Destroy(tempBlock);
        //Add LastBlock
        GameObject go;
        segmentsCleared++;
        go = Instantiate(nextSpawn);

        if(_levelBlockContainer != null)
        {
            go.transform.SetParent(_levelBlockContainer);
        }

        go.transform.localPosition = blockPositions[blockPositions.Length - 1].position;
        activeBlocks.Add(go);

        ActivateBlock();
        levelMoving = false;

        LevelBlockInfo currentBlock = activeBlocks[0].GetComponent<LevelBlockInfo>();
        if (currentBlock != null)
        {
            if (currentBlock.blockType == LevelBlockInfo.BlockType.enemies)
            {
                SoundFXManager.instance.StartBossBlock();
            }
            else if (currentBlock.blockType == LevelBlockInfo.BlockType.boss)
            {
                SoundFXManager.instance.StartEnemyBlock();
            }
        }
    }

    private void ActivateBlock()
    {
        LevelBlockInfo next = activeBlocks[0].GetComponent<LevelBlockInfo>();
        next.Initialize();
        
        if(OnNextBlock != null)
        {
            OnNextBlock(next);
        }
        _canKillAllAgain = true; //TEMP: FOR TESTING MOVE TO A NEW SCRIPT
    }

    #region Debugging

    //TEMP: MOVE ALL OF THIS TO A DEBUG SCRIPT
    bool _canKillAllAgain = false;
    [ContextMenu("Kill Current Minables")]
    public void TEST_KillMinables()
    {
        if(_canKillAllAgain)
        {
            // myMinables.ForEach(m => m.Destroy());
            LevelBlockInfo tempLevelBlock = activeBlocks[0].GetComponent<LevelBlockInfo>();
            if (tempLevelBlock.blockType == LevelBlockInfo.BlockType.boss)
            {
                bossBeat = true;
                for (int i = 0; i < tempLevelBlock.myMinables.Count; i++)
                {
                    if (tempLevelBlock.myMinables[i].minableType == Minable.MinableType.boss)
                    {
                        tempLevelBlock.myMinables[i].Destroy();
                    }
                    
                }
            }
            tempLevelBlock.BlockComplete();
            _canKillAllAgain = false;
        }
    }

    #endregion
}
