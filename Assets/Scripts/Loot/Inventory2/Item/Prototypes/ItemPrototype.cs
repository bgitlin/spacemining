﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Item/Generic")]
public class ItemPrototype : ScriptableObject
{
	public Item item;

	public bool randomizeClone;

	public Item Clone()
	{
		Item clone = item.Clone();
		if(randomizeClone)
		{
			clone.stats = RandomizeStats(clone.stats, clone.statPercents);
		}
		return clone;
	}
		
	StatData RandomizeStats(StatData stats, StatData percents)
	{
		StatData mods = new StatData
		{
			attack = stats.attack + (UnityEngine.Random.Range(0.5f,1f) * percents.attack),
			defense = stats.defense + (UnityEngine.Random.Range(0.5f,1f) * percents.defense),
			miningEfficiency = stats.miningEfficiency + (UnityEngine.Random.Range(0.5f,1f) * percents.miningEfficiency),
			luck = stats.luck + (UnityEngine.Random.Range(0.5f,1) * percents.luck),
		};
		return mods;
	}
}
