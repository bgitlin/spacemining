﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public class Item 
{
	public string name;

	[TextArea()]
	public string desc;

	[SerializeField]
	public Sprite iconSprite;
	
	public GameObject gameObject;

	public bool equipped;

	public string ownerID;

	public StatData stats;

	public StatData statPercents;

	public int level;

	public List<string> tags;

	public bool isActor;

	public Item(){}

	protected Item(Item i)
	{
		this.name = i.name;
		this.desc = i.desc;
		this.iconSprite = i.iconSprite;
		this.gameObject = i.gameObject;
		this.equipped = i.equipped;
		this.ownerID = i.ownerID;
		this.stats = i.stats;
		this.statPercents = i.statPercents;
		this.tags = i.tags;
		this.level = i.level;
		this.isActor = i.isActor;
	}

	public Item Clone()
	{	
		string json = JsonUtility.ToJson(this);
		Item i = new Item(JsonUtility.FromJson<Item>(json));
		return i;
	}

	public GameObject InstantiateGameObject()
	{
		GameObject g = null;
		if(gameObject != null)
		{
			g = GameObject.Instantiate(gameObject);
		}
		return g;
	}

	public void Upgrade()
	{
        level += 1;
        stats.RecalculateStats(level, statPercents);
		InventoryManager.instance.Save();
	}

		//TODO: TEMP!
	public string CreateRandomName(Item i)
	{
		return i.tags[0] + " " + Math.Round(UnityEngine.Random.Range(0.1f, 1000f), 3);
	}

}
