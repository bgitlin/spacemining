﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class ItemView : MonoBehaviour 
{
	[SerializeField]
	List<Text> _stats;

	[SerializeField]
	Image _icon;

	public Action onClicked;

	public void Display(Item i)
	{
		if(_stats != null)
		{
			_stats[0].text = i.stats.attack.ToString();
			_stats[1].text = i.stats.defense.ToString();
			_stats[2].text = i.stats.miningEfficiency.ToString();
			_stats[3].text = i.stats.luck.ToString();
		}

		if(_icon != null)
		{
			_icon.sprite = i.iconSprite;
		}
	}

}
