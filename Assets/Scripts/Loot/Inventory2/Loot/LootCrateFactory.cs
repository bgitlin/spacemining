﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootCrateFactory : MonoBehaviour
{
	public LootCrate crate;

	void Awake()
	{
		Subscribe();
	}

	void OnDisable()
	{
		UnSubscribe();
	}

	void Subscribe()
	{
		BossBehaviour.OnBossDestroyed += b => 
		{
			this.SpawnCrate(b.transform);
		};
	}

	void UnSubscribe()
	{
		BossBehaviour.OnBossDestroyed -= b => 
		{
			this.SpawnCrate(b.transform);
		};
	}

	void SpawnCrate(Transform _transform)
	{
        Vector3 newPos = new Vector3(_transform.position.x, _transform.position.y + 5f, _transform.position.z);
		Instantiate(crate, newPos, Quaternion.identity);
	}

	[ContextMenu("Create LootCrate")]
	void SpawnCrate()
	{
		Instantiate(crate, transform.position, Quaternion.identity);
	}

}
