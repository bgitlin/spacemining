﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class LootCrate : MonoBehaviour
{
	[SerializeField]
	LootCratePrototype prototype;

	[SerializeField]
	RangedInt itemDropRange;

	//TODO: Change to unity event and scriptable object
    public static Action OnLootCollected;

	Item GiveLoot(IProbable probability)
	{
		List<float> weights = new List<float>();

		prototype.loots.ForEach( l => weights.Add(l.dropRate));

		List<int> ids = probability.SelectIds( weights,  itemDropRange);

		//TODO: Change from always giving 1st elem
		Item i = prototype.loots[ids[0]].itemPrototype.Clone();

		return i;
	}

	[ContextMenu("Open")]
	void Open()
	{
        FMODUnity.RuntimeManager.PlayOneShot("event:/LootChest");
        Item i = GiveLoot(new RouletteProbable());

		InventoryManager.instance.AddItem(i);

		if(OnLootCollected != null)
		{
			OnLootCollected();
		}

		Instantiate(i.gameObject);
	}

	void OnMouseDown() 
	{
		Open();	
		Destroy(this.gameObject);
	}

	public void OnValidate()
	{
		if(prototype != null && prototype.loots != null)
		{
			if(itemDropRange.end > prototype.loots.Count)
			{
				itemDropRange.end = prototype.loots.Count;
			}
		}
	}
}
