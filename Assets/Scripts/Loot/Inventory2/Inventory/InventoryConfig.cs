﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory/Config")]
public class InventoryConfig : ScriptableObject
{
	public List<string> dontSpawnTags;

	public List<ItemPrototype> startingItems;

	public List<Item> CloneItems()
	{
		List<Item> items = new List<Item>();
		startingItems.ForEach( i => items.Add(i.Clone()));
		return items;
	}
}
