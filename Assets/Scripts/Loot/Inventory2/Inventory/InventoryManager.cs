﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

/// <summary>
/// Each actors has there own instance of an inventory
/// which for our specifications, need to be be pointers
/// to one single shared inventory. They maintain their
/// own invenetories incase this requirement changes
/// </summary>
public class InventoryManager : MonoBehaviour
{
	[SerializeField]
	Inventory _inventoryCollection;

	List<ActorData> _actorDatas;

	[SerializeField]
	InventoryConfig _inventoryConfig;              


	//In for testing, ideally set up event manager 
	//so i dont have to have people reference this
	public static InventoryManager instance;
	public static Action<Item> OnAddedLoot;

	[Space(25)]
	[Header("Test Properties")]
	[SerializeField]
	Actor _actor;


	void Awake()
	{
		instance = this;	
		_actorDatas = new List<ActorData>();	

		if(_actor != null)
		{
			_actor.data.baseStats = LoadStatData(_actor.data.id);
			_actor.Init(_actor.data);
			_actorDatas.Add(_actor.data);
		}
		Load();
	}

	public void AddActor(Actor a)
	{
		_actorDatas.Add(a.data);
	}

	public void AddItem(Item i)
	{
		i.name = i.CreateRandomName(i);

		_inventoryCollection.items.Add(i);

		if(OnAddedLoot != null)
		{
			OnAddedLoot(i);
		}

		_actor.data.Equals(i);
		Save();
	}

	public void UnEquipItem(string actorID, Item i)
	{
		ActorData data = _actorDatas.FirstOrDefault(a => a.id == actorID);
		if(data != null)
		{
			data.UnEquip(i);
		}
	}

	void EquipItem(ActorData a, Item i)
	{
		if(i.equipped && !string.IsNullOrEmpty(a.id))
		{
			//If was equipped by another actor, unequip it from them
			if(!String.Equals(a.id, i.ownerID, StringComparison.CurrentCultureIgnoreCase))
			{
				UnEquipItem(i.ownerID, i);
			}
		}
		a.Equip(i);
	}

	public void EquipItem(string actorId, Item i, bool save=true)
	{

		if(i.equipped)
		{
			//If was equipped by another actor, unequip it from them
			if(!string.IsNullOrEmpty(actorId) && !String.Equals(actorId, i.ownerID, StringComparison.CurrentCultureIgnoreCase))
			{
				UnEquipItem(i.ownerID, i);
			}
		}

		//Get the actorData associated with the id
		ActorData a = _actorDatas.FirstOrDefault( d => d.id == actorId);
	
		if(a != null)
			a.Equip(i);

		if(save)
			Save();

        SoundFXManager.instance.WeaponEquip();
	}

	void CreateActorsFromIds(List<string> actorIDS) 
	{
		actorIDS.Distinct().ToList().ForEach( id => CreateActor(id));
	}

	//TODO: Move out of inventorymanager into ActorFactory
	void CreateActor(string id)
	{
		if(!string.IsNullOrEmpty(id))
		{
			StatData baseStats = LoadStatData(id+"Stats");
			ActorData a = new ActorData()
			{
				baseStats = baseStats,
				id = id
			}; 
			if(!_actorDatas.Contains(a))
				_actorDatas.Add(a);
		}		
	}

	void EquipActorItems(List<ActorData> actorDatas)
	{
		actorDatas.ForEach(a =>
		{
			List<Item> equippedItems = GetActorsItems(a.id);

			equippedItems.ForEach(i =>
			{
				EquipItem(a,i);
			});
		});	
	}

#region Serialization
	[ContextMenu("Save")]
	public void Save()
	{
		string itemJson = JsonUtility.ToJson(_inventoryCollection, true);
		PlayerPrefs.SetString("inventory", itemJson);
		PlayerPrefs.Save();
	}
	
	[ContextMenu("Load")]
	void Load()
	{
		// Trys to load the entire inventory from json
		_inventoryCollection = LoadFromPlayerPrefs<Inventory>("inventory");

		// Inventory is empty
		if(_inventoryCollection == null || _inventoryCollection.items.Where(i => i.ownerID == "player").ToList().Count < 1)
		{
			//TODO: Adds starting inventory to player
			_inventoryCollection = new Inventory(_inventoryConfig);
		}

		//Finds all the unique actors by id
		List<string> actorIDS = _inventoryCollection
								.items
								.Where(i => i.isActor)
								.Select( i => i.name)
								.Distinct()
								.ToList();

		//Look up the actorData associated with all the ids and cache them
		CreateActorsFromIds(actorIDS);

		//For each actorData equip all of there items
		//And the actor in the scene
		EquipActorItems(_actorDatas);
	}

	
	T LoadFromPlayerPrefs<T>(string id)
	{
		string json = PlayerPrefs.GetString(id);
		return JsonUtility.FromJson<T>(json);
	}	

	StatData LoadStatData(string _id)
    {
        string data = PlayerPrefs.GetString(_id+"Stats", "");
		print(data);
        if(!string.IsNullOrEmpty(data))
        {
            return JsonUtility.FromJson<StatData>(data);
        }
        else
        {
            print("Empty");
            return new StatData();
        }
    }
#endregion Serialization

#region Queries

	public Item RequestItemByOwner(string id)
	{
		return _inventoryCollection.items.FirstOrDefault( i => i.ownerID == id);
	}

	//TODO: MOVE ALL THESE TO INVENTORY
	public List<Item> GetFilteredInventory(string filter)
    {
		if(_inventoryCollection != null && _inventoryCollection.items != null)
        	return _inventoryCollection.items.Where(i => i.tags.Any(t => String.Equals(t, filter, StringComparison.CurrentCultureIgnoreCase))).ToList();
		return null;
    }

    public List<Item> GetFilteredInventory(List<string> filters, int count)
    {
        List<Item> filteredItems = new List<Item>();
        filters.ForEach(f => filteredItems.AddRange(GetFilteredInventory(filters, count)));
        return filteredItems;
    }

	public List<Item> GetFilteredInventory(List<string> filters, Predicate<Item> predicate)
    {
        List<Item> filteredLoot = new List<Item>();
        filters.ForEach(f => filteredLoot.AddRange(GetFilteredInventory(f)));
        return filteredLoot.Where(l => predicate(l)).ToList();
    }

    public List<Item>  GetFilteredInventory(string filter, Predicate<Item> predicate)
    {
        return GetFilteredInventory(filter).Where(l => predicate(l)).ToList();
    }

    public List<Item> GetFilteredInventory(string filter, int count, Predicate<Item> predicate)
    {
        return GetFilteredInventory(filter).Where(l => predicate(l)).Take(count).ToList();
    }

	public List<Item> GetActorsItems(string id, string filter=null)
	{
		if(string.IsNullOrEmpty(filter))
			return _inventoryCollection.items
					.Where(i => i.ownerID == id)
					.ToList();

		return _inventoryCollection.items
				.Where(i => i.ownerID ==id && i.tags.Any(t => String.Equals(t, filter, StringComparison.CurrentCultureIgnoreCase))).ToList();
	}

	public ActorData GetActorData(string id)
	{
		return _actorDatas.FirstOrDefault( a => a.id == id);
	}

#endregion Queries
	
}
