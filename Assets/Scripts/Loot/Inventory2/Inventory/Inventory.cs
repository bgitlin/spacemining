﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[Serializable]
public class Inventory
{
	[Header("Inventory Properties")]
	public List<Item> items = new List<Item>();

	public Inventory(){}

	public Inventory(InventoryConfig config)
	{
		this.items = CreateNewInventory(config);
	}

	public Inventory(List<Item> startingItems)
	{
		items.AddRange(startingItems);
	}

	public List<Item> FilterByTag(string filter)
	{
		return items.Where(i => i.tags.Any(t => String.Equals(t, filter, StringComparison.CurrentCultureIgnoreCase))).ToList();
	}

	List<Item> CreateNewInventory(InventoryConfig config)
	{
		List<Item> startingItems = new List<Item>();
		startingItems.AddRange(config.CloneItems());
		return startingItems;	
	}

	Inventory CreateNewInventory(ItemPrototype startWeapon)
	{
		Item startingWeapon = startWeapon.Clone();
		startingWeapon.ownerID = "player";
		startingWeapon.stats = new StatData();
		List<Item> startingItems = new List<Item>(){startingWeapon};
		return new Inventory(startingItems);	
	}


}
