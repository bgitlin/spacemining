﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompanionController : Actor 
{

    [SerializeField]
    NPC _npc;

    protected override void Awake()
    {
        base.Awake();
    }


    public override void Init(ActorData data)
    {
        base.Init(data);
        //TODO: TEMP?
        Item i = InventoryManager.instance.RequestItemByOwner(data.id);
        data.name = i.name;
        data.iconSprite = i.iconSprite;
    }

    //This is TEMP: Remove after npc no longer has stats
    public void SetNPCStats(StatData stats)
    {
        _npc = this.GetComponent<NPC>();
        _npc.data.Stats = stats;
    }

    protected override void Subscribe()
    {
        
    }

    protected override void UnSubscribe()
    {
    
    }
}
