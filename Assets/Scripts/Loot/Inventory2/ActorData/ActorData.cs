﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[Serializable]
public class ActorData
{
	public string id;
	public string name;
	public Sprite iconSprite;

	//These should NOT be serialized
	private Inventory _equippedInventory;
	//They are in for testing 
	//So i can easily see in the inspector
	private List<StatData> _modifiers;
	public List<StatData> Modifiers{get {return _modifiers;}}

	public StatData baseStats;

	public GameObject gameObject;

	public int weaponLimit = 1;

	public ActorData()
	{
		_equippedInventory = new Inventory();
		_modifiers = new List<StatData>();
	}

	Item GetCurrentEquip(string tag)
	{
		return _equippedInventory.items.FirstOrDefault(i => i.tags.Any(t => String.Equals(t, tag, StringComparison.CurrentCultureIgnoreCase)));
	}

	//TODO: Remove currently equipped weapon
	public void Equip(Item i)
	{	
		//If we don't have this item equipped
		if(!_equippedInventory.items.Contains(i))
		{
			List<Item> currentWeapons = _equippedInventory.FilterByTag("weapon");
			//If we do hit the max number of item type, remove old item
			if(currentWeapons.Count >= weaponLimit)
			{
				UnEquip(currentWeapons[0]);
			}
			_equippedInventory.items.Add(i);
			i.equipped = true;
			i.ownerID = id;
			if(!i.isActor)
				_modifiers.Add(i.stats);
		}		
		else 
		{
			UnEquip(i);
		}
		Save();
	}

	public void UnEquip(Item i)
	{
		if(!i.isActor)
			_modifiers.Remove(i.stats);

		_equippedInventory.items.Remove(i);
		i.ownerID = null;
		i.equipped = false;
		Save();
	}

	public void Save()
	{

	}

	public void SpawnItemGameObject()
	{
		_equippedInventory.items.ForEach( i => 
		{
			if(!i.isActor)
				i.InstantiateGameObject();
		});
	}

	public void UpgradeStats(StatData stats)
	{
		
	}
}
