﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public abstract class Actor : MonoBehaviour
{

	[SerializeField]
	[TextArea(1,15)]
	string _json;

	[SerializeField]
	public ActorData data;

	public virtual void Init(ActorData data)
	{
		this.data = data;
		
		if(this.data.gameObject != null)
		{
			Instantiate(this.data.gameObject);
		}	

		Save();//May need to move for companion
	}
	
    protected virtual void Awake()
    {
        Subscribe();
    }

    protected virtual void OnDestroy()
    {
        UnSubscribe();
    }

    protected abstract void Subscribe();

    protected abstract void UnSubscribe();


	void OnDisable()
	{
		Save();
	}

	//TODO: Cache and only update OnItemEquipped event.
    public StatData GetModifiedStats()
    {
        StatData stats = data.baseStats;

        data.Modifiers.ForEach( m =>
        {
            stats = StatData.ModifyStats(stats, m, (a,b) => (a + b));  
        }); 

        return stats;
    }

	public StatData GetBaseStats()
	{
		Debug.Log("\n\t\t+++++++++++++++++++++++++++++++++++++++++++++++ PLAYER STATS "  + data.baseStats+ "\n");
		return data.baseStats;
	}

	[ContextMenu("Save")]
	public void Save()
	{
		if(data != null)
		{
			_json = JsonUtility.ToJson(data, true);
			PlayerPrefs.SetString(data.id, _json);
		}
	}

	[ContextMenu("Calcualte Modified Stats")]
	void ShowTotalStats()
	{
		Debug.Log(this + " " + GetModifiedStats());
	}
}
