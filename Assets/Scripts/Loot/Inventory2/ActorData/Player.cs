﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Player : Actor
{
    [SerializeField]
    PlayerHealth _health;

    [SerializeField]
    PlayerShip _ship;

    public override void Init(ActorData data)
    {
        if(_ship != null)
        {
            _ship.baseStats = GetBaseStats();
        }
    }

    protected override void Subscribe()
    {
        if(_ship != null)
        {
            _ship.ModifiedStats += GetModifiedStats;
        }
    }

    protected override void UnSubscribe()
    {
        if(_ship != null)
        {
            _ship.ModifiedStats -= GetModifiedStats;
        }
    }

    [ContextMenu("Set Stats")]
    void SetStats()
    {
        _ship.baseStats = GetModifiedStats();
    }

}
