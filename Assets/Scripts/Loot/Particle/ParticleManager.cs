﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// TODO: Potentially a temp class. Ideally, we could just have
/// instances of ParticleSystemControllers that can subscribe to whatever 
/// events they want and handle emissions on there own. 
/// </summary>
public class ParticleManager : MonoBehaviour
{
    [SerializeField]
    ParticleSystemController _damageSystem;

    [SerializeField]
    ParticleSystemController _deathSystem;

    [SerializeField]
    ParticleSystemController _levelBlockComplete;
    //TODO: REMOVE : SHOULD SUBSCRIBE TO ALL HEALTH SYSTEM ONDAMAGE TAKEN INSTEAD
    public static ParticleManager instance;

    private void Awake()
    {
        instance = this;
    }

    void Subscribe()
    {
        //TODO: Subscribe To all healthsystems damage/death
    }

    void UnSubscribe()
    {

    }
    
    public void OnDamageTaken(Vector3 position)
    {
         //_damageSystem.Systems.ForEach(p => EmitParticle(p, position));
        EmitParticles(_damageSystem, position);
    }

    public void OnDeath(Vector3 position)
    {
        //_deathSystem.Systems.ForEach(p => EmitParticle(p, position));
        EmitParticles(_deathSystem, position);
    }

    void EmitParticle(ParticleSystem system, Vector3 position, int total=1)
    { 
        var emitParams = new ParticleSystem.EmitParams
        {
            position = position,
            applyShapeToPosition = true
        };

        ParticleSystem.Burst[] bursts = new ParticleSystem.Burst[system.emission.burstCount];
        system.emission.GetBursts(bursts);

        if (bursts.Length > 0)
        {
            foreach (ParticleSystem.Burst burst in bursts)
            {
                system.Emit(emitParams, Mathf.RoundToInt(Random.Range(Mathf.Clamp(burst.count.constantMin, 1, Mathf.Infinity), burst.count.constantMax)));
            }
        }
        else
        {
            system.Emit(emitParams, total);
        }
    }

    void EmitParticles(ParticleSystemController controllers, Vector3 position)
    {
         for(int i=0; i<controllers.Systems.Count;i++)
         {
            EmitParticle(controllers.Systems[i], position);
         }
    }

}
