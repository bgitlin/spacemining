﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Gets a list of all particle systems that are its chidlren
/// and caches them. Used in ParticleManager
/// so we don't have to constantly find the particle systems.
/// </summary>
public class ParticleSystemController : MonoBehaviour 
{
	List<ParticleSystem> _systems;
	public List<ParticleSystem> Systems
	{
		get { return _systems; }
	}

	void Awake()
	{
		_systems = FindChildSystems();
	}

	List<ParticleSystem> FindChildSystems()
	{
		ParticleSystem[] systems = this.GetComponentsInChildren<ParticleSystem>();
		return systems.ToList();
	}
	

}
