﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICollectable
{

	string Name{ get; }

	bool Equippable{ get; }

	void Collect(Inventory l);

} 

