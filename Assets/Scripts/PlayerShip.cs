﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class PlayerShip : MonoBehaviour, IAttacker
{
    public static PlayerShip instance;
    private void Awake()
    {
        instance = this;
    }

    [SerializeField]
    public StatData baseStats;

    public Func<StatData> ModifiedStats;

    public PlayerHealth health;

    public void Initialize()
    {
        health.Initialize();
        UI_HUD.instance.Subscribe();
    }


    //Obsolete, made a wrapper for right now
    public StatData GetModifiedStats()
    {
        return ModifiedStats();
    }

    //Obsolete, made a wrapper for right now
    public StatData GetStatData()
    {
        return ModifiedStats();
    }

}
