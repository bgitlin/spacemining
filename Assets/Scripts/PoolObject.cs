﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolObject : MonoBehaviour {

    public ObjectPool myPool;

    public virtual void EnterPool(ObjectPool _pool)
    {
        myPool = _pool;
        gameObject.SetActive(false);
    }

    public void ExitPool(ObjectPool _pool)
    {
        gameObject.SetActive(true);
        myPool = _pool;
    }

    public virtual void ReturnToMyPool()
    {
        myPool.ReturnToPool(gameObject);
    }
}
