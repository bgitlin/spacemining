﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource : Minable {

    public LevelBlockInfo myBlockInfo;
    
    public override void Initialize()
    {
        minableType = MinableType.resource;
        healthSystem.InitHealth(UpgradeManager.instance.resourceHealthCurve.Evaluate(DataManager.instance.celestrialData.level + 1 + (DataManager.instance.celestrialData.currentMissionIndex * 0.25f)));
        healthSystem.Onkilled += Harvest;
    }

    public override void Destroy()
    {
        base.Destroy();

        float miningEF = PlayerShip.instance.GetModifiedStats().miningEfficiency;
        DataManager.instance.AdjustGold(Mathf.RoundToInt(Random.Range(5, 15) * miningEF ));
        myBlockInfo.RemoveMinable(this);
        gameObject.SetActive(false);
    }

    public void Harvest(IAttacker attacker)
    {
        float miningEF = attacker.GetStatData().miningEfficiency;// PlayerShip.instance.GetStats().miningEfficiency;
        DataManager.instance.AdjustGold(Mathf.RoundToInt(Random.Range(5, 15) * miningEF));
        myBlockInfo.RemoveMinable(this);
        gameObject.SetActive(false);
        UI_HUD.instance.resourceIconManager.CreateIcon(UI_ResourceIconManager.ResourceType.Ore, transform.position);
    }
}
