﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public abstract class Routine : MonoBehaviour
{
	
	[SerializeField]
	protected NPC _owner;

	[SerializeField]
	protected float _routineWaitTime;

	[SerializeField]
	protected LevelBlockInfo _currentBlock;

	protected Coroutine _currentRoutine;

	protected virtual void Awake()
	{
		if(_owner == null)
		{
			_owner = this.GetComponent<NPC>();
		}
	}

	public abstract float CalculateUtility();

	public abstract void Activate();

	public abstract IEnumerator Run(StatData stats);

}
