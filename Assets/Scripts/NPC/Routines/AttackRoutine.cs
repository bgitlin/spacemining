﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackRoutine : Routine 
{
    public LineRenderer lineRenderer;

    public override void Activate()
    {
        throw new System.NotImplementedException();
    }

    public override IEnumerator Run(StatData stats)
    {
        HealthSystem target = _owner.data.targetSystem.FindClosestTarget<HealthSystem>(_owner.data.perceptionSystem.ScanArea(), true);
		while(target.IsAlive())
		{
			_owner.data.attackSystem.Attack(stats.attack,target);
            Display(target.transform);
            yield return new WaitForSeconds(_routineWaitTime);
		}
		yield return null;
	}
	
    public override float CalculateUtility()
    {
        return 5f;
    }

    void Display(Transform target)
    {
        if (lineRenderer != null)
        {
            //TEMP
            lineRenderer.enabled = true;
            lineRenderer.SetPosition(0, this.transform.position);
            lineRenderer.SetPosition(1, target.position);
        }
    }

}
