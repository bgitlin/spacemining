﻿public enum RoutineStatus 
{
	 INACTIVE=0, 
	 ACTIVE=1,
	 COMPLETED=2,
	 FAILED=3 
};