﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRoutine : Routine 
{

	bool _reachedTarget = false;

    public override void Activate()
    {
        if(_owner.data.movementSystem != null)
		{
			_owner.data.movementSystem.Arrived += Arrived;
		}
    }

	void Arrived()
	{
		_reachedTarget = true;
	}

    public override IEnumerator Run(StatData stats)
    {
		Activate();
        if(_owner.data.targetSystem.target != null)
            yield return StartCoroutine(_owner.data.movementSystem.Movement(_owner.data.targetSystem.target.position));
        yield return null;
    }


	public override float CalculateUtility()
    {
		if(_reachedTarget)
        	return 1;
		return 1000;
    }

	
}
