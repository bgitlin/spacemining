﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;

[RequireComponent( typeof(MiningControl))]
public class MineRoutine : Routine, IAttacker
{
	public Minable currentTarget;

	public MiningControl miningController;

    [SerializeField]
    private StatData _stats;

    int _timesToRun;

    WaitForSeconds _mineSpeed;


    protected override void Awake()
    {
        base.Awake();
        this.miningController = this.GetComponent<MiningControl>();
        this.miningController.Init(this);
        LevelManager.OnNextBlock += NextBlock;
    }

    public void Start()
	{
        _stats = this.GetComponent<NPC>().data.Stats;  
    }

	void OnDestroy()
	{
        LevelManager.OnNextBlock -= NextBlock;
        StopAllCoroutines();
		Unsubscribe();
	}

    void Subscribe()
    {
        if (this._currentBlock != null)
        {
            this._currentBlock.OnInitialized += Activate;
            this._currentBlock.OnCompleted += Unsubscribe;
        }
    }

	void Unsubscribe()
	{
        _owner.data.targetSystem.target = null;
        if (this._currentBlock != null)
		{
			this._currentBlock.OnInitialized -= Activate;
			this._currentBlock.OnCompleted -= Unsubscribe;
		}			
         
	}

    public void NextBlock(LevelBlockInfo newBlock)
    {
        Unsubscribe();
        StopAllCoroutines();
        this._currentBlock = newBlock;
        Subscribe();
        Activate();
    }


	[ContextMenu("Activate")]
	/// <summary>
	/// Selects a resource to mine
	/// </summary>
	/// <param name="targetSys"></param>
    public override void Activate()
    {
        _owner.data.targetSystem.target = null;
        if (_currentBlock != null && gameObject.activeInHierarchy)
		{	
			if(_currentRoutine != null)
				StopCoroutine(_currentRoutine);		

			if( _currentBlock.myMinables.Count > 0)
			{
				if(currentTarget != null)
				{
					currentTarget.healthSystem.OnDeath -= Activate;
				}

                if (gameObject.activeSelf)
                {
                    this.currentTarget = FindMinable();

                    if (currentTarget != null && this.gameObject.activeSelf)
                    {
                        //Whenever the currentTarget dies, choose a new one
                        currentTarget.healthSystem.OnDeath += Activate;
                        _timesToRun = UnityEngine.Random.Range(10, 20);
                        _currentRoutine = StartCoroutine(Run(_stats));
                    }
                }
			}
		}
    }


	Minable FindMinable()
	{
        if(_currentBlock != null)
        {
            List<Minable> minables = _currentBlock.myMinables.Where(m => m.gameObject.activeSelf).ToList();
            Minable min = _owner.data.targetSystem.FindClosestTarget<Minable>(minables, true);
            return min;
        }
        return null; 
	}


    public override IEnumerator Run(StatData stats)
    {
        //while(currentTarget.healthSystem.health.value > 0)
        _mineSpeed = new WaitForSeconds(_routineWaitTime);
        while (_timesToRun >= 0 )
		{
            if (currentTarget != null)
            {
                miningController.StartMining(_owner.data.targetSystem.FuzzyTarget(currentTarget.transform.position), stats.attack, false);
                yield return _mineSpeed;
            }
            _timesToRun -= 1;
            miningController.StopMining();
        }

        this.Activate();
    }


    #region Debugging

    [ContextMenu("FIND CLOSEST TARGET")]
    void TEST_FindClosestTarget()
    {
        this.currentTarget = FindMinable();
    }

    public StatData GetStatData()
    {
        return this._stats;
    }

    public override float CalculateUtility()
    {
        throw new NotImplementedException();
    }

    #endregion
}
