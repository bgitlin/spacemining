﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class NPC : MonoBehaviour
{

	public NPCData data;

	void Awake()
	{
        //this.data.targetSystem.controller = this.transform;
      //  data.healthSystem.health.OnDeath += Destroy;

        if(data.healthSystem != null)
        {
            data.healthSystem.InitHealth();
            data.healthSystem.OnDeath += Destroy;
        }

        StartCoroutine(ProcessRoutine());

    }

	IEnumerator ProcessRoutine() 
	{
        for(; ;)
        {
            RunRoutine();
            yield return new WaitForSeconds(1f);
        }
    }

	[ContextMenu("Run Routine")]
	public void RunRoutine()
	{
		if(data.currentRoutine != null) StopCoroutine(data.currentRoutine);
		data.currentRoutine = StartCoroutine(RunRoutine(SelectRoutine()));
	}

	Routine SelectRoutine()
	{
		data.routine = data.routines.OrderByDescending(r => r.CalculateUtility()).FirstOrDefault();
		return data.routine;
	}

    IEnumerator RunRoutine(Routine r)
    {
        if(r != null)
        {
            yield return r.Run(data.Stats);
        }
        yield return null;        
    }

    protected virtual void Destroy()
    {
        this.gameObject.SetActive(false);
        StopAllCoroutines();
        Destroy(this.gameObject);
    }

    #region	Debugging
    [ContextMenu("SAVE")]
	public void Save()
	{
		string json = JsonUtility.ToJson(this);
		Debug.Log(json);
	}

	#endregion Debugging
}
