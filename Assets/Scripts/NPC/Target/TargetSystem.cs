﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class TargetSystem : MonoBehaviour
{
    [HideInInspector]
    public Transform controller;

    [SerializeField]
    public Transform target;

    [SerializeField]
    Target _data;

    [SerializeField]
    bool _showTarget;

    private void Awake()
    {
        controller = this.transform;
    }

    public T AquireTarget<T>()
    {
        return default(T);
        //Ray cast for what's directly in front of the controller
        //check to see if target is a priority
        //choose the highest priority target as the current target 
    }

    private void Update()
    {
        if(target != null)
        {
            LookAtTarget();
        }
    }

    public void LookAtTarget()
    {
        LookAtTarget(this.target);
    }

    //TODO: Fix rotation on one axis
    public void LookAtTarget(Transform target)
    {
        if (controller != null && target != null)
        {

            Vector3 lookPos = target.position - transform.position;
            lookPos.y = 0;
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _data.lookSpeed);
        }
    }

    public T FindClosestTarget<T>(List<T> targets, bool setTarget=false) where T : MonoBehaviour
    {
        if (targets != null)
        {
            Vector3 position = controller.position;
            T target = targets
                .OrderBy(o => (o.transform.position - position).sqrMagnitude)
                .FirstOrDefault().transform
                .GetComponent<T>();

            if(setTarget)
            {
                this.target = target.transform;
            }

            return target;
        }
        return null;

    }

    public Vector3 FuzzyTarget(Vector3 targetPos)
    {
        Vector3 random = UnityEngine.Random.insideUnitCircle * _data.shotSpread;

        return new Vector3(targetPos.x += random.x, targetPos.y += random.y, targetPos.z);
    }

    private void OnDrawGizmos()
    {
        if (_showTarget)
        {

            if (this.target != null)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawCube(this.target.position, new Vector3(1, 1, 1));

                Gizmos.color = Color.black;
                Gizmos.DrawLine(this.target.position, this.target.position);

                Gizmos.color = Color.green;
                Gizmos.DrawCube(this.target.position, new Vector3(1, 1, 1));
            }

        }

    }

}
