﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerceptionSystem : MonoBehaviour
{
    public Perception data;

    public bool IsInLayerMask(GameObject obj, LayerMask mask)
    {
        return ((mask.value & (1 << obj.layer)) > 0);
    }

    public List<HealthSystem> ScanArea()
    {
        List<HealthSystem> colliders = new List<HealthSystem>();

        data.hitColliders = Physics.OverlapSphere(transform.position, data.FovRadius, data.targetMask);
        if (data.hitColliders != null)
        {
            for (int i = 0; i < data.hitColliders.Length; i++)
            {
                //check if the object is within the angle bounds
                if (Vector3.Angle(transform.forward, (data.hitColliders[i].transform.position - transform.position)) < data.AngleSpread)
                {
                    colliders.Add(data.hitColliders[i].transform.gameObject.GetComponent<HealthSystem>());
                }
            }
        }
        return colliders;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        //Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
        Gizmos.DrawWireSphere(transform.position + Vector3.up, data.FovRadius);

        Gizmos.color = Color.red;
        //Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
        Gizmos.DrawWireSphere(transform.position + Vector3.up, data.AttackDistance);

        Quaternion leftRayRotation = Quaternion.AngleAxis(-data.AngleSpread / 2, Vector3.up);
        Quaternion rightRayRotation = Quaternion.AngleAxis(data.AngleSpread / 2, Vector3.up);
        Vector3 leftBound = leftRayRotation * transform.forward;
        Vector3 rightBound = rightRayRotation * transform.forward;

        Gizmos.color = Color.green;
        Gizmos.DrawRay(this.transform.position + Vector3.up, this.transform.forward * data.FovRadius);

        if (data.AngleSpread < 360f)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawRay(transform.position + Vector3.up, leftBound * data.FovRadius);
            Gizmos.DrawRay(transform.position + Vector3.up, rightBound * data.FovRadius);
        }

        if (data.hitColliders != null)
        {
            for (int i = 0; i < data.hitColliders.Length; i++)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(data.hitColliders[i].gameObject.transform.position, transform.position);
            }
        }
    }
}
