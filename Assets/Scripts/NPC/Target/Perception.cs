﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Perception 
{


    [Range(0, 100)]
    [SerializeField]
    private float _fovRadius;
    public float FovRadius
    {
        get { return _fovRadius; }
        set
        {
            if (value >= 0) _fovRadius = value;
            else _angleSpread = 0;
        }
    }

    [Range(0, 360)]
    [SerializeField]
    private float _angleSpread;
    public float AngleSpread
    {
        get { return _angleSpread; }
        set
        {
            if (value >= 0 && value <= 360) _angleSpread = value;
            else _angleSpread = 0;
        }
    }


    [Range(0, 100)]
    [SerializeField]
    private float _attackDistance;
    public float AttackDistance
    {
        get { return _attackDistance; }
        set
        {
            if (value >= 1 && value <= this.FovRadius) _attackDistance = value;
            else if (_attackDistance > this.FovRadius) _attackDistance = this.FovRadius;
            else _attackDistance = 1;
        }
    }

    public LayerMask targetMask;  //The layer which targets can be seen on
    public Collider[] hitColliders; //DEBUGGING

}
