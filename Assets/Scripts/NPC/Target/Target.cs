﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Target 
{
    [Range(0.5f, 5f)]
    public float shotSpread = 1.75f;

    public float lookSpeed = 5.5f;
}
