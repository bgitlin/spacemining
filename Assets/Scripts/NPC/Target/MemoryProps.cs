﻿using System;
using UnityEngine;

/// <summary>
/// Perception Mananges the Memory of 
/// what the AI has "experienced" 
/// Everytime an AI percieves one of their Targets
/// Memory is updated and then their AI Goals are 
/// changed whether or not they remember that the player was 
/// in their face a second ago or not.
/// </summary>
[Serializable]
public class MemoryProps
{

    //The time when the target was last percieved. 
    //We use this when determining whether or not an
    //AI can "REMEMBER" when a target was percieved, or 
    //if they are stupid and forget immediately  when
    //the target is out of sight (out of sight, out of mind)
    [Tooltip("The Time the target was last sensed")]
    [HideInInspector]
    public float targetVisibleTime;

    //How long the target has been visible to this object
    //We set this to the Time.time when the target enters
    //this objects FOV then we get how long they have been visible
    // Time.time - whenTargetBecameVisible
    [Tooltip("The Time the target has been visible to this object")]
    [HideInInspector]
    public float whenTargetBecameVisible;

    //Helpr variable - might be useful
    [HideInInspector]
    public float whenTargetLastVisible;

    //The position the targer was last percieved at. This can
    //be used to help target player if they go out of view
    [HideInInspector]
    public Vector3 lastVisibleTargetPos;

    //Set true if the target is within sight
    [HideInInspector]
    public bool targetInFOV;

    //Set true if there is nothing that
    //would obstuct an attack between
    //this AI and their target
//    [HideInInspector]
    public bool targetAttackable;

    public LayerMask myLayer;
}