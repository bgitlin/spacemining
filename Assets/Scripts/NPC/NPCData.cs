﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class NPCData  
{
	#region Data
	[SerializeField]
	StatData _stats;
    
	public StatData Stats
	{
		get{ return _stats;}
        set { _stats = value; }
	}

    public Inventory inventory;

	public string name;

	#endregion Data

	#region Systems

	public AttackSystem attackSystem;

	public TargetSystem targetSystem;

	public HealthSystem healthSystem;

	public MovementSystem movementSystem;

	public PerceptionSystem perceptionSystem;

	public List<Routine> routines;

	public Routine routine;

	public Coroutine currentRoutine;

	#endregion System

}
