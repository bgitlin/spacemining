﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AttackSystem
{

	public void Attack(IAttacker attacker, IAttackable target)
	{
		target.TakeDamage(attacker);
	}

	public void Attack(float damage, IAttackable target)
	{
		target.TakeDamage(damage);
	}

	public void Attack(float damage, HealthSystem target)
	{
		target.TakeDamage(damage);
	}

}
