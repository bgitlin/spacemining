﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAttackable
{
	void TakeDamage(IAttacker attacker);

    void TakeDamage(float damage);
    
}