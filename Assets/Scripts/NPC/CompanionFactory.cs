﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

//TODO: MAKE ABSTRACT FACTORY to make generic factories for NPCS, really no need for this class
//but in right now for testing
public class CompanionFactory : MonoBehaviour 
{
    public List<GameObject> currentCompanions;

    [SerializeField]
    Transform _companionContainer;

    [SerializeField]
    GameObject _companionControllerPrefab;

    [SerializeField]
    List<Waypoint> _wayPoints;

    private void Start()
    {
        SpawnCompanions();
    }

    [ContextMenu("Spawn")]
    void SpawnCompanions()
    {
        List<Item> companions = InventoryManager.instance.GetFilteredInventory("companion", (l => l.equipped));

        companions.ForEach(c =>
        {
            GameObject companionGameObject = Instantiate(_companionControllerPrefab, _companionContainer);
            companionGameObject.name = c.name;
            CompanionController controller = companionGameObject.AddComponent<CompanionController>();

            //TODO: MAKE THEM USE ITEM STATS AS WELL
            controller.SetNPCStats(c.stats);
            companionGameObject.GetComponentInChildren<GenerateCompanion>().GenrateShip(c.name);
            companionGameObject.GetComponentInChildren<HealthSystem>().OnDeath += () => {UpdateList(companionGameObject);};
            AssignWayPoint(companionGameObject);
            currentCompanions.Add(companionGameObject);
        });
    }
    
    void UpdateList(GameObject g)
    {
        if(currentCompanions.Contains(g))
        {
            currentCompanions.Remove(g);
        }
    }

    void AssignWayPoint(GameObject e)
    {
        Waypoint point = _wayPoints.Where(w => w.Occupied() == false).FirstOrDefault();

        if (point != null)
        {
            point.AddOccupier<MonoBehaviour>(e.GetComponent<NPC>(), t => e.transform.position = t.position);
        }
    }

}