﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HealthSystem : MonoBehaviour
{
    [SerializeField]
	Health _health;
    [SerializeField]
    protected HealthView _healthView;

    
    public Action OnDeath;
    public Action<IAttacker> Onkilled;
    public Action OnDamageTaken;

    public void InitHealth(float health, Action Death=null, Action<IAttacker> Killed=null, Action DamageTaken=null)
    {
        _health.value = health;
        OnDeath += Death;
        Onkilled += Killed;
        OnDamageTaken += DamageTaken;
        InitView();
    }


    public void InitHealth()
    {
        InitView();
    }

    public void InitHealth(float health, Action DamageTaken)
    {
        InitHealth(health, null, null, DamageTaken);
    }

    public void InitHealth(float health, Action<IAttacker> Killed)
    {
        InitHealth(health, null, Killed, null);
    }

    void InitView()
    {
        if(_healthView != null)
        {
            _healthView.Init(_health.value);
            OnDamageTaken += () => { _healthView.SetPoints(_health.value); };
            OnDeath += _healthView.Stop;
        }
    }

    public void TakeDamage(float damage, float defense = 0)
    {
        _health.value -= damage - defense;

        if(OnDamageTaken != null)
        {
            OnDamageTaken();
        }

        if (_health.value <= 0)
        {
            Death();
        }
    }

    public void TakeDamage(IAttacker attacker, float defense = 0)
    {
        TakeDamage(attacker.GetStatData().attack, defense);
        if(!IsAlive())
        {
            if(Onkilled != null)
            {
                Onkilled(attacker);
            }
        }
    }

    public bool IsAlive()
    {
        return _health.value > 0 ? true : false;
    }

    protected virtual void Death()
	{
		if(OnDeath != null)
        {
            OnDeath();
        }
        StopAllCoroutines();
	}
}
