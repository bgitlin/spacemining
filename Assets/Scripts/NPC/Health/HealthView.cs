﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthView : MonoBehaviour 
{

    public Canvas myHealthCanvas;
    public Image hitPointsBar;

    float _totalValue;

    public void Init(float health)
    {
        hitPointsBar.fillAmount = 1;
        _totalValue = health;
    }

    public void SetPoints(float currentValue)
    {
        hitPointsBar.fillAmount = Map.map(currentValue, 0, _totalValue, 0, 1);
    }

	public void Stop()
	{
		myHealthCanvas.enabled = false;
	}
	
}
