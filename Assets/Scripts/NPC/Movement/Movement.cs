﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Movement  
{
	[Range(0,1)]
	public float speed; 

	[Range(0,10)]
	public float stoppingDistance;

	public Coroutine currentRoutine;

}
