﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementSystem : MonoBehaviour 
{

	[SerializeField]
	Movement _data;

	public Action Arrived;

	public void Move(Transform target)
	{
		if(_data.currentRoutine != null) StopCoroutine(_data.currentRoutine);

		_data.currentRoutine = StartCoroutine(Movement(target.position, _data.speed,  _data.stoppingDistance));
	}

	public IEnumerator Movement (Vector3 target)
    {
		yield return StartCoroutine(Movement(target, _data.speed, _data.stoppingDistance));
	}

	public IEnumerator Movement (Vector3 target, float speed, float stoppingDistance)
    {
        while(Vector3.Distance(transform.position, target) > stoppingDistance)
        {
            transform.position = Vector3.Lerp(transform.position, target, speed * Time.deltaTime);
            yield return null;
        }
		if(Arrived != null)
		{
			Arrived();
		}
		Debug.Log("Arrived");
    }

}
