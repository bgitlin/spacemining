﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipAnimator : MonoBehaviour
{
    //Idle
    public float speed;
    public float rotXIncrement;
    public float centeringMultiplier;
    public float maxAngle;
    public float minAngle;

    //Hit
    public float hitSpeed;
    public float moveDistance;
    public float straightHitAngle;

    public Vector3 centerHitRot;
    public Vector3 leftHitRot;
    public Vector3 rightHitRot;

    public Vector3 posMultiplier;
    public Vector3 enemyPosition; //For testing

    private float time;
    private float prevXNoise;
    private float prevChangeXNoise;

    private Vector3 startPos;
    private Vector3 startRot;

    private float randX;
    private float randY;
    private float randZ;

    private float timeAdjust;

    private Coroutine idle;
    private Coroutine hit;

    private void Start()
    {
        randX = Random.Range(0f, 1f);
        randY = Random.Range(0f, 1f);
        randZ = Random.Range(0f, 1f);

        time = 0f;
        timeAdjust = 0f;
        prevXNoise = 0f;
        prevChangeXNoise = 0f;

        startRot = transform.rotation.eulerAngles;
        startPos = transform.position;

        maxAngle = startRot.x + maxAngle;
        minAngle = startRot.x + minAngle;

        idle = StartCoroutine(IdleAnim());
    }

    public void Hit(Vector3 _enemyPos)
    {
        StopCoroutine(idle);
        hit = StartCoroutine(HitAnim(enemyPosition));
        timeAdjust += 2 / hitSpeed;
    }

    public void Hit()
    {
        StopCoroutine(idle);
        hit = StartCoroutine(HitAnim(enemyPosition));
        timeAdjust += 2 / hitSpeed;
    }

    private IEnumerator HitAnim(Vector3 _enemyPos)
    {
        Vector3 initPos = transform.position;
        Vector3 initRot = transform.rotation.eulerAngles;
        Vector3 midRot = initRot;

        Vector3 moveVect = NormalizeVector(initPos - _enemyPos);

        Vector3 midPos = startPos + (moveVect * moveDistance);

        float hitAngle = CalcAngle(new Vector2(moveVect.x, moveVect.z * -1));

        if (hitAngle < straightHitAngle && hitAngle > (-1 * straightHitAngle))
        {
            midRot = centerHitRot;
        }
        else if (hitAngle > straightHitAngle)
        {
            midRot = rightHitRot;
        }
        else
        {
            midRot = leftHitRot;
        }

        float timer = 0;
        while (timer < 1)
        {
            timer += Time.deltaTime * hitSpeed;

            float tempX = Mathf.LerpAngle(initRot.x, midRot.x, timer);
            float tempY = Mathf.LerpAngle(initRot.y, midRot.y, timer);
            float tempZ = Mathf.LerpAngle(initRot.z, midRot.z, timer);

            transform.rotation = Quaternion.Euler(new Vector3(tempX, tempY, tempZ));

            transform.position = Vector3.Lerp(startPos, midPos, timer);

            yield return 0f;
        }

        transform.rotation = Quaternion.Euler(midRot);

        timer = 0;
        while (timer < 1)
        {
            timer += Time.deltaTime * hitSpeed;

            float tempX = Mathf.LerpAngle(midRot.x, initRot.x, timer);
            float tempY = Mathf.LerpAngle(midRot.y, initRot.y, timer);
            float tempZ = Mathf.LerpAngle(midRot.z, initRot.z, timer);

            transform.rotation = Quaternion.Euler(new Vector3(tempX, tempY, tempZ));
            transform.position = Vector3.Lerp(midPos, initPos, timer);

            yield return 0f;
        }

        transform.rotation = Quaternion.Euler(startRot);
        transform.position = initPos;

        idle = StartCoroutine(IdleAnim());
        StopCoroutine(hit);

        yield return 0f;
    }

    private Vector3 NormalizeVector(Vector3 _vector)
    {
        return _vector / Mathf.Sqrt(Mathf.Pow(_vector.x, 2) + Mathf.Pow(_vector.y, 2) + Mathf.Pow(_vector.z, 2));
    }

    private float CalcAngle(Vector2 _vector)
    {
        return Mathf.Acos(_vector.y / Mathf.Sqrt(Mathf.Pow(_vector.x, 2) + Mathf.Pow(_vector.y, 2))) * Mathf.Rad2Deg * Mathf.Sign(_vector.x);
    }

    private IEnumerator IdleAnim()
    {
        while (true)
        {
            float perlinNoiseX = Map.map(Mathf.PerlinNoise(Time.time * speed - timeAdjust * speed + randX, randX), 0, 1, -1, 1);
            float perlinNoiseY = Map.map(Mathf.PerlinNoise(randY, Time.time * speed - timeAdjust * speed + randY), 0, 1, -1, 1);
            float perlinNoiseZ = Map.map(Mathf.PerlinNoise(randZ, Time.time * speed - timeAdjust * speed + randZ), 0, 1, -1, 1);
            float changeXNoise = perlinNoiseX - prevXNoise;

            Vector3 currentRot = transform.rotation.eulerAngles;

            if (currentRot.z > 180)
            {
                currentRot.z -= 360;
            }

            if (changeXNoise < 0f)
            {
                if (currentRot.z < startRot.z)
                {
                    currentRot.z += centeringMultiplier * rotXIncrement;
                }
                else
                {
                    currentRot.z += rotXIncrement;
                }

                if (currentRot.z > maxAngle)
                {
                    currentRot.z = maxAngle;
                }
            }
            else if (changeXNoise > 0f)
            {
                if (currentRot.z > startRot.z)
                {
                    currentRot.z -= centeringMultiplier * rotXIncrement;
                }
                else
                {
                    currentRot.z -= rotXIncrement;
                }

                if (currentRot.z < minAngle)
                {
                    currentRot.z = minAngle;
                }
            }

            if (currentRot.z < 0)
            {
                currentRot.z += 360;
            }

            transform.rotation = Quaternion.Euler(currentRot);
            transform.position = new Vector3(startPos.x + perlinNoiseX * posMultiplier.x, startPos.y + perlinNoiseY * posMultiplier.y, startPos.z + perlinNoiseZ + posMultiplier.z);

            prevXNoise = perlinNoiseX;
            prevChangeXNoise = changeXNoise;

            yield return 0f;
        }
    }
}
