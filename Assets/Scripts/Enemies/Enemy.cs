﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Enemy 
{
	public float attackTime = 1f;
	public float attackDamage = 2f;
}
