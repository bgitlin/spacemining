﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyFactory : Minable 
{
	[SerializeField]
	EnemyController _enemyPrefab;

	[SerializeField]
	List<Waypoint> _wayPoints;

	Coroutine _enemyRoutine;

    [SerializeField]
    Transform _enemyContainer;

	[SerializeField]
	float _spawnTime = 0.5f;

	[SerializeField]
	int _maxEnemies;

    List<EnemyController> enemies = new List<EnemyController>();

    [SerializeField]
    LevelBlockInfo _myBlockInfo;

    public override void Initialize()
    {
        this.healthSystem.InitHealth(2 * UpgradeManager.instance.healthCurve.Evaluate(DataManager.instance.celestrialData.level + 1 + (DataManager.instance.celestrialData.currentMissionIndex * 0.25f)));
        _enemyRoutine = StartCoroutine(CreateEnemies(_maxEnemies));
        base.Initialize();
    }

    public override void Destroy()
    {
        if(_enemyRoutine != null)
            StopCoroutine(_enemyRoutine);

        if(_myBlockInfo != null)
            _myBlockInfo.RemoveMinable(this);
            
        gameObject.SetActive(false);
        base.Destroy();
    }

    [ContextMenu("Create Enemy")]
	public GameObject CreateEnemy()
	{
        GameObject enemy = Instantiate(_enemyPrefab.gameObject, _enemyContainer.position, Quaternion.identity);
        enemy.transform.SetParent(_enemyContainer);
		return enemy;
	}

    void AssignWayPoint(EnemyController e)
    {
        Waypoint point = _wayPoints.Where(w => w.Occupied() == false).FirstOrDefault();

        if(point != null)
        {
            point.AddOccupier<EnemyController>(e, e.RunRoutines);
        }
    }

    EnemyController Spawn()
    {
        EnemyController enemy = CreateEnemy().GetComponent<EnemyController>();

        enemy.Initialize(_myBlockInfo);

        AssignWayPoint(enemy);

        enemy.healthSystem.OnDeath += () =>
        {
            EnemyDied(enemy);
            _wayPoints.ForEach(w => w.RemoveOccupier(enemy)); //TODO: not most efficient, probably make dict 
        };

        if(_myBlockInfo != null)
            _myBlockInfo.myMinables.Add(enemy);

        return enemy;
    }
    
    void EnemyDied(EnemyController e)
    {
        enemies.Remove(e);
        if(this.healthSystem.IsAlive())
        {
            _enemyRoutine = StartCoroutine(CreateEnemies(_maxEnemies));
        }
    }

	IEnumerator CreateEnemies(int amt)
	{
		while(enemies.Count <= amt)
		{
            enemies.Add(Spawn());
            yield return new WaitForSeconds(_spawnTime);
        }
	}
	
}
