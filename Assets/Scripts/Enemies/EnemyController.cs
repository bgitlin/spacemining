﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : Minable
{
	//Probably move this to minable
 	public LevelBlockInfo myBlockInfo;

	[SerializeField]
	Enemy _data;

	[SerializeField]
	NPC _npcController;

	Coroutine _attackRoutine;

	bool _canAttack = true;

	public override void Initialize (LevelBlockInfo block)
	{
        this._npcController = this.GetComponent<NPC>();

        this.myBlockInfo = block; 

		_data = new Enemy();
		
		_data.attackDamage = 0.75f * (UpgradeManager.instance.damageCurve.Evaluate(DataManager.instance.celestrialData.level + (DataManager.instance.celestrialData.currentMissionIndex * 0.25f)));

        healthSystem.InitHealth(UpgradeManager.instance.healthCurve.Evaluate(DataManager.instance.celestrialData.level + 1 + (DataManager.instance.celestrialData.currentMissionIndex * 0.25f)));

		_attackRoutine = StartCoroutine(Attack());
	
		base.Initialize();
    }

	public void RunRoutines(Transform target)
	{
        _npcController.data.targetSystem.target = target;
		_npcController.RunRoutine();
	}

	public IEnumerator Attack()
	{
		while(_canAttack)
		{
			//_attackSystem.Attack(this, _targetSystem.AquireTarget<HealthSystem>());
			yield return new WaitForSeconds(_data.attackTime);
		}
	}

    public StatData GetStatData()
    {
		return this._npcController.data.Stats;
    }

	public override void Destroy()
	{
        base.Destroy();
		if(myBlockInfo != null)
		{
            myBlockInfo.RemoveMinable(this);
		}
		Destroy(this.gameObject);

	}
}
