﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour 
{

	[SerializeField]
	int _occupylimit;

	[SerializeField]
	IList _occupiers;

	public bool Occupied()
	{
		if(_occupiers == null || _occupiers.Count <= 0) return false;
		return true;
	}

	public void AddOccupier<T>(T t, Action<Transform> OnOccupierAdded=null) where T : MonoBehaviour
	{
		if(_occupiers == null) _occupiers = new List<T>();

		if(_occupiers.Count <= _occupylimit)
		{
			_occupiers.Add(t);

			if(OnOccupierAdded != null)
			{
				OnOccupierAdded(this.transform);
			}
		}
	}

    public void RemoveOccupier<T>(T t) where T : MonoBehaviour
    {
        if (_occupiers != null && _occupiers.Contains(t))
        {
            _occupiers.Remove(t);
        }
    }

	public T GetOccupier<T>(T t) where T : MonoBehaviour
	{
		if(_occupiers != null &&  _occupiers.Contains(t))
		{
			return t;
		}
		return default(T);
	}

}
