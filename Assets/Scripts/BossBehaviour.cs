﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossBehaviour : Minable {

    public LevelBlockInfo myBlockInfo;

    public float timeBetweenDamage = 5;
    public float waitAfterFire;
    public float timeBetweenShots;
    private int shotsPerVolley = 5;

    public GameObject bossShot;

    private WaitForSeconds shotWait;
    private WaitForSeconds waitAfterVolley;
    
    private bool firing;
    

    private float halfFireArc = 60;
    public Transform fireLoc;
    
    public GameObject breakParticles;
    public Animator animator;

    //TODO: Don't love this being static, maybe 
    //maybe setting up a global Event Manager would be better? -ash
    public static Action<BossBehaviour> OnBossDestroyed;
    

    public override void Initialize()
    {
        print("init boss");
        minableType = MinableType.boss;

        StartShootRoutine();
        LootCrate.OnLootCollected = () => 
        {
            GameManager.instance.MissionComplete();
        };

        healthSystem.InitHealth(UpgradeManager.instance.healthCurve.Evaluate(DataManager.instance.celestrialData.level + 1 + (DataManager.instance.celestrialData.currentMissionIndex * 0.25f)) * 10);
        base.Initialize();
    }

    public override void TakeDamage(IAttacker attacker)
    {
        base.TakeDamage(attacker);
        animator.SetTrigger("GetHit");
    }

    public override void DamageTook()
    {
        
    }

    public override void Destroy()
    {
        base.Destroy();
        GameManager.instance.levelManager.bossBeat = true;
        myBlockInfo.RemoveMinable(this);
        print("Boss Defeated.");
        animator.SetTrigger("Die");
        //gameObject.SetActive(false);
        OnBossDestroyed(this);
        OnBossDestroyed = null;
        this.GetComponent<Collider>().enabled = false;
    }

    private void BossDefeated()
    {
        StopCoroutine(shootRoutine);
    }

    IEnumerator shootRoutine;
    private void StartShootRoutine()
    {
        shootRoutine = ShootRoutine();
        StartCoroutine(shootRoutine);
    }
    private IEnumerator ShootRoutine()
    {
        float timer = timeBetweenDamage;

        while (true)
        {
            timer += Time.deltaTime;

            if (timer > timeBetweenDamage)
            {
                StartCoroutine(FireRoutine(shotsPerVolley));

                yield return new WaitForSeconds(3f);
                timer = 0;

                yield return 0f;
            }

            yield return 0f;
        }
    }

    private IEnumerator FireRoutine(int _amt)
    {
        if (firing)
        {
            yield break;
        }

        firing = true;
        int fireTimes = 0;

        Quaternion myQ = Quaternion.Euler(0, 0, 0);

        while (fireTimes < _amt)
        {
            fireTimes++;
            Shoot(Map.map(fireTimes, 0, (_amt - 1), -halfFireArc, halfFireArc));
            yield return new WaitForSeconds(0.25f);
            yield return 0f;
        }
        firing = false;
    }

    private void Shoot(float rotation)
    {
        animator.SetTrigger("Attack");
        fireLoc.rotation = Quaternion.identity;
        Quaternion myQ = Quaternion.Euler(0, rotation - halfFireArc, 0);
        fireLoc.localRotation = myQ;
        GameObject tempBossShot = Instantiate(bossShot, fireLoc.position, fireLoc.rotation)as GameObject;
        myBlockInfo.myMinables.Add(tempBossShot.GetComponent<Minable>());
        tempBossShot.GetComponent<BossShot>().myBlockInfo = myBlockInfo;
        tempBossShot.GetComponent<BossShot>().myRB.AddForce(fireLoc.transform.forward * 2.5f, ForceMode.Impulse);
    }
    
}
