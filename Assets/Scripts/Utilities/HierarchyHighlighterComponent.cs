﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HierarchyHighlighterComponent : MonoBehaviour
{
    public bool highlight = false;
    public Color color;
}