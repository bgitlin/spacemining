﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine; 

[CreateAssetMenu]
public class BlockDataList:ScriptableObject {

	public List< PlanetBlockData > planets;
	public List< EnemyData > enemies;
	public List< ColorPalette > colors;
}
