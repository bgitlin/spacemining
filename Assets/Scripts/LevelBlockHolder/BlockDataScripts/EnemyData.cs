﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemyData : ScriptableObject {

	public GameObject boss;
	
	public List< GameObject > minions;

}
