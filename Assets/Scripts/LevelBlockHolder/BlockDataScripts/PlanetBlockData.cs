﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlanetBlockData : ScriptableObject {

	public CelestrialData.GenerationType planet;

	public List<GameObject> levelBlocks;
	public List<GameObject> endBlocks;
	
}
