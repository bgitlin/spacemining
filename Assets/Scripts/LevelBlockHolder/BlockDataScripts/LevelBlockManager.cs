﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine; 

public class LevelBlockManager:MonoBehaviour {

	public class EnvironmentData {
		
		private PlanetBlockData planet; 

		private EnemyData enemies; 

		private ColorPalette colors; 

		public void SetPlanet(PlanetBlockData _planet) {
			planet = _planet; 
		}

		public void SetEnemies(EnemyData _enemies) {
			enemies = _enemies; 
		}

		public void SetColors(ColorPalette _colors) {
			colors = _colors; 
		}

		public PlanetBlockData GetPlanet() {
			return planet; 
		}

		public EnemyData GetEnemies() {
			return enemies; 
		}

		public ColorPalette GetColors() {
			return colors; 
		}
	}

	public enum BlockType {
		empty, 
		minable, 
		enemy, 
		boss, 
	}

	public BlockDataList blockDataList; 

	private EnvironmentData environmentData; 

	public void SetEnvironmentData() {
		environmentData.SetPlanet(blockDataList.planets[Random.Range(0, blockDataList.planets.Count)]); 
		environmentData.SetEnemies(blockDataList.enemies[Random.Range(0, blockDataList.enemies.Count)]); 
		environmentData.SetColors(blockDataList.colors[Random.Range(0, blockDataList.colors.Count)]); 
	}

	public void SetEnvironmentData(EnvironmentData _environmentData) {
		environmentData = _environmentData; 
	}

	public void SetEnvironmentData(PlanetBlockData _planet, EnemyData _enemies, ColorPalette _colors) {
		environmentData.SetPlanet(_planet); 
		environmentData.SetEnemies(_enemies); 
		environmentData.SetColors(_colors); 
	}

	public List < GameObject > GenerateLevel(List < BlockType > _blockList) {
		List < GameObject > level = null; 

		for (int i = 0; i < _blockList.Count; i++) {
			switch (_blockList[i]) {
				case BlockType.empty:
					level.Add(EmptyBlock()); 
					break; 
				case BlockType.minable:
					level.Add(MinableBlock()); 
					break; 
				case BlockType.enemy:
					level.Add(MinionBlock()); 
					break; 
				case BlockType.boss:
					level.Add(BossBlock()); 
					break; 
			}
		}

		return level; 
	}

	private GameObject EmptyBlock() {
		
		GameObject levelBlock = new GameObject("EmptyBlock"); 
		PlanetBlockData planetData = environmentData.GetPlanet(); 

		GameObject environment = planetData.endBlocks[Random.Range(0, planetData.endBlocks.Count)]; 
		environment.transform.SetParent(levelBlock.transform); 

		return levelBlock; 
	}

	private GameObject MinableBlock() {
		
		GameObject levelBlock = new GameObject("MinableBlock"); 
		PlanetBlockData planetData = environmentData.GetPlanet(); 

		GameObject environment = planetData.endBlocks[Random.Range(0, planetData.endBlocks.Count)]; 
		environment.transform.SetParent(levelBlock.transform); 

		return levelBlock; 
	}

	private GameObject MinionBlock() {
		
		GameObject levelBlock = new GameObject("EnemyBlock"); 
		PlanetBlockData planetData = environmentData.GetPlanet(); 
		EnemyData enemyData = environmentData.GetEnemies(); 

		GameObject environment = planetData.endBlocks[Random.Range(0, planetData.endBlocks.Count)]; 
		environment.transform.SetParent(levelBlock.transform); 

		GameObject boss = enemyData.minions[Random.Range(0, enemyData.minions.Count)]; 
		boss.transform.SetParent(levelBlock.transform); 

		return levelBlock; 
	}

	private GameObject BossBlock() {
		
		GameObject levelBlock = new GameObject("BossBlock"); 
		PlanetBlockData planetData = environmentData.GetPlanet(); 
		EnemyData enemyData = environmentData.GetEnemies(); 

		GameObject environment = planetData.endBlocks[Random.Range(0, planetData.endBlocks.Count)]; 
		environment.transform.SetParent(levelBlock.transform); 

		GameObject boss = enemyData.boss; 
		boss.transform.SetParent(levelBlock.transform); 

		return levelBlock; 
	}
}
