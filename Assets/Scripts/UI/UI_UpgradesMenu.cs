﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_UpgradesMenu : UI_Menu
{
    
    //TODO: Maybe remove reference and shoot out event when we have
    //global event manager and aditive scene loading? 
    [SerializeField]
    UI_InventoryMenu _inventoryUI;

    // [SerializeField]
    // LootView _currentWeapon;

    public UI_StatItem[] stats;
    
    public virtual void OpenMenu(StatData _inputStats, Inventory _inputInv)
    {
        if (!gameObject.activeInHierarchy)
        {
            UpdateStats(_inputStats);
            UpdateCurrentWeapon(_inputInv);
            gameObject.SetActive(true);
        }
        else
        {
            CloseMenu();
        }
    }
    
    public override void OpenMenu()
    {
        base.OpenMenu();
        UpdateStats();
        UpdateCurrentWeapon();
    }

    public override void CloseMenu()
    {
        base.CloseMenu();
    }
    

    void UpdateCurrentWeapon(Inventory _inventory = null)
    {
        //TODO: Get the players current Weapon, not just a random weapon
        List<Item> items = InventoryManager.instance.GetActorsItems("player", "weapon");
        UI_WeaponItem currentWeaponView = GetComponentInChildren<UI_WeaponItem>();
        currentWeaponView.onclick +=
        i =>
        {
            if(_inventoryUI != null && UI_MenuManager.instance.currentMenu != _inventoryUI)
            {
                UI_MenuManager.instance.OpenMenu(_inventoryUI);
            }
        };
        if(items.Count > 0)
        {
            Item currentWeapon = items[0];
    
            if(currentWeapon != null)
            {
                if (currentWeapon != null)
                {
                    currentWeaponView.Display
                    (
                        currentWeapon.name,
                        currentWeapon.stats,
                        currentWeapon.iconSprite
                    );
                }
            }
        }
    }

    public void UpdateStats(StatData _data = null)

    {
        StatData inputStatData;
        if (_data == null)
        {
            inputStatData = PlayerShip.instance.baseStats;
        }
        else
        {
            inputStatData = _data;
        }

        int cost;

        stats[0].level.text = inputStatData.attack.ToString();
        if (stats[0].cost != null)
        {
            cost = Mathf.RoundToInt(UpgradeManager.instance.costCurve.Evaluate(inputStatData.attack));
            stats[0].cost.text = cost.ToString();
            stats[0].button.interactable = cost <= DataManager.instance.currentGold;
        }

        stats[1].level.text = inputStatData.defense.ToString();
        if (stats[1].cost != null)
        {
            cost = Mathf.RoundToInt(UpgradeManager.instance.costCurve.Evaluate(inputStatData.defense));
            stats[1].cost.text = cost.ToString();
            stats[1].button.interactable = cost <= DataManager.instance.currentGold;
        }

        stats[2].level.text = inputStatData.miningEfficiency.ToString();
        if (stats[2].cost != null)
        {
            cost = Mathf.RoundToInt(UpgradeManager.instance.costCurve.Evaluate(inputStatData.miningEfficiency));
            stats[2].cost.text = cost.ToString();
            stats[2].button.interactable = cost <= DataManager.instance.currentGold;
        }


        stats[3].level.text = inputStatData.luck.ToString();
        if (stats[3].cost != null)
        {
            cost = Mathf.RoundToInt(UpgradeManager.instance.costCurve.Evaluate(inputStatData.luck));
            stats[3].cost.text = cost.ToString();
            stats[3].button.interactable = cost <= DataManager.instance.currentGold;
        }
    }
}
