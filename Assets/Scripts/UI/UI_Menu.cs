﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Menu : MonoBehaviour
{
    public RectTransform canvas;
    public RectTransform panel;
    
    public virtual void OpenMenu()
    {
        panel.gameObject.SetActive(true);
        Vector2 start = new Vector2(panel.anchoredPosition.x, -1 * (panel.anchoredPosition.y + panel.sizeDelta.y));
        Vector2 target = panel.anchoredPosition;
        AnimationManager.instance.Slide(panel, start, target, 2);
    }
    public virtual void CloseMenu()
    {
        panel.gameObject.SetActive(false);
    }
}
