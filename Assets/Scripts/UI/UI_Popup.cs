﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UI_Popup : MonoBehaviour 
{

	public Text text;

	public Button close;

	public Action OnClick;

	void Awake()
	{
		OnClick += CloseMenu;

		close.onClick.AddListener(() => OnClick());
	}

	void OnDestroy()
	{
		OnClick -= CloseMenu;
	}
	

	void CloseMenu()
	{
		this.gameObject.SetActive(false);
		Destroy(this.gameObject);
	}

	public void SetNotification(string notification)
	{
		if(text != null)
		{
			text.text = notification;
		}
	}
}
