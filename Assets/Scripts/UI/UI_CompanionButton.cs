﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class UI_CompanionButton : MonoBehaviour
{

    [SerializeField]
    Text nameText;

    public Item companion;

    public Action onClick;

    public static Action<Item> sendCompanion;


    void Awake() 
    {
        Subscribe();
    }   

    void OnDisable()
    {
        UnSubscribe();
    }

    void Subscribe()
    {
        UI_CompanionInventoryMenu.OnSwappedCompanions += OnUpdateCompanion;
    }

    void UnSubscribe()
    {
        UI_CompanionInventoryMenu.OnSwappedCompanions -= OnUpdateCompanion;
    }
    public void OpenMenu()
    {
        if (sendCompanion != null)
        {
            sendCompanion(companion);
        }
        if (onClick != null)
        {
            onClick();
        }
    }

    public void DisplayName(string name)
    {
        if(nameText != null)
        {
            nameText.text = name;
        }
    }

    void OnUpdateCompanion(Item previous, Item newItem)
    {
        if(companion.name == previous.name)
        {
            companion = newItem;
            DisplayName(newItem.name);
        }
    }

}
 