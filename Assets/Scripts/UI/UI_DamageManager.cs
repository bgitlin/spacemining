﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_DamageManager : MonoBehaviour
{
    public static UI_DamageManager instance;

    private void Awake()
    {
        instance = this;
        StartCoroutine(Timer());
    }

    public GameObject canvas;
    public DamageText damageText;  
    public List<UIMinable> minables;

    private void Start()
    {
        minables = new List<UIMinable>();
    }

    public void DisplayDamage(float _damage, MiningControl _attacker, Minable _minable)
    {
        if (_minable.minableType != Minable.MinableType.voxel)// && _minable.minableType != Minable.MinableType.resource)
        {
            UpdateDamage(_damage, _attacker, _minable);
        }
    }

    private void DisplayText(UIMinable _minable)
    {
        DamageText instance = Instantiate(damageText);
        Vector2 cameraPosition = Camera.main.WorldToScreenPoint(_minable.minablePos);

        instance.transform.SetParent(canvas.transform, false);
        instance.transform.position = new Vector2(cameraPosition.x + Screen.width * Random.Range(-.05f, .05f), cameraPosition.y + Screen.width * Random.Range(-.05f, .05f));
        instance.SetText(Mathf.Round(_minable.damageTaken).ToString());
        instance.SetColor(new Color(1f, Random.Range(0f, .5f), 0f, 1f));
    }

    private void UpdateDamage(float _damage, MiningControl _attacker, Minable _minable)
    {
        UpdateMinable(_minable, _damage);
    }

    private void UpdateMinable(Minable _minable, float _damage)
    {
        int j = 0;
        bool duplicate = false;
        while ((j < minables.Count) && !duplicate)
        {
            if (minables[j].minable == _minable)
            {
                minables[j].damageTaken += _damage;
                duplicate = true;
            }
            j++;
        }
        
        if (!duplicate)
        {
            minables.Add(new UIMinable(_damage, _minable));
        }
    }

    private IEnumerator Timer()
    {
        while (true)
        {
            float timer = 0f;

            while (timer < .5f)
            {
                timer += Time.deltaTime;
                yield return 0f;
            }
            PrintList();
            minables.Clear();
            yield return 0f;
        }
    }

    private void PrintList()
    {
        for (int j = 0; j < minables.Count; j++)
        {
            DisplayText(minables[j]);
        }
    }
}

public class UIMinable
{
    public float damageTaken;
    public Minable minable;
    public Vector3 minablePos;

    public UIMinable(float _damageTaken, Minable _minable)
    {
        damageTaken = _damageTaken;
        minable = _minable;
        minablePos = minable.transform.position;
    }
}