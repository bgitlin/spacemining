﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

public class UI_InventoryMenu : UI_Menu 
{
    public Action<Item> OnItemEquipped;

    [SerializeField]
    protected List<string> _tagsToShow;

    [SerializeField]
    Transform _lootContainer;

    [SerializeField]
    protected List<Item> _items;

    [SerializeField]
    protected List<UI_WeaponItem> _itemViews;

    [SerializeField]
    protected UI_WeaponItem _itemPrefab;

    //TODO: Set currentActor
    [SerializeField]
    public string currentActorId = "player";

    private void OnEnable()
    {
        OnItemEquipped += UpdateInventory;
    }
    void OnDisable()
    {
        OnItemEquipped -= UpdateInventory;
    }

    public override void OpenMenu()
	{
        base.OpenMenu();
        _items
            .AddRange(InventoryManager.instance
            .GetFilteredInventory(_tagsToShow, (i => !i.equipped))
            .ToList());     
            
        CreateLoot(_items);     
    }

	public override void CloseMenu()
	{       
        base.CloseMenu();
        _items.Clear();
        //TODO: Temp for now, destroys and recreates everytime, might cache later
        DestroyLootUI();
	}

    protected void CreateLoot(List<Item> items)
    {
        for(int i=0; i<items.Count; i++)
        {
            UI_WeaponItem view = Instantiate(_itemPrefab,_lootContainer);
            _itemViews.Add(view);
            view.Display(_items[i], OnItemEquipped);
        }        
    }

    void UpdateInventory(Item i)
    {
        InventoryManager.instance.EquipItem(currentActorId,i);    
        _itemViews.ForEach( v => v.UpdateColor());
    }

    protected void DestroyLootUI()
    {
        for(int i=0;i<_itemViews.Count;i++)
        {
            Destroy(_itemViews[i].gameObject);
        }
        _itemViews.Clear();
    } 
}
