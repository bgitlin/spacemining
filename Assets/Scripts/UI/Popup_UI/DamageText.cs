﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageText : MonoBehaviour
{
    public Text damageText;
    public CanvasGroup canvasGroup;
    public AnimationCurve upCurve;
    public AnimationCurve downCurve;

    public float upSpeed;
    public float downSpeed;

    private void Awake()
    {
        StartCoroutine(AnimationRoutine(upSpeed, downSpeed));
    }

    public void SetText(string _text)
    {
        float damage = float.Parse(_text);
        int firstNum = 0;
        int secondNum = 0;

        if (damage >= 1000 && damage < 10000)
        {
            firstNum = (int)Mathf.Floor(damage / 1000);
            secondNum = (int)Mathf.Floor((damage / 100) % 10);
            _text = firstNum.ToString() + "." + secondNum.ToString() + "k";
        }

        damageText.text = _text;
    }

    public void SetColor(Color color)
    {
        damageText.color = color;
    }

    private IEnumerator AnimationRoutine(float _upSpeed, float _downSpeed)
    {
        float timer = 0;
        float randX = Screen.width * Random.Range(-.1f, .1f);
        float randY = Screen.height * Random.Range(.08f, .12f);

        Vector2 startPos = damageText.rectTransform.anchoredPosition;
        Vector2 midPos = startPos + new Vector2(randX, randY);
        Vector2 endPos = startPos + new Vector2(randX * 1.5f, 0f);

        Color initialColor = damageText.color;

        while (timer < 1f)
        {
            timer += Time.deltaTime * _upSpeed;
            damageText.rectTransform.anchoredPosition = Vector2.Lerp(startPos, midPos, upCurve.Evaluate(timer));
            yield return 0f;
        }

        timer = 0;
        while (timer < 1f)
        {
            timer += Time.deltaTime * _downSpeed;
            damageText.rectTransform.anchoredPosition = Vector2.Lerp(midPos, endPos, downCurve.Evaluate(timer));
            canvasGroup.alpha = Mathf.Lerp(1f, 0f, downCurve.Evaluate(timer));
            yield return 0f;
        }

        Destroy(gameObject);
    }
}
