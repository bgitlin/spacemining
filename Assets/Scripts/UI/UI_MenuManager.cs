﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_MenuManager : MonoBehaviour
{
    public static UI_MenuManager instance;
    private void Awake()
    {
        instance = this;
    }

    public UI_Menu currentMenu;
    private UI_Menu previousMenu;

    public void OpenMenu(UI_Menu _menu)
    {
        if (currentMenu == _menu)
        {
            CloseMenu();
            SoundFXManager.instance.CloseMenu();
        }
        else
        {
            CloseMenu();
            _menu.OpenMenu();
            currentMenu = _menu;
            SoundFXManager.instance.OpenMenu();
        }
    }

    public void CloseMenu()
    {
        if(currentMenu != null)
        {
            currentMenu.CloseMenu();
            previousMenu = currentMenu;
            currentMenu = null;
        }
    }

    public void OpenPreviousMenu()
    {
        print("openingPreviousMenu");
        UI_Menu cachedCurrent = currentMenu;
        previousMenu.OpenMenu();
        print(previousMenu.ToString());
        currentMenu.CloseMenu();
        print(currentMenu.ToString());
        currentMenu = previousMenu;
        previousMenu = cachedCurrent;
        SoundFXManager.instance.OpenMenu();
    }
}
