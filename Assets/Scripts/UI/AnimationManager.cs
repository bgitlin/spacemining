﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationManager : MonoBehaviour
{
    public static AnimationManager instance;
    void Awake()
    {
        instance = this;
    }
    
    public float animationSpeed;
    public AnimationCurve easeInCurve;
    public AnimationCurve bounceCurve;
    private float bpm = 10;

    public void RandomScale(RectTransform[] _rectTransform)
    {
        for (int i = 0; i < _rectTransform.Length; i++)
        {
			StartCoroutine(ScaleRoutine(_rectTransform[i], Random.value*0.25f, 0f, 1f));
        }
    }

    public void Scale(RectTransform _rectTransform, float _waitTime, float _startScale, float _endScale, float _speed = 1f)
    {
        StartCoroutine(ScaleRoutine(_rectTransform, _waitTime, _startScale, _endScale, _speed));
    }

    public void Slide(RectTransform _rect, Vector2 _start, Vector2 _target, float _speed = 1)
    {
        StartCoroutine(SlideRoutine(_rect, _start, _target, _speed));
    }

    public IEnumerator ScaleRoutine (RectTransform _rectTransform, float _waitTime, float _startScale, float _endScale, float speed = 1f)
    {
        _rectTransform.gameObject.SetActive(true);
        _rectTransform.localScale = Vector3.one * _startScale;
        Animator animator = _rectTransform.GetComponent<Animator>();
        if (animator != null)
        {
            animator.enabled = false;
        }
        
        yield return new WaitForSeconds(_waitTime);

        float timer = 0;
        while (timer < 1)
        {
            timer += Time.deltaTime * speed;
            _rectTransform.localScale = Vector3.one * Mathf.Lerp(_startScale, _endScale, easeInCurve.Evaluate(timer));
            yield return 0f;
        }

        if (animator != null)
        {
            animator.enabled = true;
        }
    }
    
    public IEnumerator SlideRoutine (RectTransform _rect, Vector2 _start, Vector2 _target, float _speed = 1)
    {
        float timer = 0;

        while (timer < 1)
        {
            timer += Time.deltaTime * animationSpeed * _speed;
            _rect.anchoredPosition = Vector2.Lerp(_start, _target, easeInCurve.Evaluate(timer));
            yield return 0f;
        }

        _rect.anchoredPosition = _target;
    }

    public IEnumerator ScrollScale(RectTransform rect, CanvasGroup canvas, RectTransform _icon, RectTransform _content)
    {
        float scale = 1;

        while (rect.gameObject.activeInHierarchy)
        {
            float currentPos = Mathf.Abs(rect.offsetMin.x + _content.offsetMin.x);
            scale = Map.map(currentPos, 0, 300, 1, 0);
            _icon.localScale = Vector3.one * scale;
            canvas.alpha = Mathf.Clamp(scale, 0.25f, 1f);
            yield return 0f;
        }
    }
    public IEnumerator ScrollFade(RectTransform _rect, RectTransform _content, CanvasGroup _canvas)
    {
        float alpha = 1;
        while (_rect.gameObject.activeInHierarchy)
        {
            alpha = Map.map(Mathf.Abs(_rect.offsetMin.x + _content.offsetMin.x), 0, 100, 1, 0);
            _canvas.alpha = Mathf.Clamp(alpha, 0, 1f);
            yield return 0f;
        }
    }

    public void ScaleAndRotate(RectTransform[] _rect)
    {
        for (int i = 0; i < _rect.Length; i++)
        {
            int direction = i == _rect.Length - 1 ? -1:1;
            StartCoroutine(ScaleAndRotateRoutine(_rect[i], Random.Range(0.5f, 1.5f) * i, direction));
        }
    }

    IEnumerator ScaleAndRotateRoutine(RectTransform _rect, float _offset, int _direction)
    {
        float currentOffset = _offset;

        while (true)
        {
            _rect.transform.localScale = Vector3.one * Map.map(Mathf.Sin((Time.time + currentOffset) * 2), -1f, 1f, 0.95f, 1.15f);
            _rect.transform.Rotate(new Vector3(0,0,0.2f * _direction));
            yield return 0f;
        }
    }

    public void ScaleAndFade(RectTransform[] _rect, Image[] _image, float _start, float _target)
    {
        for (int i = 0; i < _rect.Length; i++)
        {
            StartCoroutine(ScaleAndFadeRoutine(_rect[i], _image[i], _start, _target * Random.Range(0.75f, 1f)));
        }
    }

    IEnumerator ScaleAndFadeRoutine(RectTransform _rect, Image _image, float _start, float _target)
    {
        float timer = 0;
        float speed = Random.Range(0.8f, 1.2f);
        int direction = Random.value > 0.5f ? -1 : 1;
        _rect.gameObject.SetActive(true);

        while (timer < 1)
        {
            timer += Time.deltaTime;
            _rect.transform.localScale = Vector3.one * Map.map(easeInCurve.Evaluate(timer*speed), 0, 1, _start, _target);
            _rect.transform.Rotate(new Vector3(0, 0, 0.4f * speed * direction));
            _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, easeInCurve.Evaluate(timer));
            yield return 0f;
        }

        _rect.gameObject.SetActive(false);
    }

    public IEnumerator ScaleAndFadeRoutine(Image _image, float _startScale, float _endScale, float _startFade, float _endFade, float _speed=1)
    {
        float timer = 0;
        _image.rectTransform.gameObject.SetActive(true);

        while (timer < 1)
        {
            timer += Time.deltaTime * _speed;
            _image.rectTransform.transform.localScale = Vector3.one * Map.map(easeInCurve.Evaluate(timer), 0, 1, _startScale, _endScale);
            _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, Map.map(easeInCurve.Evaluate(timer), 0, 1, _startFade, _endFade));
            yield return 0f;
        }
    }

    public void FadeIn(CanvasGroup _canvas, float _speed)
    {
        StartCoroutine(FadeInRoutine(_canvas, _speed));
    }

    public void FadeIn(Image _image, float _speed)
    {
        StartCoroutine(FadeInRoutine(_image, _speed));
    }

    IEnumerator FadeInRoutine(CanvasGroup _canvas, float _speed)
    {
        float timer = 0;
        while(timer < 1)
        {
            timer += Time.deltaTime * _speed;
            _canvas.alpha = easeInCurve.Evaluate(timer);
            yield return 0f;
        }
    }

    IEnumerator FadeInRoutine(Image _image, float _speed)
    {
        float timer = 0;
        while (timer < 1)
        {
            timer += Time.deltaTime * _speed;
            _image.color= new Color(_image.color.r, _image.color.g, _image.color.b, easeInCurve.Evaluate(timer));
            yield return 0f;
        }
    }

    public void RandomScaleBounce(RectTransform[] _rects)
    {
        StartCoroutine(ScaleBounceRoutine(_rects));
    }

    IEnumerator ScaleBounceRoutine(RectTransform[] _rects)
    {
        float timer = 0;
        int index = 0;

        while (true)
        {
            if (timer > 1)
            {
                timer = 0;
                index = Random.Range(0, _rects.Length);
            }

            timer += Time.deltaTime * (bpm/60);
            _rects[index].transform.localScale = Vector3.one * bounceCurve.Evaluate(timer);

            yield return 0f;
        }
    }

    public void RandomScaleBounce(RectTransform _rect)
    {
        StartCoroutine(ScaleBounceRoutine(_rect));
    }

    IEnumerator ScaleBounceRoutine(RectTransform _rect)
    {
        float timer = 0;

        while (true)
        {
            if (timer > 1)
            {
                timer = 0;
            }

            timer += Time.deltaTime * (bpm / 60);
            _rect.transform.localScale = Vector3.one * bounceCurve.Evaluate(timer);

            yield return 0f;
        }
    }

    public void IdleScale(RectTransform _rect, float _speed, float _scale)
    {
        StartCoroutine(IdleScaleRoutine(_rect, _speed, _scale));
    }

    public IEnumerator IdleScaleRoutine(RectTransform _rect, float _speed, float _scale)
    {
        float scale = 1;
        while (true)
        {
            scale = Map.map(Mathf.Sin(Time.time * _speed), -1, 1, 0.9f, 1.05f * _scale);
            _rect.localScale = Vector3.MoveTowards(_rect.localScale, Vector3.one * scale, Time.deltaTime);
            yield return 0f;
        }
    }

    public void InfiniteScrollAndBounce (RectTransform[] _rects, int _direction)
    {
        for (int i =0; i < _rects.Length; i++)
        {
            StartCoroutine(InfiniteScrollAndBounceRoutine(_rects[i], _direction));
        }
    }

    IEnumerator InfiniteScrollAndBounceRoutine(RectTransform _rect, int _direction)
    {
        float timer = 0;

        while (true)
        {
            if (timer > 1)
            {
                timer = 0;
            }
            timer += Time.deltaTime * 0.25f;

            _rect.anchoredPosition = new Vector2(Map.map(timer, 0, 1, 0, _rect.sizeDelta.x * _direction), _rect.anchoredPosition.y);


            yield return 0f;
        }
    }

    public void ScrollMaterial(Renderer _renderer, int _direction, float _acceleration, float _waitTimer)
    {
        StartCoroutine(ScrollMaterialRoutine(_renderer, _direction, _acceleration, _waitTimer));
    }

    IEnumerator ScrollMaterialRoutine(Renderer _renderer, int _direction, float _acceleration, float _waitTimer)
    {
        float timer = 0;
        float speed = 0;

        yield return new WaitForSeconds(_waitTimer);
        while (true)
        {
            timer += Time.deltaTime * speed;
            speed = Mathf.MoveTowards(speed, 0.25f, Time.deltaTime * _acceleration);

            var offset = new Vector2(timer * _direction, 0);
            _renderer.material.SetTextureOffset("_MainTex", offset);
            yield return 0f;
        }
    }

    public void FadeIn(Image _image, float _start, float _end, float _speed = 1)
    {
        StartCoroutine(FadeInRoutine(_image, _start, _end, _speed));
    }

    public IEnumerator FadeInRoutine(Image _image, float _start, float _end, float _speed = 1)
    {
        float timer = 0;
        _image.gameObject.SetActive(true);
        while (timer < 1)
        {
            timer += Time.deltaTime * _speed;
            _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, Map.map(easeInCurve.Evaluate(timer), 0, 1, _start, _end));
            yield return 0f;
        }
    }
    
    public IEnumerator ScaleAndFadeLoopRoutine(Image _image, float _start, float _target, float _speed = 1, float delay = 0)
    {
        yield return new WaitForSeconds(delay);
        float timer = 0;
        _image.gameObject.SetActive(true);

        while (true)
        {
            timer += Time.deltaTime * _speed;
            if (timer > 1) timer = 0;
            _image.rectTransform.transform.localScale = Vector3.one * Map.map(easeInCurve.Evaluate(timer), 0, 1, _start, _target);
            _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, bounceCurve.Evaluate(timer));
            yield return 0f;
        }
    }

    public IEnumerator ScaleAndFadeRoutine(Image _image, float _start, float _target, float _speed = 1)
    {
        float timer = 0;
        _image.gameObject.SetActive(true);

        while (timer <= 1)
        {
            timer += Time.deltaTime * _speed;
            _image.rectTransform.transform.localScale = Vector3.one * Map.map(easeInCurve.Evaluate(timer), 0, 1, _start, _target);
            _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, bounceCurve.Evaluate(timer));
            yield return 0f;
        }

        _image.gameObject.SetActive(false);
    }

    public IEnumerator RotateRoutine(RectTransform _rect, int _direction)
    {
        while (true)
        {
            _rect.transform.Rotate(new Vector3(0, 0, 0.2f * _direction));
            yield return 0f;
        }
    }
    
    public IEnumerator ResizeObj(Transform _transform, float _startScale, float _targetScale, float _speed = 1f)
    {
        float timer = 0f;

        while (timer < 1)
        {
            timer += Time.deltaTime * _speed;
            _transform.localScale = Vector3.LerpUnclamped(Vector3.one * _startScale, Vector3.one * _targetScale, easeInCurve.Evaluate(timer));
            yield return 0f;
        }
        _transform.localScale = _targetScale * Vector3.one;

        if (_targetScale == 0)
        {
            _transform.gameObject.SetActive(false);
            _transform.localScale = Vector3.one;
        }
    }

    public IEnumerator ResizeRectObj(RectTransform _rectTransform, float _startScale, float _targetScale, float _speed = 1f)
    {
        float timer = 0f;

        while (timer < 1)
        {
            timer += Time.deltaTime * _speed;
            _rectTransform.localScale = Vector3.LerpUnclamped(Vector3.one * _startScale, Vector3.one * _targetScale, easeInCurve.Evaluate(timer));
            yield return 0f;
        }
        _rectTransform.localScale = _targetScale * Vector3.one;

        if (_targetScale == 0)
        {
            _rectTransform.gameObject.SetActive(false);
            _rectTransform.localScale = Vector3.one;
        }
    }

    public IEnumerator ScaleAndBounceRoutine(RectTransform _rectTransform, float _startScale, float _endScale, float _speed = 1f)
    {
        float timer = 0;
        while(timer < 1)
        {
            timer += Time.deltaTime * _speed;
            _rectTransform.localScale = Vector3.LerpUnclamped(Vector3.one * _startScale, Vector3.one * _endScale, bounceCurve.Evaluate(timer));
            yield return 0f;
        }
    }

    public void FadeInCanvasGroup(CanvasGroup _group, float _start, float _end, float _speed = 0)
    {
        StartCoroutine(FadeInCanvasGroupRoutine(_group, _start, _end, _speed));
    }
    private IEnumerator FadeInCanvasGroupRoutine(CanvasGroup _group, float _start, float _end, float _speed)
    {
        float timer = 0;

        while (timer < 1)
        {
            timer += Time.deltaTime * _speed;
            _group.alpha = Map.map(easeInCurve.Evaluate(timer), 0, 1, _start, _end);
            yield return 0f;
        }
    }
}
