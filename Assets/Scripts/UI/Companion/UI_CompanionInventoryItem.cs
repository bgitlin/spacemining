﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_CompanionInventoryItem : MonoBehaviour 
{

    [SerializeField]
    UI_UpgradeItem _upgraderUI;

    public Action<UI_CompanionInventoryItem,Item> OnEquipped;

    [SerializeField]
    Button _inventoryButton;

    [SerializeField]
    Button _equipButton;

    [SerializeField]
    Text _equipButtonText;

    public Item item;

    public void Init(Item i , Action<string> onInventory)
    {
        item = i;
        _upgraderUI.SetUpgradeButton(() => UpdateStats(i));
        _upgraderUI.UpdateInfo(i.level);
        _upgraderUI.SetStats(i.stats);

        if (_equipButton != null)
        {
            EquipStatus(i.equipped);

            _equipButton.onClick.AddListener(() =>
           {
               if(OnEquipped != null)
               {
                   OnEquipped(this, i);
               }
           });
        }

        if (_inventoryButton != null)
        {
            Item currentWeapon = InventoryManager.instance.RequestItemByOwner(i.name);
            if(currentWeapon != null)
            {
                _inventoryButton.GetComponentInChildren<Text>().text = currentWeapon.stats.ToString();
                _inventoryButton.image.sprite = currentWeapon.iconSprite;
            }

            if(onInventory != null)
            {
                _inventoryButton.onClick.AddListener( () => onInventory(i.name));
            }
        }
    }

    void UpdateStats(Item i)
    {
        if (UpgradeManager.instance.CanPurchase(i.level))
        {
            UpgradeManager.instance.PurchaseCompanionUpgrade(i.level);
            i.Upgrade();
            _upgraderUI.SetStats(i.stats);
            _upgraderUI.UpdateInfo(i.level);
            SoundFXManager.instance.UpgradePurchased();
        }
        else
        {
            SoundFXManager.instance.UpgradeCantPurchase();
        }
    }

    void OnDestroy()
    {
        _equipButton.onClick.RemoveAllListeners();
    }

    public void EquipStatus(bool state)
    {
        if (_equipButtonText != null && _equipButtonText.text != null)
        {
            string text;

            if(state)
            {
                text = "Equipped";
            }
            else
            {
                text = "Unequipped";
            }

            _equipButtonText.text = text;
        }
    }

}
