﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class UI_CompanionHUD : UI_Menu
{

    [SerializeField]
    UI_CompanionButton _companionButtonPrefab;

    List<UI_CompanionButton> _companionButtons = new List<UI_CompanionButton>();

    [SerializeField]
    UI_Menu _menuToOpen;

    [SerializeField]
    Transform _container;

    //TODO: TEMP: This should probably be stored in json config 
    int _maxCompanions = 3;

    void Start()
    {
        CreateButtons();
    }

    void GetAvailableCompanionSlots()
    {
        
        //Gets X companions which are already equipped
        List<Item> companions = InventoryManager.instance.GetFilteredInventory("companion", _maxCompanions, l => l.equipped).ToList();

        //If we didn't hit the max # companions, assign new ones from loot
         if (companions.Count <= _maxCompanions)
         { 
             companions.AddRange(InventoryManager.instance.GetFilteredInventory("companion", Mathf.Abs(companions.Count - _maxCompanions), l => !l.equipped));
         }
              
         companions.ForEach(c => 
         {
             Debug.Log("Equipping to player " + c.name);
             if(!c.equipped)
                InventoryManager.instance.EquipItem("player", c);
             Debug.Log(c.name + " " + c.equipped);

             CreateCompanionSlots(c);
         });
    }

    public void CreateCompanionSlots(Item c)
    {
        UI_CompanionButton button = Instantiate(_companionButtonPrefab, _container) as UI_CompanionButton;

        button.companion = c;

        button.DisplayName(c.name);

        button.onClick += SelectCompanion;  

        _companionButtons.Add(button);
    }

    public void SelectCompanion()
    {
        UI_MenuManager.instance.OpenMenu(_menuToOpen);
    }

    void Cleanup()
    {
        for (int i = 0; i < _companionButtons.Count; i++)
        {
            Destroy(_companionButtons[i].gameObject);
        }
        _companionButtons.Clear();
    }

    #region Debugging

    [ContextMenu("Create Companions")]
    public void CreateButtons()
    {
        Cleanup();
        GetAvailableCompanionSlots();
    }

    #endregion

}
