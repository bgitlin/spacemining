﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UI_CompanionInventoryMenu : UI_InventoryMenu
{

    //TODO: TEMP: This is the loot item which we want to sort to the top
    Item firstLoot;

    [SerializeField]
    UI_InventoryMenu _subInventory;

    [SerializeField]
    List<UI_CompanionInventoryItem> _companionItems;

    [SerializeField]
    UI_CompanionHUD _hud;
    
    public static Action<Item, Item> OnSwappedCompanions;


    private void Awake()
    {
        UI_CompanionButton.sendCompanion = CompanionInventoryMenu;
    }

    private void CompanionInventoryMenu(Item c)
    {
        firstLoot = c;
        OnItemEquipped += l =>
        {
            UI_MenuManager.instance.OpenMenu(_subInventory);
        };
    }

    public override void OpenMenu()
    {
        _items.Clear();

        if (firstLoot != null)
            _items.Insert(0, firstLoot);

        base.OpenMenu();

        _itemViews.ForEach(l =>
        {
            UI_CompanionInventoryItem c = l.gameObject.GetComponent<UI_CompanionInventoryItem>();
            _companionItems.Add(c);
            c.OnEquipped += EquippedChange;
            c.Init(l.item, s => 
            {
                _subInventory.currentActorId = l.item.name; 
                UI_MenuManager.instance.OpenMenu(_subInventory);
            });
        });
    }

    public override void CloseMenu()
    {
       _companionItems.Clear();
        base.CloseMenu();
    }

    void EquippedChange(UI_CompanionInventoryItem caller, Item i)
    {
        if(firstLoot.name != i.name)
        {
            List<UI_CompanionInventoryItem> companionViews = new List<UI_CompanionInventoryItem>();
            
            companionViews.AddRange(_companionItems);
            
            companionViews.Remove(caller);

            companionViews.ForEach(views =>
            {
                InventoryManager.instance.UnEquipItem("player", views.item);
                views.EquipStatus(false);
                
            });

        
            InventoryManager.instance.EquipItem("player", i);

            caller.EquipStatus(i.equipped);

            if(OnSwappedCompanions != null)
            {
                OnSwappedCompanions(firstLoot,i);
            }
            firstLoot = i;

            SoundFXManager.instance.CompanionEquip();
        }   
    }
}
