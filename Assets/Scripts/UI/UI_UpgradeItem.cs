﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_UpgradeItem : MonoBehaviour 
{
	[SerializeField]
    UI_StatItem[] _stats;

	[SerializeField]
    StatData _currentStats;

	[SerializeField]
    Text _nameText;
	
	[SerializeField]
    Text _costText;

	[SerializeField]
    Text _levelText;

	[SerializeField]
	Button _upgradeButton;

	void SetText(Text text, string value)
	{
		if(text != null)
		{
			text.text = value;
		}
	}

	public void SetName(string name)
	{
		SetText(_nameText, name);
	}

	public void SetCost(string name)
	{
		SetText(_costText, name);
	}

	public void SetLevel(string level)
	{
		SetText(_levelText, level);
	}

	public void SetUpgradeButton(Action onClick)
	{
		if(_upgradeButton != null)
		{
			_upgradeButton.onClick.AddListener( () =>
			{
				onClick();
			});
		}
	}

    public void UpdateCost(int level)
    {
        SetCost(UpgradeManager.instance.ReturnUpgradeCost(level).ToString() + " Ore");    
    }

    public void UpdateInfo(int level)
    {
        SetLevel((level).ToString());
        UpdateCost(level);
    }

	public void SetStats(StatData data)
	{
		StatData inputStatData;
        if (data == null)
        {
            inputStatData = PlayerShip.instance.GetModifiedStats();
        }
        else
        {
            inputStatData = data;
        }

        int cost;
        int attackTemp = Mathf.RoundToInt(inputStatData.attack);
        _stats[0].level.text = attackTemp.ToString();
        if (_stats[0].cost != null)
        {
            cost = Mathf.RoundToInt(UpgradeManager.instance.costCurve.Evaluate(inputStatData.attack));
            //_stats[0].cost.text = cost.ToString();
            //_stats[0].button.interactable = cost <= DataManager.instance.currentGold;
        }

        int defenseTemp = Mathf.RoundToInt(inputStatData.defense);
        _stats[1].level.text = defenseTemp.ToString();
        if (_stats[1].cost != null)
        {
            cost = Mathf.RoundToInt(UpgradeManager.instance.costCurve.Evaluate(inputStatData.defense));
            //_stats[1].cost.text = cost.ToString();
            //_stats[1].button.interactable = cost <= DataManager.instance.currentGold;
        }

        int miningEfficiencyTemp = Mathf.RoundToInt(inputStatData.miningEfficiency);
        _stats[2].level.text = miningEfficiencyTemp.ToString();
        if (_stats[2].cost != null)
        {
            cost = Mathf.RoundToInt(UpgradeManager.instance.costCurve.Evaluate(inputStatData.miningEfficiency));
            //_stats[2].cost.text = cost.ToString();
            //_stats[2].button.interactable = cost <= DataManager.instance.currentGold;
        }


        int luckTemp = Mathf.RoundToInt(inputStatData.luck);
        _stats[3].level.text = miningEfficiencyTemp.ToString();
        if (_stats[3].cost != null)
        {
            cost = Mathf.RoundToInt(UpgradeManager.instance.costCurve.Evaluate(inputStatData.luck));
            //_stats[3].cost.text = cost.ToString();
            //_stats[3].button.interactable = cost <= DataManager.instance.currentGold;
        }
	}

}
