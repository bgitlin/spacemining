﻿using UnityEngine;
using UnityEngine.UI;

public class UI_StatItem : MonoBehaviour
{
    public Text header;
    public Text level;
    public Text cost;
    public Button button;
}
