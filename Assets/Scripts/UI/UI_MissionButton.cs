﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_MissionButton : MonoBehaviour {

    public CelestrialBody myBody;
    public Text missionName;
    public int missionNum;
    public MissionData myMissionData;
    public Image lockedIcon;
    public Image checkMarkImg;
    public Image myImage;
    

    public void Initialize(CelestrialBody _myBody, int _missionNum, MissionData _missionData)
    {
        myBody = _myBody;
        missionNum = _missionNum;
        myMissionData = _missionData;
        myMissionData.index = _missionNum;
        missionName.text = ("Mission " + (missionNum + 1));
        myImage = GetComponent<Image>();
    }

    public void Selected()
    {
        if (!myMissionData.locked)
        {
            myBody.SelectedMission(missionNum);
        }
        else
        {

        }
    }
}
