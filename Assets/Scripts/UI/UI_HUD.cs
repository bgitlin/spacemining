﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_HUD : MonoBehaviour {

    public UI_Popup popupPrefab;
    public Text gold;
    public Text prizenetonium;

    public UI_ResourceIconManager resourceIconManager;

    public static UI_HUD instance;
    private void Awake()
    {
        instance = this;
    }
    
    void OnDisable()
    {
        UnSubscribe();
    }
    
    public void Subscribe()
    {
        InventoryManager.OnAddedLoot += OnLootCollect;
        PlayerShip.instance.health.OnDeath += OnPlayerDeath;
    }

    void UnSubscribe()
    {
        InventoryManager.OnAddedLoot -= OnLootCollect;
        PlayerShip.instance.health.OnDeath -= OnPlayerDeath;  
    }

    public void UpdateGoldText(int _value)
    {
        gold.text = _value.ToString();
    }

    public void UpdatePrizenetoniumText(int _value)
    {
        prizenetonium.text = _value.ToString();
    }

    //TODO: MOVE POPUP LOGIC TO POPUP MANAGER?

    
    UI_Popup CreatePopup(string message)
    {
        UI_Popup popup = Instantiate(popupPrefab);
        popup.transform.SetParent(this.transform, false);
        popup.SetNotification(message);
        return popup;
    }

    void OnLootCollect(Item c)
    {
        CreatePopup("Loot Collected: "+ c.name);
    }

    void OnPlayerDeath()
    {
        UI_Popup pop = CreatePopup("You died.");
        //TODO: TEMP This should probably be moved
        pop.OnClick += GameManager.instance.RestartLevel;
    }
}
