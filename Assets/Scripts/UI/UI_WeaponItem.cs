﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class UI_WeaponItem : MonoBehaviour, IPointerClickHandler 
{
    public Text header;
    public Image icon;
	public UI_StatItem[] statItems;
    public Item item;
    public Action<Item> onclick;
    
    public void Display(Item i, Action<Item> onClicked)
    {
        item = i;
        DisplayStatItems(i.stats);
        SetHeaderText(i.name);
        SetIcon(i.iconSprite);
        if(onClicked != null)
        {
            onclick += onClicked;
        }
    }

    public void Display( string name, StatData stats, Sprite icon, Action onClicked=null)
    {
        DisplayStatItems(stats);
        SetHeaderText(name);
        SetIcon(icon);

        if(onClicked != null)
        {
            onclick += i =>{ onClicked();};
        }
    }

    //TODO: Testing 
    public void DisplayStatItems(StatData stats)
    {
        if(stats != null)
        {
            statItems[0].level.text = stats.attack.ToString();
            statItems[1].level.text = stats.defense.ToString();
            statItems[2].level.text = stats.miningEfficiency.ToString();
            statItems[3].level.text = stats.luck.ToString();
        }
    }

    void ChangeColor(Color c)
    {
        if(this.icon != null)
        {
            this.icon.color = c;
        }
    }

    public void UpdateColor()
    {
        if(item.equipped)
            ChangeColor(Color.green);
        else
            ChangeColor(Color.white);
    }

    void SetIcon(Sprite icon)
    {
        if(this.icon != null)
        {
            this.icon.sprite = icon;
        }
    }

    public void SetHeaderText(string name)
    {
        if(header != null)
        {
            header.text = name;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if(onclick != null)
        {
            onclick(item);
        }
    }
}
