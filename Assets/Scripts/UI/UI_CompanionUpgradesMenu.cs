﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UI_CompanionUpgradesMenu : UI_Menu
{
    public static UI_CompanionUpgradesMenu instance;

    [SerializeField]
    UI_UpgradeItem _upgraderUI;

    private bool menuOpen = false;

    [SerializeField]
    Button _upgradeButton;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        UI_CompanionButton.sendCompanion += PopulateInfo;
    }

    public void PopulateInfo(Item i)
    {
        Debug.Log(i.name + " " + this);
        _upgradeButton.onClick.AddListener( () => ClickUpgrade(i));
        if (!menuOpen)
        {
            UpdateStats(i.stats);
            UpdateName(i.name);
            _upgraderUI.UpdateInfo(i.level);
        }
    }
    
    public override void OpenMenu()
    {
        base.OpenMenu();
        menuOpen = true;
    }
    public override void CloseMenu()
    {
         _upgradeButton.onClick.RemoveAllListeners();
        base.CloseMenu();
        menuOpen = false;
    }

    void UpdateName(string name)
    {
        _upgraderUI.SetName(name);
    }

    void ClickUpgrade(Item i)
    {
        Debug.Log(i.name + " clicked" );
        if (UpgradeManager.instance.CanPurchase(i.level))
        {
            UpgradeManager.instance.PurchaseCompanionUpgrade(i.level);
            i.Upgrade();
            UpdateStats(i.stats);
            _upgraderUI.UpdateInfo(i.level);
        }
    }

    void UpdateStats(StatData _data)
    {
       _upgraderUI.SetStats(_data);
    }
}
