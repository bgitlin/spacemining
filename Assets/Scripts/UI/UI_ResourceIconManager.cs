﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ResourceIconManager : UI_ImagePool
{
    [SerializeField] private Sprite[] icons;
    public enum ResourceType { PrizeNet, Ore }
    [SerializeField] private Image[] targets;
    [SerializeField] private AnimationCurve moveCurve;
    [SerializeField] private AnimationCurve scaleCurve;

    [FMODUnity.EventRef]
    public string resourceGather;

    public void CreateIcon(ResourceType _type, Vector3 _startPos)
    {
        FMODUnity.RuntimeManager.PlayOneShot(resourceGather);
        SoundFXManager.instance.ResourceDestroy();
        Image image = TakeFromPool();
        image.sprite = icons[(int)_type];
        Vector3 screenPoint = Camera.main.WorldToScreenPoint(_startPos);
        Vector2 start = new Vector2(Map.map(screenPoint.x, 0, Screen.width, -panel.sizeDelta.x * 0.5f, panel.sizeDelta.x * 0.5f), Map.map(screenPoint.y, 0, Screen.height, -panel.sizeDelta.y, 0));

        StartCoroutine(SlideRoutine(image, start, targets[(int)_type].rectTransform.anchoredPosition, 1));
    }

    private IEnumerator SlideRoutine(Image _image, Vector2 _start, Vector2 _target, float _speed = 1)
    {
        float timer = 0;
        while (timer < 1)
        {
            timer += Time.deltaTime * _speed;
            _image.rectTransform.anchoredPosition = Vector2.Lerp(_start, _target, moveCurve.Evaluate(timer));
            _image.rectTransform.localScale = Vector2.one * scaleCurve.Evaluate(timer);
            yield return 0f;
        }

        ReturnToPool(_image);
    }
}
