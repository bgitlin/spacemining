﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ImagePool : MonoBehaviour
{
    [SerializeField] protected RectTransform panel;
    [SerializeField] protected Image imagePrefab;
    [SerializeField] protected int startingSize;
    protected List<Image> images = new List<Image>();
    protected List<Image> imagePool = new List<Image>();

    private void Start()
    {
        for (int i = 0; i < startingSize; i++)
        {
            imagePool.Add(CreateNewImage());
        }
    }

    protected virtual Image TakeFromPool()
    {
        Image newImage;
        if (imagePool.Count > 0)
        {
            newImage = imagePool[0];
            imagePool.RemoveAt(0);
        }
        else
        {
            newImage = CreateNewImage();
        }

        images.Add(newImage);
        newImage.gameObject.SetActive(true);
        return newImage;
    }

    protected virtual void ReturnToPool(Image _image)
    {
        images.RemoveAt(images.IndexOf(_image));
        imagePool.Add(_image);
        _image.gameObject.SetActive(false);
    }

    private Image CreateNewImage()
    {
        Image newImage = Instantiate(imagePrefab, panel);
        newImage.gameObject.SetActive(false);
        return newImage;
    }
}
