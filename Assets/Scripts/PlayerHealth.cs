﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : HealthSystem
{
    public Action PlayerDeath;

    public void Initialize()
    {
        //Temp values for Jeff/Jennie demoing, remove +100
        InitHealth(UpgradeManager.instance.healthCurve.Evaluate(PlayerShip.instance.GetModifiedStats().defense + 100));
    }

    //TODO: Make ship grow/shrink
    public void Damage(float damage)
    {
        TakeDamage(damage);
    }

    protected override void Death()
    {
        if(PlayerDeath != null)
        {
            PlayerDeath();
        }
        base.Death();
    }
}
