﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class IdleMineData //ToDo : Loot
{
    public float rateOfGold; //Per 10 seconds Right now.
    public int goldCollected;
    public int goldCapacity;
    public float timePassed;

    public IdleMineData(int _rateOfGold, int _capacity)
    {
        rateOfGold = _rateOfGold;
        goldCapacity = _capacity;
    }
}
