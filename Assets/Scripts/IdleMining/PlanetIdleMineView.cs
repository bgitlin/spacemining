﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetIdleMineView : MonoBehaviour {
    
    [SerializeField]
    private Button myButton;
    private Text buttonText;

    public RectTransform idleMineLayoutElement;

    //UI Prefabs
    public GameObject idleMineUI;
    public List<GameObject> idleMineVisList;

    public void InitializeView(System.Func<int> _onCollect, int _mineAmt)
    {
        myButton = GetComponentInChildren<Button>();
        buttonText = myButton.GetComponentInChildren<Text>();
        InitEvents(_onCollect);
        InitMineView(_mineAmt);
        UpdateCollectButton(0);
    }

    public void InitEvents(System.Func<int> _onCollect)
    {
        myButton.onClick.AddListener(() => 
        {
            //When mine resources Collected
            _onCollect();
        });
    }

    public void UpdateCollectButton(int _amt)
    {
        buttonText.text = _amt.ToString();
    }

    public void InitMineView(int _amt)
    {
        //This should check to see what we've already instantiated.
        for (int i = 0; i < _amt; i++)
        {
            AddMine();
        }
    }

    public void AddMine()
    {
        idleMineVisList.Add(Instantiate(idleMineUI, idleMineLayoutElement));
    }
}
