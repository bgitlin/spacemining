﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IdleMineManager : UI_Menu
{
    public static IdleMineManager instance;
    public void Awake()
    {
        instance = this;
    }
    
    public List<IdleMineController> idleMineControllers;

    //Prefabs
    public GameObject ui_PlanetPanel;
    public IdleMineController idleMineController;

    //Ref
    public RectTransform planetLayoutGroup;
    public Button collectAllButton;
    public Text collectAllButtonText;

    //API Calls for System.DateTime
    public System.DateTime loadedDateTime;

    //Action
    public System.Action updateMines;

    //Temp Button Things
    public RectTransform idleMiningPanel;
    public bool panelActive = false;

    public override void OpenMenu()
    {
        AnimationManager.instance.Slide(idleMiningPanel, idleMiningPanel.anchoredPosition, Vector2.zero, 2);
    }

    public override void CloseMenu()
    {
        AnimationManager.instance.Slide(idleMiningPanel, idleMiningPanel.anchoredPosition, new Vector2(500, 0), 2);
    }
    //End temp button things

    private IEnumerator IdleMineTimeRoutine()
    {
        float timer = 0;
        while(true)
        {
            timer += Time.deltaTime;
            if (timer >= 5f)
            {
                timer = 0;
                if (updateMines != null)
                {
                    updateMines.Invoke();
                    DataManager.instance.SaveCurrentGalaxyData();//move
                }
            }
            yield return 0f;
        }
    }

    public void Initialize()
    {
        LoadIdleMineData();
        collectAllButton.onClick.AddListener(CollectAllGold);
        UpdateCollectAllButton();
        StartCoroutine(IdleMineTimeRoutine());
    }

    public List<IdleMineController> LoadIdleMineData()
    {
        idleMineControllers = new List<IdleMineController>();
        print("loaded");
        for (int i = 0; i < DataManager.instance.galaxyData.celestrialDatas.Count; i++)
        {
            if (DataManager.instance.galaxyData.celestrialDatas[i].idleMineDataList.Count > 0)
            {
                idleMineControllers.Add(Instantiate(idleMineController, this.transform) as IdleMineController);
                idleMineControllers[i].idleMines = DataManager.instance.galaxyData.celestrialDatas[i].idleMineDataList;
                
                DataManager.instance.SavePlanetData(DataManager.instance.galaxyData.celestrialDatas[i]);
            }
        }
        UpdateIdleMineView();

        return idleMineControllers;
    }

    public void UpdateIdleMineView()
    {
        for (int i = 0; i < idleMineControllers.Count; i++)
        {
            if (idleMineControllers[i].idleMineView != null)
            {
                int num = idleMineControllers[i].idleMines.Count - idleMineControllers[i].idleMineView.idleMineVisList.Count;
                for (int j = 0; j < num; j++)
                {
                    idleMineControllers[i].idleMineView.AddMine();
                }
            }
            else
            {
                PlanetIdleMineView myView = Instantiate(ui_PlanetPanel, planetLayoutGroup).GetComponent<PlanetIdleMineView>();
                idleMineControllers[i].idleMineView = myView;
                idleMineControllers[i].InitializeController(ref updateMines);
            }
        }
    }
    
    public void AddNewIdleMine(int _planetIndex)
    {
        GalaxyData tempGalaxyData = DataManager.instance.galaxyData;
        
        for (int i = 0; i < tempGalaxyData.celestrialDatas.Count; i++)
        {
            if (tempGalaxyData.celestrialDatas[i].planetIndex == _planetIndex)
            {
                if (tempGalaxyData.celestrialDatas[i].idleMineDataList.Count == 0)
                {
                    idleMineControllers.Add(Instantiate(idleMineController, this.transform) as IdleMineController);
                }

                if (idleMineControllers[i].idleMines.Count < 3)
                {
                    IdleMineData tempIdleMineData = new IdleMineData(100 * (_planetIndex * 10), 500);
                    tempGalaxyData.celestrialDatas[i].idleMineDataList.Add(tempIdleMineData);
                    idleMineControllers[i].idleMines = tempGalaxyData.celestrialDatas[i].idleMineDataList;
                    DataManager.instance.SavePlanetData(tempGalaxyData.celestrialDatas[i]);
                }
            }
        }
        UpdateIdleMineView();
    }

    public void CollectAllGold()
    {
        DataManager.instance.AdjustGold(CollectAllPlanetsGold());
        UpdateCollectAllButton();
        DataManager.instance.SaveCurrentGalaxyData();
    }
    public int CollectAllPlanetsGold()
    {
        int totalGold = 0;
        for (int i = 0; i < idleMineControllers.Count; i++)
        {
            totalGold = idleMineControllers[i].CollectPlanetGold();
        }
        return totalGold;
    }
    public void UpdateCollectAllButton()
    {
        int total = 0;
        for (int i = 0; i < idleMineControllers.Count; i++)
        {
            total += idleMineControllers[i].GetTotalControllerValue();
        }
        collectAllButtonText.text = total.ToString();
    }

    //Debug
    [ContextMenu ("AddIdleMinePlanet1")]
    public void AddIdleMine()
    {
        AddNewIdleMine(0);
    }

    [ContextMenu("AddIdleMinePlanet2")]
    public void AddIdleMine2()
    {
        AddNewIdleMine(1);
    }
}
