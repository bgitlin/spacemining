﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleMineController : MonoBehaviour {

    //PerPlanet
    public PlanetIdleMineView idleMineView;
    public List<IdleMineData> idleMines;
    private float timer = 0;
    
    public void InitializeController(ref System.Action _inputEvent)
    {
        _inputEvent += AddGoldToAllMines;
        idleMineView.InitializeView(CollectPlanetGold, idleMines.Count);
        UpdateVisual();
    }
    
    public int CollectPlanetGold()
    {
        int totalGold = 0;

        for (int i = 0; i < idleMines.Count; i++)
        {
            totalGold += idleMines[i].goldCollected;
            idleMines[i].goldCollected = 0;
        }
        DataManager.instance.AdjustGold(totalGold);
        UpdateVisual();
        return totalGold;
    }

    public int GetTotalControllerValue()
    {
        int totalGold = 0;

        for (int i = 0; i < idleMines.Count; i++)
        {
            totalGold += idleMines[i].goldCollected;
        }
        return totalGold;
    }

    public void AddGoldToAllMines()
    {
        for (int i = 0; i < idleMines.Count; i++)
        {
            AddGoldToMine(idleMines[i]);
            //ParticleFx of "+amt" (?)
        }
        UpdateVisual();
    }

    private int AddGoldToMine(IdleMineData _idleMineData)
    {
        if ((_idleMineData.goldCollected + _idleMineData.rateOfGold) < _idleMineData.goldCapacity)
        {
            _idleMineData.goldCollected += Mathf.RoundToInt(_idleMineData.rateOfGold);
        }
        else
        {
            _idleMineData.goldCollected = _idleMineData.goldCapacity;
        }

        return _idleMineData.goldCollected;
    }

    private void UpdateVisual()
    {
        print("updatedVisual");
        
        if (idleMineView != null)
        {
            idleMineView.UpdateCollectButton(GetTotalControllerValue());
        }
        IdleMineManager.instance.UpdateCollectAllButton();
    }
}
