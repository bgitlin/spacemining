﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MiningHit
{
    public IEnumerator routine;
    public Coroutine currentRoutine;
    public bool canHitAgain;
}
