﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class MiningControl : MonoBehaviour
{

    public LayerMask miningLMask;

    IAttacker _owner;

    bool _running;
    bool _canAttack;

    Coroutine _miningRoutine;

    Vector3 targetPosition;

    Vector3 depthCheck;

    [SerializeField]
    float _maxDistance = 25;

    [SerializeField]
    float _radius;

    public LineRenderer lineRenderer;

    Dictionary<Minable, MiningHit> _minables = new Dictionary<Minable, MiningHit>();

    public UnityEvent onHit = new UnityEvent();

    private void Awake()
    {
        lineRenderer = this.GetComponent<LineRenderer>();
    }

    public void Init(IAttacker owner)
    {
        this._owner = owner;
    }


    //SphereCastInput
    IEnumerator MiningInput(float damage, bool fromCamera)
    {
        if (!_canAttack)
        {
            yield return null;
        }

        _running = true;
        while (true)
        {
            //For Swipe
            if (!GameManager.instance.levelManager.levelMoving)
            {
                Ray ray;

                RaycastHit hit;

                if (fromCamera)
                {
                    ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                }
                else
                {
                    ray = new Ray(transform.position, targetPosition - transform.position);
                }

                Physics.Raycast(ray, out hit, 300, miningLMask);

                _maxDistance = (ray.origin - hit.point).magnitude;

                RaycastHit[] hitInfo = Physics.SphereCastAll(ray, _radius, _maxDistance, miningLMask);

                if (hitInfo.Length > 0)
                {
                    //Set all those minables to currently touching, resets after x seconds of not being called again
                    //To do, not most efficient;
                    //hitInfo.ToList()
                    //    .ForEach(m => Assess(m.transform.GetComponent<Minable>(), damage));

                    for (int i = 0; i < hitInfo.Length; i++)
                    {
                        TryMine(hitInfo[i].transform.GetComponent<Minable>(), damage);
                    }

                }

            }

            yield return null;
        }
    }

    /// <summary>
    /// Attemps to cause damage
    /// to a minable, if it succeeds
    /// it will not be able to attack that
    /// minable for X seconds. Every attempt
    /// to mine while its coolingdown will
    /// restart it
    /// </summary>
    void TryMine(Minable m, float damage)
    {
        MiningHit hit;

        //if this person has not already hit me
        if (!_minables.TryGetValue(m, out hit))
        {
            hit = new MiningHit() { canHitAgain = true };
            _minables.Add(m, hit);
        }

        //Minable is alive
        if (m.healthSystem.IsAlive())
        {
            if (hit.canHitAgain)
            {
                m.TakeDamage(_owner);

                Display();

                hit.canHitAgain = false;

                //start routine that will allow them to hit again after X seconds
                hit.currentRoutine = StartCoroutine(hit.routine = CoolDown
                (
                    m.attackedCoolDown, () =>
                    {
                        hit.canHitAgain = true;
                    }
                ));

                if(onHit != null)
                {
                    onHit.Invoke();
                }
            }
            //If they are NOT able to hit again
            else
            {
                //Restart their cool down before they can hit me again
                StopCoroutine(hit.currentRoutine);

                //start routine that will allow them to hit again after X seconds
                hit.currentRoutine = StartCoroutine(hit.routine = CoolDown
                (
                    m.attackedCoolDown, () =>
                    {
                        hit.canHitAgain = true;
                    }
                ));
            }
        }
        else
        {
            if (hit.currentRoutine != null)
                StopCoroutine(hit.currentRoutine);
        }
    }

    void Display()
    {
        if (lineRenderer != null)
        {
            //TEMP
            lineRenderer.enabled = true;
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, targetPosition);

            StartCoroutine(CoolDown(1.5f, () => { }));
        }
    }

    public void StartMining(Vector3 input, float damage, bool camera)
    {
        targetPosition = input;
        if (!_running)
            _miningRoutine = StartCoroutine(MiningInput(damage, camera));
    }

    public void StopMining()
    {
        if (_miningRoutine != null)
        {
            StopCoroutine(_miningRoutine);
            _running = false;
        }
    }

    IEnumerator CoolDown(float time, System.Action callback)
    {
        yield return new WaitForSeconds(time);
        if (callback != null)
        {
            callback();
        }
    }

    #region Debugging


    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, targetPosition - transform.position);

        Gizmos.color = Color.blue;
        Gizmos.DrawRay(transform.position, new Vector3(_maxDistance,1,1) - transform.position);
    }



    #endregion
}
