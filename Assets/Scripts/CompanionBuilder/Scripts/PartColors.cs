﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PartColors : ScriptableObject {

    public Color primaryColor;
    public Color secondaryColor;
    public Color accentColor;

    private int location;

    public void SetLocation(int i)
    {
        location = i;
    }

    public int GetLocation()
    {
        return location;
    }
}
