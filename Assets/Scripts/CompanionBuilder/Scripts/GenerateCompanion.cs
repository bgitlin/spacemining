﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GenerateCompanion : MonoBehaviour 
{
    [Serializable]
    public class Parts
    {
        public PartData head, body, tail, wings;
        public PartColors colors;

        public Parts(CompanionParts _partsList)
        {
            head = _partsList.GetHead();
            body = _partsList.GetBody();
            tail = _partsList.GetTail();
            wings = _partsList.GetWings();
            colors = _partsList.GetColors();
        }

        public Parts(PartData _head, PartData _body, PartData _tail, PartData _wings, PartColors _colors){
            head = _head;
            body = _body;
            tail = _tail;
            wings = _wings;
            colors = _colors;
        }
    }

    public GenerateCompanion instance;

    private Parts parts;

    public CompanionParts partsList;

    private Renderer[] myRenderers;

    public void GenrateShip(string _name){
        Load(_name);
        InstantiateParts();
    }

    private void InstantiateParts()
    {
        myRenderers = new Renderer[4];

        GameObject tempHead = Instantiate(parts.head.gameObject, transform);
        GameObject tempBody = Instantiate(parts.body.gameObject, instance.transform);
        GameObject tempTail = Instantiate(parts.tail.gameObject, instance.transform);
        GameObject tempWings = Instantiate(parts.wings.gameObject, instance.transform);

        myRenderers[0] = tempHead.GetComponent<Renderer>();
        myRenderers[1] = tempBody.GetComponent<Renderer>();
        myRenderers[2] = tempTail.GetComponent<Renderer>();
        myRenderers[3] = tempWings.GetComponent<Renderer>();

        for(int i = 0; i < myRenderers.Length; i++){
            SetColors(myRenderers[i], parts.colors);
        }
    }

    private void SetColors(Renderer _renderer, PartColors colors)
    {
        _renderer.material.SetColor("_RColor", colors.primaryColor);
        _renderer.material.SetColor("_GColor", colors.secondaryColor);
        _renderer.material.SetColor("_BColor", colors.accentColor);
    }

    private void Save(string _name)
    {
        string json = JsonUtility.ToJson(parts);
        PlayerPrefs.SetString(_name + ":parts", json);
    }

    private void Load(string _name)
    {
        string json = PlayerPrefs.GetString(_name + ":parts");

        if(string.IsNullOrEmpty(json)){
            parts = new Parts(partsList);
            Save(_name);
            return;
        } 

        parts = JsonUtility.FromJson<Parts>(json);
        return;
    }
}
