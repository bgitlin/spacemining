﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PartData : ScriptableObject {

    public GameObject gameObject;

    public Material material;

    private int location;

    public void SetLocation(int i)
    {
        location = i;
    }

    public int GetLocation()
    {
        return location;
    }
}
