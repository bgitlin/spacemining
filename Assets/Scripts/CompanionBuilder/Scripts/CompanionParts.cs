﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CompanionParts : ScriptableObject
{
    public List<PartData> heads;
    public List<PartData> bodies;
    public List<PartData> tails;
    public List<PartData> wings;

    public List<PartColors> colors;

    public PartData GetHead()
    {
        int rand = Random.Range(0, heads.Count);

        PartData temp = heads[rand];
        temp.SetLocation(rand);

        return temp;
    }

    public PartData GetBody()
    {
        int rand = Random.Range(0, bodies.Count);

        PartData temp = bodies[rand];
        temp.SetLocation(rand);

        return temp;
    }

    public PartData GetTail()
    {
        int rand = Random.Range(0, tails.Count);

        PartData temp = tails[rand];
        temp.SetLocation(rand);

        return temp;
    }

    public PartData GetWings()
    {
        int rand = Random.Range(0, wings.Count);

        PartData temp = wings[rand];
        temp.SetLocation(rand);

        return temp;
    }

    public PartColors GetColors()
    {
        int rand = Random.Range(0, colors.Count);

        PartColors temp = colors[rand];
        temp.SetLocation(rand);

        return temp;
    }
}
