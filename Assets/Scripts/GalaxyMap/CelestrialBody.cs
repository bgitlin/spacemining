﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CelestrialBody : MonoBehaviour {

    public string id;
    public CelestrialData celesData;
    public Canvas myCanvas;
    public Transform missionLayoutGroup;
    public List<UI_MissionButton> missionUI;

    public virtual void Initialize(string _id)
    {
        id = _id;
        PopulateMissions();
    }

    public virtual bool IsBeat()
    {
        return celesData.beat;
    }
    
    public virtual void Selected()
    {
        if (!missionLayoutGroup.gameObject.activeSelf)
        {
            print("sideSelected");
            UI_Overworld.instance.BodySelected(this);
            missionLayoutGroup.gameObject.SetActive(true);
            GalaxyMapManager.instance.galaxyCameraManager.GoToPosition((transform.position + GalaxyMapManager.instance.focusPlanetOffset), -5f, 3);
        }
    }

    public void Return()
    {
        missionLayoutGroup.gameObject.SetActive(false);
    }

    public virtual void PopulateMissions()
    {
        missionUI = new List<UI_MissionButton>();
        for (int i = 0; i < celesData.missions.Count; i++)
        {
            MissionData tempMissionData = celesData.missions[i];
            UI_MissionButton tempMissionButton = Instantiate(GalaxyMapManager.instance.missionButton, missionLayoutGroup) as UI_MissionButton;
            tempMissionButton.Initialize(this, i, tempMissionData);

            tempMissionButton.myImage.color = tempMissionData.locked ? Color.gray : Color.white;
            tempMissionButton.lockedIcon.enabled = tempMissionData.locked;
            tempMissionButton.checkMarkImg.enabled = tempMissionData.complete;
            
            missionUI.Add(tempMissionButton);
        }
    }

    public virtual void UpdateMissions(List<MissionData> _missionData)
    {
        for (int i = 0; i < _missionData.Count; i++)
        {
            missionUI[i].myMissionData.locked = _missionData[i].locked;
            missionUI[i].myMissionData.complete = _missionData[i].complete;

            missionUI[i].myImage.color = _missionData[i].locked ? Color.gray : Color.white;
            missionUI[i].lockedIcon.enabled = _missionData[i].locked;
            missionUI[i].checkMarkImg.enabled = _missionData[i].complete;
        }
        print("missionsUpdated");
    }

    public virtual MissionData GetNextMission()
    {
        MissionData tempMissionData = new MissionData();
        for (int i = 0; i < celesData.missions.Count; i++)
        {
            if (!celesData.missions[i].locked && !celesData.missions[i].complete)
            {
                tempMissionData = celesData.missions[i];
            }
        }
        return tempMissionData;
    }

    public virtual void SelectedMission(int _missionNum)
    {
        //Load Scene
        print("missionIndex" + _missionNum);
        celesData.currentMissionIndex = _missionNum;//Setting the current mission
        DataManager.instance.SaveLastCelestrialData(celesData);
        GalaxyMapManager.instance.LoadLevel();
    }

    public virtual void SelectedMission()
    {
        //Load Scene
        celesData.currentMissionIndex = GetNextMission().index;//Setting the current mission
        print(GetNextMission().index);
        DataManager.instance.SaveLastCelestrialData(celesData);
        GalaxyMapManager.instance.LoadLevel();
    }
}
