﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GalaxyMapManager : MonoBehaviour {

    public static GalaxyMapManager instance;
    private void Awake()
    {
        instance = this;
    }

    public Vector3 distToNextPlanet = new Vector3(0, 7.5f, 0);
    public Vector3 distToNextSideContent = new Vector3(0, 2f, 0);
    public Vector3 sideContentOffset = new Vector3(2, 0, 0);
    public Vector3 sideContentHeightOffset = new Vector3(0, 3, 0);
    public Vector3 focusPlanetOffset = new Vector3(0, -0.75f, 0);

    public CelestrialBody[] planetPrefabs;
    public CelestrialBody[] sideContent;

    //public List<PlanetData> celestrialDataList = new List<PlanetData>();

    public List<Planet> planets = new List<Planet>();

    public LineRenderer myLineR;

    // Control
    public CameraManager galaxyCameraManager;

    private bool scrolling;
    private Vector3 mousePosition;

    public LayerMask celestrialBodyLayer;
    public LayerMask uiLayer;

    // UI
    public UI_MissionButton missionButton;
    public GameObject backButton;
    private bool zoomed = false;

    //LineRenderers
    public LineRenderer planetLR;
    public List<LineRenderer> sideLineRList = new List<LineRenderer>();


    private void Start()
    {
        ReturnToOverworld();
        StartCheckInput();
    }
    

    public void LoadLevel()
    {
        StartCoroutine(LoadLevelRoutine());
    }
    public IEnumerator LoadLevelRoutine()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("MiningLevel");
    }

    public Planet GenerateNextPlanet(Vector3 _nextPlanetLoc, int _level, int _index, bool sides)
    {
        Vector3 tempPlanetLoc = _nextPlanetLoc;
        string tempPlanetString = ("planet" + _index.ToString());
        PlanetData pData = new PlanetData();
        pData.InitializeCelestrialData(CelestrialData.CelestrialBodyType.planet, tempPlanetLoc, _level, tempPlanetString, _index);
        pData.generationType = ((CelestrialData.GenerationType)Random.Range(0, 2));
        InitializeMissions(pData, 5);
        if (sides)
        {
            pData.sidesToGenerate = Random.Range(1, 3);
        }
        pData.planetIndex = _index;
        Planet tempBody = Instantiate(planetPrefabs[(int)pData.generationType], tempPlanetLoc, Quaternion.identity, transform) as Planet;
        planets.Add(tempBody);
        tempBody.myData = pData;
        tempBody.Initialize(tempPlanetString);
        return tempBody;
    }

    private void GeneratePlanetFromData(PlanetData _data)
    {
        print("Sides:" + _data.mySideContent.Count);
        Planet tempBody = Instantiate(planetPrefabs[(int)_data.generationType], _data.location, Quaternion.identity, transform) as Planet;
        string tempPlanetString = ("planet" + _data.planetIndex.ToString());
        planets.Add(tempBody);
        tempBody.myData = _data;

        tempBody.Initialize(tempPlanetString);
    }

    public CelestrialBody GenerateNextSideContent(Vector3 _nextSideContentLoc, int _level, int _sideNum, int _index)
    {
        int num = Random.Range(0, sideContent.Length);
        CelestrialData.CelestrialBodyType tempType = sideContent[num].celesData.type;
        CelestrialBody tempSideBody = Instantiate(sideContent[num], _nextSideContentLoc, Quaternion.identity, planets[_index].transform) as CelestrialBody;
        
        string tempSideString = ("planet" + _index + "side" + planets[_index].mySides.Count.ToString());

        tempSideBody.celesData = new CelestrialData();
        tempSideBody.celesData.InitializeCelestrialData(tempType, _nextSideContentLoc, _level, tempSideString, _index);
        tempSideBody.celesData.generationType = CelestrialData.GenerationType.gasGiant;//TEMPORARILY SET TO GAS GIANT FOR BOTH
        InitializeMissions(tempSideBody.celesData, 3);
        tempSideBody.Initialize(tempSideString);
        return tempSideBody;
    }

    private void GenerateSideFromData(PlanetData _myPlanetData, int index)
    {
        int num = ((int)_myPlanetData.mySideContent[index].type - 1);
        CelestrialBody tempSideType = sideContent[num];
        CelestrialBody tempSideBody = Instantiate(tempSideType, _myPlanetData.mySideContent[index].location, Quaternion.identity) as CelestrialBody;
        tempSideBody.transform.SetParent(planets[_myPlanetData.planetIndex].transform);
        (planets[_myPlanetData.planetIndex]).mySides.Add(tempSideBody);
        string tempSideString = ("planet" + _myPlanetData.planetIndex + "side" + (planets[_myPlanetData.planetIndex]).mySides.Count.ToString());
        
        tempSideBody.celesData = _myPlanetData.mySideContent[index];
        tempSideBody.Initialize(tempSideString);
    }
    
    public void CreateNextSideContent(Planet _planet)
    {
        print(_planet.myData.sidesToGenerate);
        if (_planet.myData.sidesToGenerate > 0)
        {
            Vector3 sideContentLoc = ((_planet.myData.mySideContent[_planet.myData.mySideContent.Count - 1].location) + sideContentHeightOffset);
            CelestrialBody tempSideBody = GenerateNextSideContent(sideContentLoc, (_planet.myData.level), (_planet.myData.mySideContent.Count + 1), _planet.myData.planetIndex);
            _planet.mySides.Add(tempSideBody);
            _planet.myData.sidesToGenerate--;
            _planet.myData.mySideContent.Add(tempSideBody.celesData);
        }
        else
        {

        }
    }

    public void InitializeMissions(CelestrialData _data, int _amt)
    {
        _data.missions = new List<MissionData>();
        for (int i = 0; i < _amt; i++)
        {
            MissionData tempData = new MissionData();
            tempData.complete = false;
            if (i == 0)
            {
                tempData.locked = false;
            }
            else
            {
                tempData.locked = true;
            }

            if (i == (_amt - 1))
            {
                tempData.missionType = MissionData.MissionType.bossBattle;
            }
            else
            {
                tempData.missionType = MissionData.MissionType.enemyEncounter;
            }
            _data.missions.Add(tempData);
        }
    }


    public void UnlockNextMission(CelestrialData _data, int _prevMission)
    {
        if ((_prevMission + 1) < _data.missions.Count)
        {
            int num = (_prevMission + 1);
            _data.missions[num].locked = false;
        }
        else
        {
            _data.beat = true;
        }
    }

    private void ReturnToOverworld()
    {
        List<PlanetData> tempPlanetDataList = new List<PlanetData>();
        
        if (DataManager.instance.LoadGalaxyData() != null)
        {
            print("not null");
            tempPlanetDataList = DataManager.instance.LoadGalaxyData().celestrialDatas;
            GenerateGalaxyFromData(tempPlanetDataList);
        }
        else
        {
            print("else");
            DataManager.instance.SaveNewPlanet(GenerateNextPlanet(Vector3.zero, 0, 0, false));
            DataManager.instance.SaveLastCelestrialData(planets[0].myData);
        }
    }

    private void GenerateGalaxyFromData(List<PlanetData> celestrialDataList)
    {
        if (celestrialDataList.Count > 0)
        {
            for (int i = 0; i < celestrialDataList.Count; i++)
            {
                print("SideCount:" + celestrialDataList[i].mySideContent.Count);
                GeneratePlanetFromData(celestrialDataList[i]);
                for (int j = 0; j < celestrialDataList[i].mySideContent.Count; j++)
                {
                    print("generating side");
                    print(j);
                    GenerateSideFromData(celestrialDataList[i], j);
                }
            }
            if (celestrialDataList.Count > 1)
            {
                //LoadLineRenderers();
            }
            CheckForNewPlanet();
        }
        
    }

    private void CheckForNewPlanet()
    {
        CelestrialData previousCelestrialBodyData = DataManager.instance.LoadLastCelestrialData();

        Planet currentPlanet = planets[previousCelestrialBodyData.planetIndex];

        galaxyCameraManager.GoToPosition(previousCelestrialBodyData.location);

        //We need a way to check if we've already generated this planet.
        if (previousCelestrialBodyData.type == CelestrialData.CelestrialBodyType.planet)
        {
            print("fromPlanet");
            if (previousCelestrialBodyData.beat)
            {
                print("planetbeat");
                print("planet" + previousCelestrialBodyData.planetIndex + "== | planets-1?" + planets.Count);
                if ((planets.Count - 1) == previousCelestrialBodyData.planetIndex)
                {
                    currentPlanet.myData.beat = true;
                    currentPlanet.myData.missions = previousCelestrialBodyData.missions;


                    //Create next planet/sides
                    Vector3 tempLoc = (previousCelestrialBodyData.location + distToNextPlanet);
                    Planet tempPlanet = GenerateNextPlanet(tempLoc, (previousCelestrialBodyData.level + 1), (previousCelestrialBodyData.planetIndex + 1), true);

                    int rand = Random.value > 0.5f ? -1 : 1;
                    Vector3 sideContentLoc = ((sideContentOffset * rand) + tempLoc);
                    CelestrialBody tempBody = GenerateNextSideContent(sideContentLoc, (previousCelestrialBodyData.level + 1), 0, (previousCelestrialBodyData.planetIndex + 1));

                    tempPlanet.mySides.Add(tempBody);
                    tempPlanet.myData.sidesToGenerate--;
                    tempPlanet.myData.mySideContent.Add(tempBody.celesData);

                    DataManager.instance.SaveLastCelestrialData(tempPlanet.myData);
                    DataManager.instance.SaveNewPlanet(tempPlanet);
                }
                else
                {
                    print("alreadyGeneratednext Planet" + previousCelestrialBodyData.planetIndex);
                }
            }
            else
            {
                currentPlanet.myData.missions = previousCelestrialBodyData.missions;
            }
            if (previousCelestrialBodyData.missions != null)
            {
                currentPlanet.UpdateMissions(previousCelestrialBodyData.missions);
            }
        }
        else
        {
            if (previousCelestrialBodyData.beat)
            {
                print("sideBeat");
                print("sideID: " + previousCelestrialBodyData.id);
                
                CreateNextSideContent(planets[previousCelestrialBodyData.planetIndex]);
                for (int i = 0; i < currentPlanet.mySides.Count; i++)
                {
                    if (previousCelestrialBodyData.id == currentPlanet.mySides[i].celesData.id)
                    {
                        print("missionsShouldBeUpdated");
                        currentPlanet.mySides[i].celesData = previousCelestrialBodyData;
                        currentPlanet.mySides[i].UpdateMissions(previousCelestrialBodyData.missions);
                    }
                }
            }
            else
            {
                print("updateSideMissions");
                print(currentPlanet.mySides.Count);
                currentPlanet.mySides[currentPlanet.mySides.Count - 1].celesData.missions = previousCelestrialBodyData.missions;
                currentPlanet.mySides[currentPlanet.mySides.Count - 1].UpdateMissions(previousCelestrialBodyData.missions);
            }
        }
        
        LoadLineRenderers();
        DataManager.instance.SavePlanetData(currentPlanet.myData);
    }

    public void ReturnToOverworldView(Vector3 _inputVector)
    {
        zoomed = false;
        galaxyCameraManager.GoToPosition(_inputVector, -10f, 3);
    }


    //Galaxy Camera Controls
    public void StartCheckInput()
    {
        if (checkInput == null)
        {
            checkInput = CheckInput();
            StartCoroutine(checkInput);
        }
    }
    private void StopCheckInput()
    {
        if (checkInput != null)
        {
            StopCoroutine(checkInput);
        }
        checkInput = null;
    }
    

    private IEnumerator checkInput;
    private IEnumerator CheckInput()
    {
        yield return new WaitForSeconds(1f);
        while (true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                mousePosition = Input.mousePosition;
                scrolling = false;
            }
            else if (Input.GetMouseButton(0))
            {
                if (!zoomed)
                {
                    TrackMousePosition();
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                if (!scrolling)
                {
                    Ray inputRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;

                    if (Physics.Raycast(inputRay, out hit, 100f, celestrialBodyLayer))
                    {
                        CelestrialBody tempBody = hit.transform.GetComponent<CelestrialBody>();
                        if (tempBody != null)
                        {
                            tempBody.Selected();
                            zoomed = true;
                        }
                    }
                    else if (Physics.Raycast(inputRay, out hit, 100f, uiLayer))
                    {
                        //We can set this up to just be one button that holds all of the mission information
                        UI_MissionButton tempButton = hit.transform.GetComponent<UI_MissionButton>();
                        if (tempButton != null)
                        {
                            tempButton.Selected();
                        }
                    }
                }
                else
                {
                    scrolling = false;
                }
            }
            yield return 0f;
        }
    }
    
    public void TrackMousePosition()
    {
        float scrollDistance = Vector3.Distance(mousePosition, Input.mousePosition);
        Vector3 mouseDistance = mousePosition - Input.mousePosition;
        if (scrollDistance > 0.5f)
        {
            galaxyCameraManager.CancelMoveTo();
            Vector3 cameraPos = galaxyCameraManager.transform.position;
            Vector3 tempVector = Vector3.MoveTowards(cameraPos, cameraPos + (new Vector3(0, mouseDistance.y/Screen.height, 0) * 23f), 1);
            tempVector = new Vector3(tempVector.x, Mathf.Clamp(tempVector.y, planets[0].transform.position.y, planets[planets.Count - 1].transform.position.y), tempVector.z);
            galaxyCameraManager.transform.position = tempVector;
            scrolling = true;
        }

        mousePosition = Input.mousePosition;
        
    }

    private void LoadLineRenderers()
    {
        if (planetLR == null)
        {
            planetLR = Instantiate(myLineR, Vector3.zero, Quaternion.identity) as LineRenderer;
        }
        planetLR.positionCount = planets.Count;
        for (int l = 0; l < planets.Count; l++)
        {
            planetLR.SetPosition(l, planets[l].transform.position);
        }


        sideLineRList.Clear();
        for (int f = 1; f < planets.Count; f++)
        {
            Planet tempPlanet = planets[f];
            AddSideLineRenderer();

            sideLineRList[f - 1].positionCount = tempPlanet.mySides.Count + 1;
            for (int g = -1; g < tempPlanet.mySides.Count; g++)
            {
                if (g == -1)
                {
                    Vector3 newDist = distToNextPlanet * 0.5f;
                    Vector3 tempVector = planets[f].transform.position - newDist;
                    sideLineRList[f - 1].SetPosition((g + 1), tempVector);
                }
                else
                {
                    sideLineRList[f - 1].SetPosition((g + 1), tempPlanet.mySides[g].transform.position);
                }
            }
        }
    }

    public void AddSideLineRenderer()
    {
        LineRenderer tempSideLineR = Instantiate(myLineR, Vector3.zero, Quaternion.identity) as LineRenderer;
        sideLineRList.Add(tempSideLineR);
    }

    public void DebugGenerateNewPlanet()
    {
        CelestrialData tempData = DataManager.instance.celestrialData;

        Planet currentPlanet = planets[tempData.planetIndex];
        PlanetData pData = currentPlanet.myData;

        print("hey new mine!");
        IdleMineManager.instance.AddNewIdleMine(pData.planetIndex);
        DataManager.instance.SavePlanetData(currentPlanet.myData);
        DataManager.instance.SaveIdleMineData();

        currentPlanet.myData.beat = true;

        Vector3 tempLoc = (pData.location + distToNextPlanet);
        Planet tempPlanet = GenerateNextPlanet(tempLoc, (pData.level + 1), (pData.planetIndex + 1), true);

        int rand = Random.value > 0.5f ? -1 : 1;
        Vector3 sideContentLoc = ((sideContentOffset * rand) + tempLoc);
        CelestrialBody tempBody = GenerateNextSideContent(sideContentLoc, (pData.level + 1), 0, (pData.planetIndex + 1));

        tempPlanet.mySides.Add(tempBody);
        tempPlanet.myData.sidesToGenerate--;
        tempPlanet.myData.mySideContent.Add(tempBody.celesData);

        print(planets.Count);
        LoadLineRenderers();

        DataManager.instance.SaveLastCelestrialData(tempPlanet.myData);
        DataManager.instance.SaveNewPlanet(tempPlanet);
    }
}
