﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : CelestrialBody {

    public List<CelestrialBody> mySides = new List<CelestrialBody>();
    public PlanetData myData;


    public override bool IsBeat()
    {
        return myData.beat;
    }

    public override void PopulateMissions()
    {
        missionUI = new List<UI_MissionButton>();
        for (int i = 0; i < myData.missions.Count; i++)
        {
            MissionData tempMissionData = myData.missions[i];
            UI_MissionButton tempMissionButton = Instantiate(GalaxyMapManager.instance.missionButton, missionLayoutGroup) as UI_MissionButton;
            tempMissionButton.Initialize(this, i, tempMissionData);

            tempMissionButton.myImage.color = tempMissionData.locked ? Color.gray : Color.white;
            tempMissionButton.lockedIcon.enabled = tempMissionData.locked;
            tempMissionButton.checkMarkImg.enabled = tempMissionData.complete;

            missionUI.Add(tempMissionButton);
        }
        print("missionsPopulated");
    }

    public override void UpdateMissions(List<MissionData> _missionData)
    {
        for (int i = 0; i < _missionData.Count; i++)
        {
            missionUI[i].myMissionData.locked = _missionData[i].locked;
            missionUI[i].myMissionData.complete = _missionData[i].complete;

            missionUI[i].myImage.color = _missionData[i].locked ? Color.gray : Color.white;
            missionUI[i].lockedIcon.enabled = _missionData[i].locked;
            missionUI[i].checkMarkImg.enabled = _missionData[i].complete;
        }
        print("missionsUpdated");
    }

    public override MissionData GetNextMission()
    {
        MissionData tempMissionData = new MissionData();
        for (int i = 0; i < myData.missions.Count; i++)
        {
            if (!myData.missions[i].locked && !myData.missions[i].complete)
            {
                tempMissionData = myData.missions[i];
            }
        }
        return tempMissionData;
    }

    public override void Selected()
    {
        if (!missionLayoutGroup.gameObject.activeSelf)
        {
            print("PlanetSelected" + myData.level);
            UI_Overworld.instance.BodySelected(this);
            missionLayoutGroup.gameObject.SetActive(true);
            GalaxyMapManager.instance.galaxyCameraManager.GoToPosition((transform.position + GalaxyMapManager.instance.focusPlanetOffset), -5f, 3);
        }
    }

    public override void SelectedMission(int _missionNum)
    {
        //Load Scene
        print("missionIndex" + _missionNum);
        myData.currentMissionIndex = _missionNum;//Setting the current mission
        DataManager.instance.SaveLastCelestrialData(myData);
        GalaxyMapManager.instance.LoadLevel();
    }
    public override void SelectedMission()
    {
        //Load Scene
        print(GetNextMission().index);
        myData.currentMissionIndex = GetNextMission().index;//Setting the current mission
        DataManager.instance.SaveLastCelestrialData(myData);
        GalaxyMapManager.instance.LoadLevel();
    }
}
