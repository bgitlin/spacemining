﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {

    public string poolID;

    public List<GameObject> pool;
    public List<GameObject> removedObjects;

    public GameObject[] prefabs;

    public int initialCount;
    public int expandValue = 1;

    public bool IsPoolEmpty()
    {
        return pool.Count > 0 ? false : true;
    }

    public void InitializePool()
    {
        pool = new List<GameObject>();
        removedObjects = new List<GameObject>();
        AddMoreToPool(initialCount);
    }

    public void AddMoreToPool(int _count)
    {
        GameObject newInstance = null;

        for (int i = 0; i < _count; i++)
        {
            int prefabIndex = Random.Range(0, prefabs.Length);
            newInstance = Instantiate(prefabs[prefabIndex], transform.position, Quaternion.identity) as GameObject;
            ReturnToPool(newInstance);
        }
    }

    public GameObject TakeFromPool(Vector3 _position, Quaternion _rotation, float _scale = 1)
    {
        if (IsPoolEmpty())
        {
            AddMoreToPool(expandValue);
        }

        GameObject takenObject = pool[0];
        takenObject.transform.position = _position;
        takenObject.transform.rotation = _rotation;
        takenObject.transform.localScale = Vector3.one * _scale;
        takenObject.GetComponent<PoolObject>().ExitPool(this);

        pool.Remove(takenObject);
        removedObjects.Add(takenObject);

        return takenObject;
    }

    public void ReturnToPool(GameObject _obj)
    {
        if (removedObjects.Contains(_obj))
        {
            removedObjects.Remove(_obj);
        }
        pool.Add(_obj);

        _obj.transform.parent = transform;
        _obj.GetComponent<PoolObject>().EnterPool(this);
    }
}
