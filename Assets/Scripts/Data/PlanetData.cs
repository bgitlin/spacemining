﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlanetData : CelestrialData {

    public List<CelestrialData> mySideContent = new List<CelestrialData>();
    public List<IdleMineData> idleMineDataList = new List<IdleMineData>();
}
