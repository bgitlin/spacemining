﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MissionData
{
    public enum MissionType
    {
        enemyEncounter,
        bossBattle
    }
    public MissionType missionType;
    public bool complete;
    public bool locked;
    public int index;
}
