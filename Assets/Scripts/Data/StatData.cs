﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class StatData
{
    public float attack;
    public float defense;
    public float miningEfficiency;
    public float luck;
    
    //Temp values for Jeff/Jennie demoing, reset all values to 1
    public StatData()
    {
        attack = 10;
        defense = 10;
        miningEfficiency = 10;
        luck = 10;
    }
    public StatData(float _atk, float _def, float _eff, float _luck)
    {
        attack = _atk;
        defense = _def;
        miningEfficiency = _eff;
        luck = _luck;
    }

    /// <summary>
    /// Pass thru a function that takes a float
    /// and returns a modified version of it.!--
    /// TODO: either support multiple modifiers or make overloaded function
    /// </summary>
    /// <param name="modifier"></param>
    public void ModifyStats(Func<float,float> modifier)
    {
        attack = modifier(attack);
        defense = modifier(defense);
        miningEfficiency = modifier(miningEfficiency);
        luck = modifier(luck);
    }

    public static StatData ModifyStats(StatData a, StatData b, Func<float,float,float> modifier)
    {
        return new StatData()
        {
            attack = modifier(a.attack, b.attack),
            defense = modifier(a.defense, b.defense),
            miningEfficiency = modifier(a.miningEfficiency, b.miningEfficiency),
            luck = modifier(a.luck, b.luck)
        };
    }

    public void RecalculateStats(int _level, StatData _perctStatData)
    {
        Debug.Log(UpgradeManager.instance.companionStatsCurve.Evaluate(_level));
        attack = UpgradeManager.instance.companionStatsCurve.Evaluate(_level) * _perctStatData.attack;
        defense = UpgradeManager.instance.companionStatsCurve.Evaluate(_level) * _perctStatData.defense;
        miningEfficiency = UpgradeManager.instance.companionStatsCurve.Evaluate(_level) * _perctStatData.miningEfficiency;
        luck = UpgradeManager.instance.companionStatsCurve.Evaluate(_level) * _perctStatData.luck;
    }

    #region  Debugging

    public override string ToString()
    {
        return  "Attack: " + attack + "\nDefense:" + defense + "\nMining Efficiency" + miningEfficiency + "\nLuck" + luck;
    }
    
    #endregion
}