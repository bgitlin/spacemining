﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System;

public class DataManager : MonoBehaviour {

    public static DataManager instance;

    private void Awake()
    {
        instance = this;

        // TEMP For some reason Initialize isn't being called
        // Putting in here for now
        Initialize();
    }
    private void Start()
    {
        if (IdleMineManager.instance != null)
        {
            IdleMineManager.instance.Initialize();
        }
        MintingManager.instance.Initialize();
    }

    public GalaxyData galaxyData;
    public CelestrialData celestrialData;
    public int currentGold;
    public Action OnInventoryLoaded;

    //Prizenetonium
    public int prizenetonium = 0;

    private void OnDisable()
    {
        SaveIdleMineData();
        SaveCurrentGalaxyData();
        SaveMinterData(MintingManager.instance.minterData);
    }

    public void Initialize()
    {
        print("currentPlanetLoaded, gold loaded");
        LoadGold();
        LoadPrizenetonium();
        celestrialData = LoadLastCelestrialData();
        
  
        if (LoadGalaxyData() == null)
        {
            galaxyData.celestrialDatas = new List<PlanetData>();
        }
        else
        {
            galaxyData = LoadGalaxyData();
        }      
    }

    public void SaveIdleMineData()
    {
        for (int i = 0; i < IdleMineManager.instance.idleMineControllers.Count; i++)
        {
            galaxyData.celestrialDatas[i].idleMineDataList = IdleMineManager.instance.idleMineControllers[i].idleMines;
        }
        SaveCurrentGalaxyData();
    }

    public void LoadGold()
    {
        currentGold = PlayerPrefs.GetInt("Gold", 0);
        if(UI_HUD.instance != null)
        {
            UI_HUD.instance.UpdateGoldText(currentGold);
        }
    }
    public void AdjustGold(int _value)
    {
        currentGold += _value;
        if(UI_HUD.instance != null)
        {
            UI_HUD.instance.UpdateGoldText(currentGold);
            if (UpgradeManager.instance.ui.gameObject.activeInHierarchy)
            {
                UpgradeManager.instance.ui.UpdateStats();
            }
        }
        PlayerPrefs.SetInt("Gold", currentGold);
    }

    //Prizenetonium
    public void LoadPrizenetonium()
    {
        prizenetonium = PlayerPrefs.GetInt("Prizenetonium", 0);
        if (UI_HUD.instance != null)
        {
            UI_HUD.instance.UpdatePrizenetoniumText(prizenetonium);
        }
    }
    public void AdjustPrizenetonium(int _value)
    {
        prizenetonium += _value;
        if (UI_HUD.instance != null)
        {
            UI_HUD.instance.UpdatePrizenetoniumText(prizenetonium);
            MintingManager.instance.CheckMint();
            if (UpgradeManager.instance.ui.gameObject.activeInHierarchy)
            {
                UpgradeManager.instance.ui.UpdateStats();
            }
        }
        PlayerPrefs.SetInt("Prizenetonium", prizenetonium);
    }

    public void SaveMinterData(MinterData _minterData)
    {
        string minterDataString = JsonUtility.ToJson(_minterData);
        PlayerPrefs.SetString("minterData", minterDataString);
        print(minterDataString);
    }

    public MinterData LoadMinterData()
    {
        string minterDataString = PlayerPrefs.GetString("minterData", "");
        if (string.IsNullOrEmpty(minterDataString))
        {
            return new MinterData();
        }
        else
        {
            MinterData minterData = JsonUtility.FromJson<MinterData>(minterDataString);
            print(minterDataString);
            return minterData;
        }
    }


    public void SaveLastCelestrialData(CelestrialData _celestrialData)
    {
        print(_celestrialData.level+ " saved Level");
        celestrialData = _celestrialData;
        string lastPlanetSaveData = JsonUtility.ToJson(celestrialData);
        PlayerPrefs.SetString("lastPlanet", lastPlanetSaveData);
        print(lastPlanetSaveData);
    }

    public CelestrialData LoadLastCelestrialData()
    {
        string lastPlanetSaveString = PlayerPrefs.GetString("lastPlanet", "");
        if (string.IsNullOrEmpty(lastPlanetSaveString))
        {
            return new CelestrialData();
        }
        else
        {
            celestrialData = JsonUtility.FromJson<CelestrialData>(lastPlanetSaveString);
            print(lastPlanetSaveString);
            return celestrialData;
        }
    }

    public void SaveNewPlanet(Planet _inputPlanet)
    {
        galaxyData.celestrialDatas.Add(_inputPlanet.myData);
        SaveCurrentGalaxyData();
    }
    public void SaveNewPlanet(PlanetData _inputPlanetData)
    {
        galaxyData.celestrialDatas.Add(_inputPlanetData);
        SaveCurrentGalaxyData();
    }

    public void SavePlanetData(PlanetData _inputPlanetData)
    {
        for (int i = 0; i < galaxyData.celestrialDatas.Count; i++)
        {
            if (galaxyData.celestrialDatas[i].planetIndex == _inputPlanetData.planetIndex)
            {
                galaxyData.celestrialDatas[i] = _inputPlanetData;
            }
        }
        SaveCurrentGalaxyData();
    }

    public void SaveCurrentGalaxyData()
    {
        string galaxySaveData = JsonUtility.ToJson(galaxyData);
        PlayerPrefs.SetString("galaxySaveData", galaxySaveData);
        print(galaxySaveData);
    }

    public GalaxyData LoadGalaxyData()
    {
        string galaxySaveString = PlayerPrefs.GetString("galaxySaveData", "");
        if (string.IsNullOrEmpty(galaxySaveString))
        {
            return null;
        }
        else
        {
            galaxyData = JsonUtility.FromJson<GalaxyData>(galaxySaveString);
            print(galaxySaveString);
            return galaxyData;
        }
    }

    public void SaveStatData(StatData _data, string _id)
    {
        PlayerPrefs.SetString(_id, JsonUtility.ToJson(_data));
        PlayerPrefs.Save();
    }

}
