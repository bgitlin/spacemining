﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class GalaxyData
{
    public List<PlanetData> celestrialDatas = new List<PlanetData>();
}
