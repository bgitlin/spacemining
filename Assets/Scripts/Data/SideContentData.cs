﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SideContentData
{
    public CelestrialData myPlanetData;
    public CelestrialData.CelestrialBodyType type;
    public Vector3 location;
    public bool beat;
    public int currentMissionIndex;

    public List<MissionData> missions;

    public void InitializeSideContentData(CelestrialData.CelestrialBodyType _type, Vector3 _loc)
    {
        type = _type;
        location = _loc;
    }

    public void InitializeMissions(int _amt)
    {
        Debug.Log("Initialized Missions to:" + _amt);
        missions = new List<MissionData>();
        for (int i = 0; i < _amt; i++)
        {
            Debug.Log("missionGenerated: " + _amt);
            MissionData tempData = new MissionData();
            tempData.complete = false;
            if (i == 0)
            {
                tempData.locked = false;
            }
            else
            {
                tempData.locked = true;
            }
            missions.Add(tempData);
        }
    }
}
