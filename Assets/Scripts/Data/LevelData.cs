﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelData : MonoBehaviour
{
    public List<LevelBlockInfo.BlockType> blockOrder = new List<LevelBlockInfo.BlockType>();
}
