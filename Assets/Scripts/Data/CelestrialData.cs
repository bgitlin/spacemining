﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class CelestrialData
{
    public enum CelestrialBodyType
    {
        planet,
        shipWreck,
        gasGiant
    }
    public CelestrialBodyType type;

    public enum GenerationType
    {
        planetType1,
        planetType2,
        planetType3,
        gasGiant,
    }
    public GenerationType generationType;

    public Vector3 location;
    public int level;
    public int galacticPosition;
    public int planetIndex;
    public string id;
    public bool beat = false;
    public int sidesToGenerate;

    //Celestrial data needs to know how many missions.
    public List<MissionData> missions;
    public int currentMissionIndex;
    
    public void InitializeCelestrialData(CelestrialBodyType _type, Vector3 _loc, int _level, string _id, int _index)
    {
        type = _type;
        location = _loc;
        level = _level;
        id = _id;
        planetIndex = _index;
    }
}
