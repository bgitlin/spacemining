﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRotation : MonoBehaviour
{
    public Transform wholePlanet;
    public Transform[] atmosphereLayers;

    public float planetRotateSpeed;
    public float atmosphereRotateSpeed;

    private void Update()
    {
        wholePlanet.Rotate(new Vector3(0f, 0f, planetRotateSpeed * Time.deltaTime));

        for (int i = 0; i < atmosphereLayers.Length; i++)
        {
            atmosphereLayers[i].Rotate(new Vector3(0f, 0f, atmosphereRotateSpeed * Time.deltaTime * Random.Range(0.75f, 2.25f)));
        }
    }
}
