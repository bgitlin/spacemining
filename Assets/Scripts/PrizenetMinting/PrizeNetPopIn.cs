﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrizeNetPopIn : UI_Menu
{
    public Text totalPrizeNetTokens;

    public override void OpenMenu()
    {
        base.OpenMenu();
        ShowTokenAmt();
    }

    public override void CloseMenu()
    {
        base.CloseMenu();
    }

    public void ShowTokenAmt()
    {
        totalPrizeNetTokens.text = MintingManager.instance.minterData.prizenetTokens.ToString();
    }
}
