﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Minter : MonoBehaviour {

    public Image minterFill;
    public Text prizenetTokenText;

    public RectTransform coinTransform;

    public CanvasGroup tokenStartedGroup;

    public void UpdateMinterUI()
    {
        minterFill.fillAmount = (MintingManager.instance.minterData.mintTime / MintingManager.instance.minterData.timeToMint);
    }

    public void UpdatePrizenetTokenText()
    {
        prizenetTokenText.text = MintingManager.instance.minterData.prizenetTokens.ToString();
    }

    public void BounceCoin()
    {
        StartCoroutine(AnimationManager.instance.ScaleAndBounceRoutine(coinTransform, 0, 1));
    }

    public void ShowIndicator()
    {
        StartCoroutine(ShowIndicatorRoutine(tokenStartedGroup));
    }
    private IEnumerator ShowIndicatorRoutine(CanvasGroup _group)
    {
        AnimationManager.instance.FadeInCanvasGroup(_group, 0, 1, 2);
        yield return new WaitForSeconds(4f);
        AnimationManager.instance.FadeInCanvasGroup(_group, 1, 0, 0.5f);
    }
}
