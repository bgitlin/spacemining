﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MinterData
{
    public float mintTime;
    public float timeToMint;
    public int currentlyMinting;
    public int currentlyMintingCap;
    public int prizenetTokens;
    public int prizenetTokenCap;

    public MinterData()
    {
        timeToMint = 30; //temp
        mintTime = 0;
        currentlyMinting = 0;
        currentlyMintingCap = 1;
        prizenetTokens = 0;
        prizenetTokenCap = 50;
    }
}
