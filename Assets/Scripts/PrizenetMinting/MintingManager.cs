﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MintingManager : MonoBehaviour {

    public static MintingManager instance;

    private void Awake()
    {
        instance = this;
    }
    
    private int mintCost = 100;
    public MinterData minterData;
    public UI_Minter ui_minter;

    public void Initialize()
    {
        minterData = DataManager.instance.LoadMinterData();
        if (minterData.currentlyMinting > 0)
        {
            StartMinting();
        }
        ui_minter.UpdateMinterUI();
        ui_minter.UpdatePrizenetTokenText();
    }

    void OnDestroy() 
    {
        StopAllCoroutines();    
    }

    public void CheckMint()
    {
        //call every time we recieve prizenetonium
        if (DataManager.instance.prizenetonium >= mintCost) //if we have enough prizenetonium
        {
            if (minterData.currentlyMinting < minterData.currentlyMintingCap) //if we have room to mint another token
            {
                if (minterData.prizenetTokens < minterData.prizenetTokenCap) //if we have room for another prizenetToken
                {
                    print("StartedMintingToken");
                    minterData.currentlyMinting++;
                    DataManager.instance.AdjustPrizenetonium(-mintCost);
                    StartMinting();
                }
            }
        }
    }

    private void StartMinting()
    {
        Debug.Log(ui_minter);
        if( ui_minter.coinTransform != null)
            ui_minter.coinTransform.gameObject.SetActive(true);
        ui_minter.BounceCoin();
        if (ui_minter.tokenStartedGroup != null)
        {
            ui_minter.ShowIndicator();
        }
        StartCoroutine(MintPrizenetToken(minterData.mintTime));
    }
    private IEnumerator MintPrizenetToken(float _timeToMint)
    {
        minterData.mintTime = _timeToMint;

        while (minterData.mintTime <= minterData.timeToMint)
        {
            minterData.mintTime += Time.deltaTime;
            ui_minter.UpdateMinterUI();
            yield return 0f;
        }

        minterData.currentlyMinting--;
        minterData.prizenetTokens++;
        ui_minter.UpdatePrizenetTokenText();
        minterData.mintTime = 0; //Reset Mint time
        ui_minter.coinTransform.gameObject.SetActive(false);
        CheckMint(); //Check for another token
        ui_minter.UpdateMinterUI();
        print("Token Minted. CurrentPriznetTokens: " + minterData.prizenetTokens);
    }

    public void CollectPrizeNetTokens()
    {
        print("Tokens Emptied");
        minterData.prizenetTokens = 0;
        ui_minter.UpdatePrizenetTokenText();
    }

    [ContextMenu ("Add Prizenetonium")]
    private void AddPrizenetonium()
    {
        DataManager.instance.AdjustPrizenetonium(50);
    }
}
