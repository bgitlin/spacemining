﻿using System;
using UnityEngine;

public class VoxelMap : MonoBehaviour
{
    public int mapWidth = 1; // In Chunks
    public int chunkWidth = 16; // In Voxels
    public int chunkHeight = 8; // In Voxels
    public float voxelSize = 1f;

    public int seed = 0;

    public VoxelChunk voxelChunkPrefab;

    [HideInInspector]
	public VoxelChunk[] chunks;
	
	private float mapSize, chunkSize, halfMapSize;

    private int voxelLayer;
    
	
	public void Initialize()
    {
        print("mapinitialized");
        voxelLayer = LayerMask.NameToLayer("Minable");

        seed = DateTime.Now.Second;

        mapSize = voxelSize * chunkWidth * mapWidth;
		halfMapSize = mapSize * 0.5f;
		chunkSize = voxelSize * chunkWidth;
		
		chunks = new VoxelChunk[mapWidth * mapWidth];
		for (int i = 0, z = 0; z < mapWidth; z++)
        {
			for (int x = 0; x < mapWidth; x++, i++)
            {
				CreateChunk(i, x, z);
			}
		}
	}

	private void CreateChunk(int i, int x, int z)
    {
		VoxelChunk chunk = Instantiate(voxelChunkPrefab) as VoxelChunk;
		chunk.Initialize(chunkWidth, chunkHeight, chunkWidth, chunkSize, seed);
		chunk.transform.parent = transform;
		chunk.transform.localPosition = new Vector3(x * chunkSize - halfMapSize, 0, z * chunkSize - halfMapSize);
		chunks[i] = chunk;
	}
}