﻿using UnityEngine;
using System.Collections.Generic;
using ProceduralNoiseProject;
using System.Collections;

[SelectionBase]
public class VoxelChunk : MonoBehaviour
{
    [HideInInspector]
	public int width;
    [HideInInspector]
    public int height;
    [HideInInspector]
    public int length;

    public Voxel voxelPrefab;

    [HideInInspector]
    public Voxel[] voxels;

    private VoxelMesh vMesh;
    private float voxelSize;

    private void Awake()
    {
        vMesh = GetComponent<VoxelMesh>();
        StartCoroutine(UpdateMeshRoutine());
    }

    public void Initialize (int width, int height, int length, float size, int seed)
    {
		this.width = width;
        this.height = height;
        this.length = length;
		voxelSize = size / width;
		voxels = new Voxel[width * height * length];

        INoise perlin = new PerlinNoise(seed, 1f);
        FractalNoise fractal = new FractalNoise(perlin, 3, 5.5f);
        VoronoiNoise voronoi = new VoronoiNoise(seed, 2f, 5f);

        int totalVoxelCount = 0;
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                for (int z = 0; z < length; z++)
                {
                    float fx = x / (width - 1.0f);
                    float fy = y / (height - 1.0f);
                    float fz = z / (length - 1.0f);

                    int idx = x + y * width + z * width * height;

                    //float state = 1;
                    //float state = fractal.Sample3D(fx, fy, fz);
                    float state = Mathf.Clamp(voronoi.Sample3D(fx, fy, fz) * perlin.Sample3D(fx,fy,fz), -1f, 1f);

                    // Ground level is always solid
                    if (y == 0)
                    {
                        state = -1f;
                    }
                    else
                    {
                        // Border edges always empty
                        if (x == 0 || x == width - 1 || z == 0 || z == length - 1 || y == height - 1)
                        {
                            state = -1f;
                        }
                    }

                    voxels[idx] = CreateVoxel(x, y, z);
                    voxels[idx].state = state;
                    if(state == 1)
                    {
                        totalVoxelCount++;
                    }
                    if (voxels[idx].state < 0)
                    {
                        voxels[idx].DeactivateDummy();
                    }
                }
            }
        }

        UpdateMesh();
	}

    public void DestoryAll()
    {
        foreach(Voxel v in voxels)
        {
            if (v.state > 0)
            {
                v.healthSystem.TakeDamage(Mathf.Infinity);
            }
        }
    }
    
    private bool updateMesh = false;
    public void UpdateMesh()
    {
        updateMesh = true;
    }
    private IEnumerator UpdateMeshRoutine()
    {
        while (true)
        {
            if (updateMesh)
            {
                vMesh.Triangulate(voxels, width, height, length, voxelSize);
                updateMesh = false;
            }
            yield return 0f;
        }
    }

	private Voxel CreateVoxel (int x, int y, int z)
    {
		Voxel v = Instantiate(voxelPrefab) as Voxel;
		v.transform.parent = transform;
		v.transform.localPosition = new Vector3((x + 0.5f) * voxelSize, (y + 0.5f) * voxelSize, (z + 0.5f) * voxelSize);
		v.transform.localScale = Vector3.one * voxelSize * 0.95f;
        v.Initialize(x, y, z, voxelSize, this);
        return v;
	}
}