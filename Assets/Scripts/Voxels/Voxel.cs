﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Voxel : Minable
{
    public float state = 0f;

    public Vector3 position, xEdgePosition, yEdgePosition, zEdgePosition;

    private VoxelChunk homeChunk;

    public void Initialize(int x, int y, int z, float size, VoxelChunk chunk)
    {
        minableType = MinableType.voxel;
        position.x = (x + 0.5f) * size;
        position.y = (y + 0.5f) * size;
        position.z = (z + 0.5f) * size;

        xEdgePosition = position;
        xEdgePosition.x += size * 0.5f;
        yEdgePosition = position;
        yEdgePosition.y += size * 0.5f;
        zEdgePosition = position;
        zEdgePosition.z += size * 0.5f;

        homeChunk = chunk;
        healthSystem.InitHealth(UpgradeManager.instance.voxelHealthCurve.Evaluate(DataManager.instance.celestrialData.level + 1 + (DataManager.instance.celestrialData.currentMissionIndex * 0.15f)) * 0.25f);
    }
    

    public override void Destroy()
    {
        base.Destroy();
        Delete();
    }
    public void DeactivateDummy()
    {
        gameObject.SetActive(false);
    }

    private void Delete()
    {
        // Play a particle system

        // Change state
        state = -1f;
        DeactivateDummy();

        // Update Mesh
        homeChunk.UpdateMesh();
    }
}
