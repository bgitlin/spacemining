﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq; //TEMP:


public class LevelBlockInfo : MonoBehaviour
{
    //When polishing this we'll create a blockdata that holds all the info about the block, and let this script just manage functionality
    public enum BlockType
    {
        empty,
        minable,
        enemies,
        boss
    }
    public BlockType blockType;

    public VoxelMap voxelMap;
    public Resource resource;
    public PrizenetoniumResource prizenetoniumResource;
    public int resourceCount;

    public Transform tempSpawnPos;

    public List<Minable> myMinables = new List<Minable>();

    public System.Action OnInitialized;
    public System.Action OnCompleted;

    public void InitializeVoxelMap(System.Action callback)
    {
        canRestart = true;
        
        if (blockType == BlockType.minable)
        {
            if (voxelMap != null)
            {
                voxelMap.Initialize();
                resourceCount = Random.Range(15, 25);
                int voxelCount = voxelMap.chunks[0].voxels.Length;
                float percentage = voxelCount/resourceCount;
                int percentOffset = Mathf.RoundToInt(Random.Range(0.01f, 0.05f) * voxelCount);
                for (int i = 0; i < voxelMap.chunks[0].voxels.Length; i++)
                {
                    //if there's still minables to spawn
                    if (myMinables.Count < resourceCount && voxelMap.chunks[0].voxels[i].position.y > 0.5f && voxelMap.chunks[0].voxels[i].state > 0)
                    {
                        //if the resource count is 10
                        //if the amount into the voxels is within 10/voxelcount 
                        if (i >= (percentage*myMinables.Count) + percentOffset)
                        {
                            if (Random.value > 0.7f)
                            {
                                PrizenetoniumResource newResource = Instantiate(prizenetoniumResource, voxelMap.chunks[0].voxels[i].transform.position, Quaternion.Euler(Vector3.one * Random.Range(0, 360)), transform) as PrizenetoniumResource;
                                newResource.myBlockInfo = this;
                                myMinables.Add(newResource);
                            }
                            else
                            {
                                Resource newResource = Instantiate(resource, voxelMap.chunks[0].voxels[i].transform.position, Quaternion.Euler(Vector3.one * Random.Range(0, 360)), transform) as Resource;
                                newResource.myBlockInfo = this;
                                myMinables.Add(newResource);
                            }
                            percentOffset = Mathf.RoundToInt(Random.Range(0.01f, 0.05f) * voxelCount);
                        }
                    }
                }
                /*
                foreach (Voxel voxel in voxelMap.chunks[0].voxels)
                {
                    if (myMinables.Count < 6 && voxel.position.y > 0.5f && voxel.state > 0 && Random.value < 0.5f)
                    {
                        Resource newResource = Instantiate(resource, voxel.transform.position, Quaternion.Euler(Vector3.one * Random.Range(0, 360)), transform) as Resource;
                        newResource.myBlockInfo = this;
                        myMinables.Add(newResource);
                    }
                }
                objectCount = myMinables.Count;
                */
            }
            else
            {
                print("voxelMapMissing?");
            }
            Resource neverFails = Instantiate(resource, tempSpawnPos.position, Quaternion.identity, transform) as Resource;
            neverFails.myBlockInfo = this;
            myMinables.Add(neverFails);

            //Initialize Resources
            for (int i = 0; i < myMinables.Count; i++)
            {
                myMinables[i].Initialize();
            }
        }

        if (callback != null)
        {
            callback();
        }
    }

    public void Initialize()
    {
        //Should activate inactive components.
        switch(blockType)
        {
            case (BlockType.boss):

                for (int i = 0; i < myMinables.Count; i++)
                {
                    (myMinables[i]).Initialize();
                }
                break;

            case (BlockType.enemies):

                myMinables = this.GetComponentsInChildren<Minable>().ToList();
                for (int i = 0; i < myMinables.Count; i++)
                {
                    myMinables[i].Initialize(this);
                }
                break;
        }

        if(OnInitialized != null)
        {
            Debug.Log("INIT");
            OnInitialized();
        }
    }

    public void RemoveMinable(Minable _inputMinable)
    {
        myMinables.Remove(_inputMinable);
        if (myMinables.Count < 1 && canRestart)
        {
            BlockComplete();
        }
    }
    
    bool canRestart = true;
    public void BlockComplete()
    {   
        if(OnCompleted != null)
        {
            OnCompleted();
        }
        GameManager.instance.levelManager.ResetLevel();
        canRestart = false;
    }
}
