﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MiningControl))]
public class PlayerInput : MonoBehaviour
{
    public bool mouseButtonDown;
    public bool mouseButton;
    public MiningControl miningControl;
    public TrailRenderer trail;

    private void Start()
    {
        miningControl.Init(PlayerShip.instance);
        miningControl.onHit.AddListener(OnHit);
        StartCoroutine(myUpdate());
    }

    private IEnumerator myUpdate()
    {
        while (true)
        {
            if (Input.GetMouseButton(0))
            {
                float attack = PlayerShip.instance.GetModifiedStats().attack;
                miningControl.StartMining(Input.mousePosition, attack, true);
            }
            else
            {
                miningControl.StopMining();
            }

            if (trail != null && Input.GetMouseButton(0))
            {
                Vector3 mousePos = Input.mousePosition;
                trail.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 100));
            }
            yield return 0f;
        }
    }

    public void OnHit()
    {
        SoundFXManager.instance.Swipe();
    }
}
