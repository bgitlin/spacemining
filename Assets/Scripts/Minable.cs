﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minable : MonoBehaviour, IAttackable
{
    public bool canDamage;

    public Vector3 startScale;

    public AttackSystem attackSytem;

    public HealthSystem healthSystem;

    public GameObject hitFx;

    public enum MinableType
    {
        voxel,
        resource,
        enemy,
        boss
    }
    public MinableType minableType;

    [Range(0,1)]
    public float attackedCoolDown;

    Dictionary<MiningControl, MiningHit> _miners = new Dictionary<MiningControl, MiningHit>();

    private void Awake()
    {
        hitFx = Resources.Load<GameObject>("HitParticles_Arrows");

        Subscribe();

        startScale = transform.localScale;

        //TODO: TEMP: In here for testing, remove once we
        //figure out what all instances should be
        attackedCoolDown = 0.01f;
    }

    public virtual void Initialize()
    {

    }

    public virtual void Initialize(LevelBlockInfo block)
    {
        Initialize();
    }

    void OnDestroy()
    {
        UnSubscribe();
    }

    void Subscribe()
    {
        healthSystem.OnDeath += Destroy;
        healthSystem.OnDamageTaken += DamageTook;
    }

    void UnSubscribe()
    {
        healthSystem.OnDeath -= Destroy;
        healthSystem.OnDamageTaken -= DamageTook;
        StopAllCoroutines();
    }

    public virtual void Hit(float damage)
    {
        healthSystem.TakeDamage(damage);
        //TEMP: Remove after particlemanager subscribes to ondamage taken
        ParticleManager.instance.OnDamageTaken(transform.position);
        //Instantiate(GameManager.instance.mineFx, transform.position, Quaternion.identity);
    }

    public virtual void TakeDamage(IAttacker attacker)
    {
        healthSystem.TakeDamage(attacker);
        //TEMP: Remove after particlemanager subscribes to ondamage taken
        ParticleManager.instance.OnDamageTaken(transform.position);
        //Instantiate(GameManager.instance.mineFx, transform.position, Quaternion.identity);
    }
    
    
    public void TakeDamage(float damage)
    {
        Hit(damage);
    }

    public virtual void DamageTook()
    {
        transform.localScale = (startScale * 1.25f);
    }

    private void Update()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, startScale, Time.deltaTime * 9);
    }


    public virtual void Destroy()
    {
        ParticleManager.instance.OnDeath(transform.position);
        //Instantiate(GameManager.instance.mineFx, transform.position, Quaternion.identity);
        UnSubscribe();
    }

    public virtual void Destroy(IAttacker attacker)
    {
        Destroy();
    }

}
