﻿Shader "Custom/Triplanar Rim" {
	Properties 
	{
		_DiffuseMap ("Diffuse Map ", 2D)  = "white" {}
		_TextureScale ("Texture Scale",float) = 1
		_TriplanarBlendSharpness ("Blend Sharpness",float) = 1
		_RimColor ("Rim color", color) = (1,1,1,1)
		_RimDirection ("Rim direction", vector) = (0,0,1,0)
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Lambert finalcolor:final

		sampler2D _DiffuseMap;
		float _TextureScale;
		float _TriplanarBlendSharpness;
		fixed4 _RimColor;
		fixed4 _RimDirection;

		struct Input
		{
			float3 worldPos;
			float3 worldNormal;
			float3 viewDir;
		}; 

		void surf (Input IN, inout SurfaceOutput o) 
		{
			// Find our UVs for each axis based on world position of the fragment.
			half2 yUV = IN.worldPos.xz / _TextureScale;
			half2 xUV = IN.worldPos.zy / _TextureScale;
			half2 zUV = IN.worldPos.xy / _TextureScale;
			// Now do texture samples from our diffuse map with each of the 3 UV set's we've just made.
			half3 yDiff = tex2D (_DiffuseMap, yUV);
			half3 xDiff = tex2D (_DiffuseMap, xUV);
			half3 zDiff = tex2D (_DiffuseMap, zUV);
			// Get the absolute value of the world normal.
			// Put the blend weights to the power of BlendSharpness, the higher the value, 
            // the sharper the transition between the planar maps will be.
			half3 blendWeights = pow (abs(IN.worldNormal), _TriplanarBlendSharpness);
			// Divide our blend mask by the sum of it's components, this will make x+y+z=1
			blendWeights = blendWeights / (blendWeights.x + blendWeights.y + blendWeights.z);
			// Finally, blend together all three samples based on the blend mask.
			o.Albedo = xDiff * blendWeights.x + yDiff * blendWeights.y + zDiff * blendWeights.z;
		}

		void final(Input IN, SurfaceOutput o, inout fixed4 color)
		{
			/*fixed3 sideNorm = IN.viewDir;
			sideNorm.x = -IN.viewDir.z;
			sideNorm.z = IN.viewDir.x;
			half rim = 1 - saturate(dot (IN.viewDir, o.Normal));
			half side = dot (sideNorm, o.Normal);
			color += saturate(rim - side);*/

			//fixed3 lightNorm = fixed3(0.6, 0.333, -0.333);
			float3 viewNorm = mul((float3x3)UNITY_MATRIX_V, IN.worldNormal);
			color.rgb += _RimColor * saturate(dot (viewNorm, _RimDirection.xyz)) * 2;

		}

		
		ENDCG
	}
}