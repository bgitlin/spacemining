﻿Shader "Custom/RBGVertMaskTransparent"
{
	Properties
	{
		_Specular ("Specular", Color) = (0,0,0,0)
		_Smoothness ("Smoothness", Range(0,1)) = 0.0
		_DetailTex ("Detail Tex", 2D) = "grey" {}
		_RColor ("R Color", Color) = (1,1,1,1)
		_GColor ("G Color", Color) = (1,1,1,1)
		_BColor ("B Color", Color) = (1,1,1,1)
		_Unlit ("Unlit", Range(0,1)) = 0
	}
	SubShader
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		LOD 200

		CGPROGRAM
		#pragma surface surf StandardSpecular fullforwardshadows finalcolor:final alpha:blend
		#pragma target 3.0

		sampler2D _DetailTex;

		struct Input
		{
			float2 uv_DetailTex;
			float4 color : COLOR;
		};

		fixed4 _Specular;
		half _Smoothness;
		half _Unlit;
		fixed4 _RColor;
		fixed4 _GColor;
		fixed4 _BColor;

		void surf (Input IN, inout SurfaceOutputStandardSpecular o)
		{
			fixed4 detail = tex2D (_DetailTex, IN.uv_DetailTex);

			fixed4 mask = IN.color;

			o.Albedo = 0;
			o.Albedo += lerp(0, _RColor, mask.r);
			o.Albedo += lerp(0, _GColor, mask.g);
			o.Albedo += lerp(0, _BColor, mask.b);

			o.Albedo *= detail.rgb;
			o.Alpha = detail.a;
			o.Specular = _Specular;
			o.Smoothness = _Smoothness;
		}

		void final (Input IN, SurfaceOutputStandardSpecular o, inout fixed4 color)
		{
			color.rgb = lerp(color.rgb, o.Albedo.rgb, _Unlit);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
