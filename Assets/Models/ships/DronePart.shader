﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.

Shader "Custom/DronePart"
{
    Properties
    {
        _MainTex ("RGB: Color masking, A: Multiply", 2D) = "white" {}
        _PatternTex ("Pattern tex", 2D) = "black" {}
        _RColor ("Primary Color", Color) = (1,0,0,1)
        _GColor ("Secondary Color", Color) = (0,1,0,1)
        _BColor ("Accent Color", Color) = (0,0,1,1)
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _Unlit ("Unlit", Range(0, 1)) = 0.0
        _ShieldTex ("Shield Texture", 2D) = "white" {}
        _ShieldFade ("Shield Fade", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200
       
        CGPROGRAM
 
        #pragma surface surf Standard fullforwardshadows finalcolor:final
        #pragma target 3.0
 
        struct Input
        {
            float2 uv_MainTex;
            float3 worldPos;
            float3 viewDir;
        };
       
        sampler2D _MainTex;
        sampler2D _PatternTex;
        sampler2D _ShieldTex;
        fixed4 _RColor;
        fixed4 _GColor;
        fixed4 _BColor;
        half _Glossiness;
        half _Metallic;
        half _ShieldFade;
        half _Unlit;
 
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)
 
        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            float4 c = tex2D (_MainTex, IN.uv_MainTex);
            o.Albedo = c.a;
            o.Albedo *= lerp(1, _RColor, c.r);
            o.Albedo *= lerp(1, _GColor, c.g);
            o.Albedo *= lerp(1, _BColor, c.b);
 
            fixed pattern = tex2D(_PatternTex, IN.uv_MainTex * 3).a;
            o.Albedo.rgb = lerp(o.Albedo.rgb, o.Albedo.rgb * (1 + pattern), c.r);
            //o.Albedo.rgb = lerp(o.Albedo.rgb, lerp(o.Albedo.rgb, _GColor.rgb * 0.5, pattern), c.r);
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
 
            o.Emission = lerp(0, _BColor * 2, c.b);
        }
 
        void final(Input IN, SurfaceOutputStandard o, inout fixed4 color)
        {
            float3 localPos = IN.worldPos - mul(unity_ObjectToWorld, float4(0,0,0,1)).xyz;
            fixed4 shieldTex = tex2D (_ShieldTex, localPos.xz + _Time.x * 3);
            shieldTex.a += 0.7;
            color = lerp(color, fixed4(0.333,0.75,1,1), _ShieldFade * shieldTex.a * shieldTex.a * shieldTex.a);
            color.rgb = lerp(color.rgb, o.Albedo, _Unlit);
        }
 
        ENDCG
    }
    FallBack "Diffuse"}