﻿using UnityEngine;
using UnityEditor;
public class GamePrefsMenu : MonoBehaviour
{
	[MenuItem("GameTools/DeletePrefs")]
	static void DeletePrefs()
	{
		PlayerPrefs.DeleteAll();
	}
}


