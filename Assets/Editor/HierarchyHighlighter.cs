﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[InitializeOnLoad]
public class HierarchyHighlighter
{

    static HierarchyHighlighter()
    {
        EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItem_CB;
    }

    private static void HierarchyWindowItem_CB(int selectionID, Rect selectionRect)
    {
        Object o = EditorUtility.InstanceIDToObject(selectionID);
        if (o != null)
        {
            if ((o as GameObject).GetComponent<HierarchyHighlighterComponent>() != null)
            {
                HierarchyHighlighterComponent h = (o as GameObject).GetComponent<HierarchyHighlighterComponent>();
                if (h.highlight)
                {
                    if (Event.current.type == EventType.Repaint)
                    {
                        GUI.backgroundColor = h.color;
                        GUI.Box(selectionRect, "");
                        GUI.backgroundColor = Color.white;
                        EditorApplication.RepaintHierarchyWindow();
                    }
                }
            }
        }
    }
}
